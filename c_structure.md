# Structures <!-- omit in toc -->


# 1. Introduction

Nous avons vu qu'il était possible en C de placer des valeurs dans des variables (ou pointeur de variables). Par contre, il arrive souvent qu'une valeur n'ait de sens que lorsqu'elle est reliée à plusieurs autres valeurs. Dans ces cas, au lieu de créer plusieurs variables distinctes qui contiendront ces valeurs, nous pouvons les réunir dans une structure mémoire que nous appelons justement `structure`.

Par exemple, au lieu d'avoir:
```C
void afficherPersonne(char* aPrenomPersonne, char* aNomPersonne,
        int aAgePersonne)
{
    printf("La personne: %s %s (%d ans)\n", aPrenomPersonne, aNomPersonne,
			aAgePersonne);
}
```
qui nécessite beaucoup d'arguments (qui en fait représentent la même chose,
dans ce cas, une personne); on aimerait avoir quelque chose comme:

```C
void afficherPersonne(personne*  aPersonne)
{
    printf("La personne: %s %s (%d ans)\n", aPersonne->prenom, aPersonne->nom,
			aPersonne->age);
}
```

# 2. Qu'est-ce qu'une structure

Une structure n'est rien d'autre qu'une suite d'espaces mémoire de taille prédéfinie permettant de stocker plusieurs données différentes. Par exemple, dans l'exemple précédent, je peux créer une structure `personne` contenant le *prenom* , le *nom* et l'*age* comme ceci:

```C
struct personne {
    char* prenom;
    char* nom;
    int age;
};
```

Il est à spécifier que généralement, la déclaration des structures doit se faire dans le fichier d'entête `.h`.

Ensuite, je peux l'initialiser comme ceci:
```C
int main(int aParametresTaille, char* aParametres[])
{
    struct personne lPersonne;
    lPersonne.prenom = "Louis";
    lPersonne.nom = "Marchand";
    lPersonne.age = 40;
    afficherPersonne(lPersonne);
	return 0;
}
```
Finalement, je peux légèrement modifier mon `afficherPersonne` afin de recevoir ma structure:
```C
void afficherPersonne(struct personne aPersonne)
{
	printf("La personne: %s %s (%d ans)\n", aPersonne.prenom, aPersonne.nom,
			aPersonne.age);
}
```

# 3. typedef

Dans les exemples précédents, on voit que le préfix `struct` doit toujours être utilisé dans le type lorsqu'on utilise une structure. Il est possible d'éviter cet encombrement en utilisant un `typedef`

Un `typedef` en C/C++ permet de définir de nouveau type de données. Ces nouveaux types sont équivalents à d'anciens types déjà existants. Par exemple, je pourrais faire un nouveau type `entier` qui est équivalent à `int` comme ceci:
```C
typedef int entier;
```
Tout comme les structures, les instructions `typedef` sont généralement placés dans le fichier d'entête `.h`.

Je pourrais maintenant utiliser ce nouveau type comme ceci:

```C
int main(int aParametresTaille, char* aParametres[])
{
    entier lEntier = 10;
    printf("Voici l'entier: %d\n", lEntier);
	return 0;
}
```

Maintenant, si nous revenons aux `structure`, nous pouvons retirer le préfix `struct` du type d'une structure en utilisant un `typedef` comme ceci:

```C
typedef struct structPersonne typedefPersonne;
```
!> Notez ici que j'ai utiliser strucPersonne et typedefPersonne afin de clarifier l'exemple. Vous verrez habituellement `typedef struct personne personne;`. Il n'y a pas d'ambiguité a utiliser 2 fois le même nom car le premier est le nom d'une structure, le deuxième est un nouveau type. Lors de son utilisation, soit que `personne` sera précédé du mot `struct` indiquant que ce qui suit est un `nom de structure`, soit qu'il ne le sera pas et le compilateur s'attendra à recevoir un `type`


Ce qui donne que nous pouvons maintenant utiliser la structure `structPersonne` comme ceci:

```C
void afficherPersonne(typedefPersonne aPersonne)
{
	printf("La personne: %s %s (%d ans)\n", aPersonne.prenom, aPersonne.nom,
			aPersonne.age);
}

int main(int aParametresTaille, char* aParametres[])
{
    typedefPersonne lPersonne;
    lPersonne.prenom = "Louis";
    lPersonne.nom = "Marchand";
    lPersonne.age = 40;
    afficherPersonne(lPersonne);
	return 0;
}
```

Enfin, il est à noter qu'il est possible d'utiliser cette syntaxe de `typedef` directement dans la déclaration de la structure comme ceci:

```C
typedef struct structPersonne {
    char* prenom;
    char* nom;
    int age;
} typedefPersonne;
```

# 4. Déréférencement de pointeur ->

Tout comme les autres types de données, il est possible d'utiliser des pointeurs de structure afin de travailler directement avec les adresses des structures. Par contre, l'utilisation du déréférencement devient syntaxiquement difficile à lire. Par exemple:

```C
void entrerAgePersonne(personne* aPersonne)
{
    int age = 0;
    printf("Entrez l'age de la personne: ");
    scanf("%d", &age);
    (*aPersonne).age = age;
}
```
Si vous enlever les parenthèses autour de `*aPersonne`, vous obtiendrez une erreur puisque le compilateur essai d'effectuer le `.age` sur `aPersonne` (qui est un pointeur, et non une structure) et non sur `*aPersonne` (qui représente la structure une fois déréférencée).

Afin d'alléger la lecture de ce genre d'instruction, C a un opérateur permettant d'effectuer le déréférencement directement afin d'accéder aux champs de la structure. Pour ce faire, on utilise `->` au lieu d'un `.` pour accéder aux champs de la structure. Comme ceci:

```C
void entrerAgePersonne(personne* aPersonne)
{
    int age = 0;
    printf("Entrez l'age de la personne: ");
    scanf("%d", &age);
    aPersonne->age = age;
}
```

Voici le fichier C complet pour bien comprendre l'utilisation de ce type de mécanisme:

```C
#include <stdio.h>

typedef struct personne {
    char* prenom;
    char* nom;
    int age;
} personne;

void afficherPersonne(personne aPersonne)
{
	printf("La personne: %s %s (%d ans)\n", aPersonne.prenom, aPersonne.nom,
			aPersonne.age);
}

void entrerAgePersonne(personne* aPersonne)
{
    int age = 0;
    printf("Entrez l'age de la personne: ");
    scanf("%d", &age);
    aPersonne->age = age;
}

int main(int aParametresTaille, char* aParametres[])
{
    personne lPersonne;
    lPersonne.prenom = "Louis";
    lPersonne.nom = "Marchand";
    entrerAgePersonne(&lPersonne);
    afficherPersonne(lPersonne);
	return 0;
}
```


***

# 5. Allocation dynamique d'une structure:

Dans l'exemple précédent, la structure était mise sur la pile et les pointeurs qui la constituait (char *prenom et char *nom) était initialisés avec des constantes. Il n'était donc pas nécessaire de faire des `calloc` et `free`. 

Voici un exemple plus "réaliste" montrant comment utiliser le tas pour y mettre une structure et son contenu. 


```c
#include <stdio.h>
#include <stdlib.h>

#define TAILLE_CHAINES 50

typedef struct personne {
    char* prenom;
    char* nom;
    int age;
} personne;

personne* allocationPersonne() {
    personne* lPersonne = calloc(1, sizeof(personne));
    if (lPersonne) {
        lPersonne->prenom = calloc(TAILLE_CHAINES, sizeof(char));
        if (lPersonne->prenom) {
            lPersonne->nom = calloc(TAILLE_CHAINES, sizeof(char));
            if (!lPersonne->nom) {
                free(lPersonne->prenom);
                free(lPersonne);
                lPersonne = NULL;
            }
        } else {
            free(lPersonne);
            lPersonne = NULL;
        }
    }
    return lPersonne;
}

void libererPersonne(personne* aPersonne) {
    if (aPersonne) {
        if (aPersonne->prenom) {
            free(aPersonne->prenom);
        }
        if (aPersonne->nom) {
            free(aPersonne->nom);
        }
        free(aPersonne);
    }
}

void entrerPersonne(personne* aPersonne) {
    if (aPersonne && aPersonne->prenom && aPersonne->nom) {
        printf("Veuillez entrer un prenom: ");
        scanf("%s", aPersonne->prenom);
        printf("Veuillez entrer un nom: ");
        scanf("%s", aPersonne->nom);
        printf("Veuillez entrer un age: ");
        scanf("%d", &(aPersonne->age));
    } else {
        printf("Un erreur s'est produit.\n");
    }
}

void afficherPersonne(personne* aPersonne) {
    if (aPersonne && aPersonne->prenom && aPersonne->nom) {
        printf("Le prenom de la personne: %s\n", aPersonne->prenom);
        printf("Le nom de la personne: %s\n", aPersonne->nom);
        printf("L'age de la personne: %d ans\n", aPersonne->age);
    } else {
        printf("Un erreur s'est produit.\n");
    }

}

int main(int aParametresTaille, char* aParametres[])
{
    personne* lPersonne = allocationPersonne();
    if (lPersonne) {
        entrerPersonne(lPersonne);
        afficherPersonne(lPersonne);
        libererPersonne(lPersonne);
    } else {
        printf("N'a pas pu créer la personne.");
    }
    return 0;
}
```

Comme nous le voyons dans l'exemple précédent, il ne suffit pas d'initialiser la structure en tant que telle. Il faut également initialiser les pointeurs contenus dans les champs de la structure avec des `calloc`. 

Il est également nécessaire de faire des `free` pour chaque `calloc` effectué. Souvent, lorsqu'une structure nécessite plusieurs `calloc` et plusieurs `free`, il peut être intéressant de créer des routines servant uniquement à effectuer les allocations et les libérations de structure (comme dans l'exemple précédent).

# L'opérateur = 

L'opérateur = est un peu particulié avec une structure.

Si on écrit 
```c
int x =10;
int y;
y = x;
x = 20;
printf("%i %i\n", x, y);
```

On s'attend à avoir 20 et 10 comme résultat. 

et si on écrit
```c
int x =10;
int *y;
y = &x;
x = 20;
printf("%i %i\n", x, *y);
```

On s'attend à avoir 20 et 20 comme résultat. 

Mais essayons avec une structure:

```c
#include <stdio.h>
#include <stdlib.h>

typedef struct {
     int champInt;       //un entier
     char *champStr;     //un pointeur vers un tableau de char
} demoStruct;

int main(int aParametresTaille, char* aParametres[])
{
    demoStruct struct1, struct2;
    char chaine[10] = "allo";

    struct1.champInt = 1;
    struct1.champStr = chaine;

    struct2 = struct1;

    printf("struct1 %i %s\n", struct1.champInt, struct1.champStr);
    printf("struct2 %i %s\n", struct2.champInt, struct2.champStr);    
}
```

Rien de spécial. La deuxième structure contient les mêmes valeurs que la première. 

Maintenant, ajoutez ce code à la fin:

```c
    struct2.champInt = 2;
    printf("struct1 %i %s\n", struct1.champInt, struct1.champStr);
    printf("struct2 %i %s\n", struct2.champInt, struct2.champStr);
```

Ici aussi, rien de spécial, les valeurs entières sont indépendantes. 

Dernier changement:

```c
    struct2.champStr[0] = 'x';
    printf("struct1 %i %s\n", struct1.champInt, struct1.champStr);
    printf("struct2 %i %s\n", struct2.champInt, struct2.champStr);
```

Oh! pourquoi le changement dans une des structures a affecté l'autre?

On dit que l'opérateur `=` fait un `shallow copy` de la structure. C'est à dire qu'il ne copie que les valeurs directement dans la structure. 

Le composant champStr est un pointeur. La chaine de caractère est donc à l'extérieur de la structure. Ce n'est que son adresse qui est dans la structure. 

Lorsqu'on change struct2.champStr[0], on change cet espace mémoire qui est l'extérieur. On le change donc pour les deux structures. 

!> Faites donc très attention avec l'utilisation de l'assignation d'une structure à une autre. 


# Passage d'une structure par valeur

Une structure peut être passée et retournée comme un type de base (int, float ... ). 

On pourrait donc reprendre le code de la section 4 sans utiliser de pointeur

```c
#include <stdio.h>

typedef struct personne {
    char* prenom;
    char* nom;
    int age;
} personne;

void afficherPersonne(personne aPersonne)
{
    printf("La personne: %s %s (%d ans)\n", aPersonne.prenom, aPersonne.nom,
            aPersonne.age);
}

personne entrerAgePersonne(personne aPersonne) //1
{
    int age = 0;
    printf("Entrez l'age de la personne: ");
    scanf("%d", &age);
    aPersonne.age = age;
    return aPersonne;
}

int main(int aParametresTaille, char* aParametres[])
{
    personne lPersonne;
    lPersonne.prenom = "Louis";
    lPersonne.nom = "Marchand";
    lPersonne = entrerAgePersonne(lPersonne); //2
    afficherPersonne(lPersonne);
    return 0;
}
```

La fonction `entrerAgePersonne` a été modifiée afinde recevoir une personne en argument (c'était un pointeur avant), et retourner cette meme personne. 

Nous avons vue dans la section sur les pointeurs qu'il était dangereux de retourner qqchose qui était sur la pile. Dans ce cas, la structure est retournée par valeur, elle est donc copiée lors du return. 

