# Listes chainées <!-- omit in toc -->

On a vu qu'il est possible de mettre plusieurs éléments dans un tableau, et qu'il est possible d'agrandir ce tableau avec realloc. Cette technique est efficace quand on a de petits tableaux, mais quand on a des milliers d'éléments, realloc devient moins efficace car il doit trouver l'espace nécessaire pour dupliquer l'information et ensuite faire la copie. Cette opération peut prendre beaucoup de temps. 

Les listes chainées sont un autre moyen utilisé pour entreposer beaucoup d'information. 

Le concept est simple: avoir une `structure` contenant l'information que l'on désire entreposer, et y ajouter un pointeur vers le prochain élément dans la liste. Quand un élément n'a pas de suivant, ce pointeur est mis à `NULL`.

![Liste chainée](_media/liste_chainee.PNG)

# Gestion de la liste

Premièrement définissons c'est qu'est un élément de notre liste. Ici, nous y mettrons simplement un entier, mais ca pourrait être n'importe quoi... un pointeur vers qqchose, une chaine de caractère, une personne...

Vous remarquerez qu'en plus de la donnée (l'entier), il y a un pointeur vers l'élément suivant. 

```c
typedef struct structElement element;
struct structElement
{
    int nombre;
    element *suivant;
};
```
Notez ici qu'il est nécessaire de définir le `typedef` avant la struct, sinon lors de la définition de la struct, le type element n'existerait pas. 

!>Il ne faut donc PAS faire

typedef struct structElement
{
    int nombre;
    element *suivant;
} element;


Nous allons aussi créer un struct pour représenter la liste elle même. 

```c
typedef struct structListe
{
    element *premier;
} liste;
```

!> ici l'utilisation d'une struct uniquement pour entreposer le premier element est un peu "overkill", mais nous verrons plus tard qu'on veut garder un peu plus que simplement le premier élément. 

Pour initialiser une liste, nous utiliserons une fonction:

```c
liste *initialiserListe()
{
    liste *lListe = calloc(1, sizeof(liste));

    if (lListe == NULL ) // vérification de l'allocation de l'espace mémoire
    {
        exit(EXIT_FAILURE); //nous n'avons pas vue cette fonction, mais elle fait ce qu'elle dit
                            //... elle termine le programme en retournant un code de sortie
                            // ici c'est un code d'erreur qui est prédéfini
    }
    lListe->premier = NULL;
    return lListe;
}
```

Nous aurons aussi besoin d'une fonction pour ajouter un element dans la liste:

```c
void insererElementAuDebut(liste *aListe, int aNombre)
{
    /* Création du nouvel élément */
    element *nouveau = calloc(1, sizeof(element));
    if (aListe == NULL || nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }
    nouveau->nombre = aNombre;

    /* Insertion de l'élément au début de la liste */
    nouveau->suivant = aListe->premier;
    aListe->premier = nouveau;
}
```

D'une fonction pour enlever le premier élément de la liste:

```c
void supprimerPremier(liste *aListe)
{
    element *lSupprimer;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (aListe->premier != NULL)
    {
        lSupprimer = aListe->premier;
        aListe->premier = aListe->premier->suivant;
        free(lSupprimer);
    }
}
```

Et finalement d'une fonction pour afficher la liste:

```c
void afficherListe(liste *aListe)
{
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }

    element *actuel = aListe->premier;
    while (actuel != NULL)
    {
        printf("%d -> ", actuel->nombre);
        actuel = actuel->suivant;
    }
    printf("NULL\n");
}

```

On peut maintenant essayer notre code. 

```c
int main()
{
    liste *maListe = initialiserListe();

    insererElementAuDebut(maListe, 4);
    insererElementAuDebut(maListe, 8);
    insererElementAuDebut(maListe, 15);
    supprimerPremier(maListe);
    afficherListe(maListe);
    return 0;
}
```

!> Conservez ce code, nous allons l'utiliser pour la prochaine leçons.
