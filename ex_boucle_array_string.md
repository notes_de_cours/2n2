# Exercices boucle array et string <!-- omit in toc -->

Ouvrez votre projet d'exercices et ajoutez les 2 fichiers suivants:

<details>
<summary> serie3.h  </summary>

```c
#ifndef SERIE3_H
#define SERIE3_H

void serie3_ex1();
void serie3_ex2();

#endif // SERIE3_H

```

</details>

<details>
<summary> serie3.c</summary>

```c
#include <stdio.h>
#include <string.h>

void serie3_ex1()
{
/*
 * Codez une fonction qui demande un mot à l'usager et qui affiche ce mot à l'envers
 * Vous devez faire une boucle qui parcours la chaine de caractères entrée
 * en partant de la fin jusqu'au début.
 * Vous ne devez pas afficher les caractères null se trouvant à la fin de la chaine
 *
 */

    char chaine[21];

    printf("Entrez un mot (20 caractères maximum): ");
    scanf("%s", chaine);
    // insérez votre code ici


    printf("\n");
}

void serie3_ex2() {
/*
 * Codez une fonction qui demande 10 entiers positifs à l'usager, les entrepose dnas un tableau
 * et qui les affiche du plus petit au plus grand sans trier le tableau (mais vous pouvez le modifier).
 * (truc: les entiers entrés sont tous positifs... donc tous plus grands qu'un négatif)
 */

    int tableau[10];

    for(int i=0; i<10; i++) {
        printf("entrez la valeur : ");
        scanf("%i", &tableau[i]);
    }
    //insérez votre code ici
}
```
</details>


Et ajoutez un include dans `main.c` pour `serie3.h`


!>Notez que les #include de `stdio.h` et `string.h` sont fournis. 

Comme pour la série 1 et 2, les réponses sont ci-bas. Ne les regardez qu'après avoir essayé de trouver la réponse. 


<details>
<summary> Serie3_ex1 solution  </summary>

```c
void serie3_ex1()
{
/*
 * Codez une fonction qui demande un mot à l'usager et qui affiche ce mot à l'envers
 * Vous devez faire une boucle qui parcours la chaine de caractères entrée
 * en partant de la fin jusqu'au début.
 * Vous ne devez pas afficher les caractères null se trouvant à la fin de la chaine
 *
 */

    char chaine[21];

    printf("Entrez un mot (20 caractères maximum): ");
    scanf("%s", chaine);
    for(int i  = strlen(chaine)-1; i>=0; i--) {
        printf("%c", chaine[i]);
    }
    printf("\n");
}
```

</details>
<details>
<summary> Serie3_ex2 solution  </summary>

```c
void serie3_ex2() {
/*
 * Codez une fonction qui demande 10 entiers positifs à l'usager, les entrepose dans un tableau
 * et qui les affiche du plus petit au plus grand sans trier le tableau (mais vous pouvez le modifier).
 * (truc: les entiers entrés sont tous positifs... donc tous plus grands qu'un négatif)
 */

    int tableau[10];

    for(int i=0; i<10; i++) {
        printf("entrez la valeur : ");
        scanf("%i", &tableau[i]);
    }
    int max = -1;
    int indice;
    for(int i = 0; i<10; i++) {
        for(int j=0; j<10; j++) {
            if(tableau[j]>=max) {
                indice=j;
                max = tableau[j];
            }
        }
        printf("%i\n", tableau[indice]);
        max = -1;
        tableau[indice] = -1;
    }
}
```
</details>

---