<!-- docs/_sidebar.md -->

- 0 TPs
  -  [TP1](tp1_2023.md)
  -  [TP2](tp2_2023.md)
- 1 Revue
    - [Introduction](/)
    - [Installation de QT Creator](qtcreator.md)
    - [Introduction au C](c_intro.md)
    - [Convention de noms](c_regle_de_nom.md)
    - [Fonction](c_fonction.md)
      <!-- preprocessor  #include #define -->
    - [Variable](c_variable.md)
    - [printf](c_printf.md)  
    - [scanf](c_scanf.md)  
    - [Exercices printf scanf](ex_printf_scanf.md)
    - [if, terniaire, et switch](c_if.md)
    - [Boucle](c_boucle.md)
    - [Exercices if et boucle](ex_boucle_if.md) <!-- donner un exemple de menu qui retourne des valeurs en démo pour le tp1 -->
    - [TP1](tp1_2023.md)
    - [fin cours 1](fin_cours1.md)   
- 2 Pointeur et structure
    -  [Tableau](c_array.md)                <!-- 0.25h --> 
    -  [string](c_string.md)                <!-- 0.5h -->
    -  [Exercices boucles array et string](ex_boucle_array_string.md) <!-- 1h -->
    -  [Pointeur](c_pointeurs.md)           <!-- 1h -->
    -  [Allocation dynamique](c_allocation_dynamique.md)
    -  [La pile](c_pile.md)                 <!-- 0.5h -->
    -  [Le tas](c_tas.md)                   <!-- 0.5h -->
    -  [fin cours 2](fin_cours2.md)
    -  [Pointeur de pointeur](c_pointeur_pointeur.md) <!-- 0.5h -->
    -  [Structure](c_structure.md)                    <!--  1.5h -->
    -  [Exercices sur les pointeur](ex_pointeurs.md)  <!--  4.0h  sur 2 cours -->
    -  [Pointeurs solution](ex_pointeurs_solution.md)
    -  [fin du cours 3](fin_cours3.md)   <!-- terminer l'excercice pointeur au cours 4, environ 2h de plus -->
    -  [Listes chainées](c_liste_chainee.md)          <!-- 1.0h -->
    -  [fin du cours 4](fin_cours4.md)
    -  [Exercice liste chainées](ex_liste_chainee1.md) <!-- 3h les laisser travailler tout le cours.-->
    -  [Listes chainées et allocation dynamique](c_liste_chainee_alloc_dynamique.md) <!--  1.0h -->
    -  [Exercice liste chainées et allocation dynamique](ex_liste_chainee2.md) <!-- 1h après avoir fait ex1, ils comprennent et c'est moins long -->
    -  [fin du cours 5](fin_cours5.md)

- 3 Les fichiers
   - [Fichiers](c_fichier.md)                     <!--  1.5h -->
   - [Exercices fichiers](ex_fichier.md)          <!--  1.5h -->
   - [Resto](ex_resto.md)                         <!--  1.0h -->
   - [TP2](tp2_2023.md)
   - [Listes chainées et fichiers](c_liste_chainee_alloc_dynamique_fichier.md)  <!-- 0.5h --> <!-- 2024 à déplacer après le premier exercice sur les fichiers -->
   - [Exercices liste chainée et fichier](ex_liste_chainee3.md) 
   <!-- math.h -->    
   <!-- struct  exemple de jeu de carte packt c programming, section 18-->
   <!-- debug -->
