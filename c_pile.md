# Allocation dynamique: la pile <!-- omit in toc -->

# 1. Introduction

La pile (ou "stack" en anglais) est un espace mémoire géré automatiquement par le langage C/C++. Lorsque vous lancez des routines (fonctions C), le compilateur empile automatiquement certaines valeurs sur la pile (adresse de retour, variables locales, arguments, etc.).

Une variable spéciale du processeur (on dit un registre) appelé "pointeur de pile" permet de savoir à quel emplacement les valeurs seront placées sur la pile.

Lorsqu'une routine est appelée:

* l'adresse de retour de l'appel est placée sur la pile et le pointeur de pile est déplacé pour pointer au prochain espace disponible,
* les arguments envoyés à la routine sont placés sur la pile et le pointeur de pile est déplacé pour pointer au prochain espace disponible,
* les variables locales de la routine sont placées sur la pile et le pointeur de pile est déplacé pour pointer au prochain espace disponible.

Lorsque la routine est terminée:
* le pointeur de pile est décrémenté afin d'aller chercher l'adresse de retour,
* l'adresse de retour est stockée afin de permettre à la routine appelante de continuer son exécution,
* La valeur de retour est stockée à la place de l'adresse de retour dans la pile.

Voici une version un peu plus visuelle de cette mécanique. Nous allons exécuter la routine `maRoutine1` dans ce code:

```c
void maRoutine2(int argument1, int argument2)
{
    int variableLocale1 = 10;
    char* variableLocale2 = "Allo";
    printf("Sortie: %s - %d\n", variableLocale2, variableLocale1);
}

void maRoutine1()
{
    int valeur = 4;
    maRoutine2(1, 2);
    maRoutine2(3, valeur);
}
```

Donc, voici l'exécution:

![Pile fonctionnement 1](_media/images_louis/pile_fonctionnement1.png)
![Pile fonctionnement 2](_media/images_louis/pile_fonctionnement2.png)
![Pile fonctionnement 3](_media/images_louis/pile_fonctionnement3.png)
![Pile fonctionnement 4](_media/images_louis/pile_fonctionnement4.png)
![Pile fonctionnement 5](_media/images_louis/pile_fonctionnement5.png)
![Pile fonctionnement 6](_media/images_louis/pile_fonctionnement6.png)
![Pile fonctionnement 7](_media/images_louis/pile_fonctionnement7.png)
![Pile fonctionnement 8](_media/images_louis/pile_fonctionnement8.png)
![Pile fonctionnement 9](_media/images_louis/pile_fonctionnement9.png)
![Pile fonctionnement 10](_media/images_louis/pile_fonctionnement10.png)
![Pile fonctionnement 11](_media/images_louis/pile_fonctionnement11.png)
![Pile fonctionnement 12](_media/images_louis/pile_fonctionnement12.png)
![Pile fonctionnement 13](_media/images_louis/pile_fonctionnement13.png)
![Pile fonctionnement 14](_media/images_louis/pile_fonctionnement14.png)
![Pile fonctionnement 15](_media/images_louis/pile_fonctionnement15.png)
![Pile fonctionnement 16](_media/images_louis/pile_fonctionnement16.png)
![Pile fonctionnement 17](_media/images_louis/pile_fonctionnement17.png)

Il est important de comprendre ici que malgré le fait que, dans l'exemple, les informations semblent effacées de la mémoire (*on dit que l'espace mémoire est `libéré`*), les informations sont toujours, techniquement en mémoire. Ils sont seulement inaccessibles. Donc, la dernière image pourrait ressembler à ceci:

![Pile fonctionnement 18](_media/images_louis/pile_fonctionnement18.png)

Également, cet exemple a été grandement simplifié afin d'éviter d'entrer dans des détails difficiles à comprendre. Il est important de prendre en note que si vous exécutez ce code, il y a peu de chance que la pile contienne exactement ces valeurs lors de l'exécution.

# 2. À noter à propos des valeurs de retour de fonctions

Il est très important de prendre en note qu'étant géré automatiquement, ce qui est placé sur la pile ne devrait pas être utilisé à l'extérieur de la routine qui l'a déclaré. En effet, si je retourne une valeur placée sur la pile, il y a de bonnes chances que cette valeur soit remplacée rapidement par le programme une fois sorti de cette routine.

Par exemple, ce programme devrait afficher "Allo":

```c
char* maRoutine1()
{
    char lTexte[5] = "Allo";
    char* lChaine = lTexte;
    printf("La chaîne: %s\n", lChaine);
}

int main(int aParametresTaille, char* aParametres[])
{
    maRoutine1();
    return 0;
}
```
Par contre, ce programme, qui est très similaire mais utilise une valeur de retour, n'affichera (probablement) pas "Allo":

```c
char* maFonction1()
{
    char lTexte[5] = "Allo";
    return lTexte;
}

int main(int aParametresTaille, char* aParametres[])
{
    char* lChaine = maFonction1();
    printf("La chaîne: %s\n", lChaine);
    return 0;
}
```

Dans `maFonction1` le `return` retourne l'`adresse` du tableau, donc dans un espace mémoire alloué pour la fonction et qui sera disponible pour d'autres fonctions dès que `maFonction1` se termine.  
