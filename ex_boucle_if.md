# Exercices boucle et if <!-- omit in toc -->

Ouvrez votre projet d'exercices et ajoutez les 2 fichiers suivants:

<details>
<summary> serie2.h  </summary>

```c
#ifndef SERIE2_H
#define SERIE2_H

void serie2_ex1();
void serie2_ex2();
void serie2_ex3();
void serie2_ex4();
#endif // SERIE2_H

```
</details>

<details>
<summary> serie2.c</summary>

```c

#include <stdio.h>

void serie2_ex1()
{
/*
 * Codez une fonction pour qu'elle demande un nombre entier
 * Ensuite affichez si ce nombre est pair ou impair
 * Rappelez-vous de l'opérateur modulo %
 */
    int valeur;
    printf("Entrez un nombre: ");
    // complétez la fonction...
    scanf("%i", &valeur);
    

    // ou, avec un ternaire
    char *reponse;
    printf("%s\n", reponse);

    
}

void serie2_ex2()
{
/*
 * Codez une fonction pour qu'elle demande 2 entiers à l'usager dans varA et varB.
 * Si varA est plus grande que varB, alors échangez la valeur de varA et varB en utilisant une troisième variable.
 * Affichez varA et varB (en d'autres mots, les valeurs sont affichées dans l'ordre croissant)
 */
    int varA, varB, varC;
    printf("entrez un premier entier ");
    scanf("%i", &varA);
    // complétez la fonction ...

    printf("varA = %i et varB = %i \n", varA, varB);
}

void serie2_ex3()
{
/*
 * Codez une fonction calculant la factoriel d'un nombre.
 * On se rappel que la factoriel de n (n!) est 1*2*3*...*n
 * Utilisez une boucle for
 * Vous n'avez pas à gérer les mauvaises valeurs (-1, 0, a ...)
 */

    int varA, factoriel = 1;
    printf("entrez un entier ");
    scanf("%i", &varA);
    // complétez la fonction ...

    printf("factoriel de %i = %i\n", varA, factoriel);
}

void serie2_ex4()
{
/*
 * Refaire la factorielle, mais en utilisant une boucle while
 */
    int varA, factoriel = 1;
    printf("entrez un entier ");
    scanf("%i", &varA);
    // complétez la fonction ...

    printf("factoriel de %i = %i\n", varA, factoriel);
}

 ```

</details>


Et ajoutez un include dans `main.c` pour `serie2.h`


Complétez le code pour la série2. 

Comme pour la série 1, les réponses sont ci-bas. Ne les regardez qu'après avoir essayé de trouver la réponse. 

<details>
<summary> Serie2_ex1 solution  </summary>

```c
void serie2_ex1()
{
/*
 * Codez une fonction pour qu'elle demande un nombre entier
 * Ensuite affichez si ce nombre est pair ou impair
 * Rappelez-vous de l'opérateur modulo %
 */
    int valeur;
    printf("Entrez un nombre: ");
    // complétez la fonction...
    scanf("%i", &valeur);
    if(valeur%2 == 0) {
        printf("pair\n");
    } else {
        printf("impair\n");
    }

    // ou, avec un ternaire
    char *reponse;
    reponse = valeur%2 == 0 ? "pair" : "impair";
    printf("%s\n", reponse);

    // ou

    printf("%s\n", valeur%2 == 0 ? "pair" : "impair");
}

```
</details>
<details>
<summary> Serie2_ex2 solution  </summary>

```c
void serie2_ex2()
{
/*
 * Codez une fonction pour qu'elle demande 2 entiers à l'usager dans varA et varB.
 * Si varA est plus grande que varB, alors échangez la valeur de varA et varB en utilisant une troisième variable.
 * Affichez varA et varB (en d'autres mots, les valeurs sont affichées dans l'ordre croissant)
 */
    int varA, varB, varC;
    printf("entrez un premier entier ");
    scanf("%i", &varA);
    // complétez la fonction ...
    printf("entrez un deuxième entier ");
    scanf("%i", &varB);
    if(varA > varB) {
        varC= varA;
        varA=varB;
        varB=varC;
    }
    printf("varA = %i et varB = %i \n", varA, varB);
}
```

</details>
<details>
<summary> Serie2_ex3 solution  </summary>

```c
void serie2_ex3()
{
/*
 * Codez une fonction calculant la factoriel d'un nombre.
 * On se rappel que la factoriel de n (n!) est 1*2*3*...*n
* Utilisez une boucle for
 */

    int varA, factoriel = 1;
    printf("entrez un entier ");
    scanf("%i", &varA);
    // complétez la fonction ...

    for(int i = 1; i<= varA; i++) {
        factoriel = factoriel*i;
    }

    printf("factoriel de %i = %i\n", varA, factoriel);
}

```

</details>
<details>
<summary> Serie2_ex4 solution  </summary>

```c
void serie2_ex4()
{
/*
 * Refaire la factorielle, mais en utilisant une boucle while
 */
    int varA, factoriel = 1;
    printf("entrez un entier ");
    scanf("%i", &varA);
    // complétez la fonction ...
    int i = 1;
    while(i<=varA) {
        factoriel = factoriel * i;
        i++;
    }

    printf("factoriel de %i = %i\n", varA, factoriel);
}


```

</details>
