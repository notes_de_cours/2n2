# if, ternaire, et switch <!-- omit in toc -->

# if

La clause `if` est identique à celle de Java. 

```c
if (condition) {
   // instruction(s)
}
else if(condition) {
   // instruction(s)
}
else if (condition) {
   // instruction(s)
}
.
.
else {
   // instruction(s)
}
```

Les opérateurs de comparaison sont aussi les mêmes qu'en Java.

    == (est égal à)
    != (n'est pas égal à)
    > (est plus grand que)
    >= (est plus grand ou égal à)
    < (est plus petit que)
    <= (est plus petit ou égal à)




# ternaire

Si on a un test simple à faire, il est aussi possible d'utiliser l'opérateur ternaire:

`?:`

Exemple:
```c
int a,b,c;
...
if (a<b) {
    c = 10;
} else {
    c = 5;
}
```
est équivalent à :
```c
int a,b,c;
...
c = a<b?10:5;
```

# Operateurs

En plus des opérateurs de comparaison, il y a des opérateurs logique, d'assignation, et des opérateurs binaires. 

Je vous conseil de feuilleter de la documentation sur le web, tel que cette [page](https://www.tutorialspoint.com/cprogramming/c_operators.htm) afin de mieux connaitre tous ces opérateurs.

# switch

Tout comme en Java, lorsqu'on doit vérifier plusieurs fois la même valeur, il est possible d'utiliser un `switch` au lieu d'une série de `if`. 

```c
switch(expression)
{    
case valeur1:    
 //code;    
 break;  //optionel  
case value2:    
 //code;    
 break;  //optionel  
......    
    
default:     
 //code à exécuter si aucune valeur ne correspond;    
}   
```
À noter:
- l'expression doit toujours être un entier ou un char.
- une expression de comparaison retourne 0 si faux, 1 si vrai.

exemple avec une expression de comparaison (booléen)
```c
switch( 1==2){
    case 0: printf("0\n");break;
    case 1: printf("1\n"); break;
    default: printf("autre chose\n");
}
```
Ce code affichera 0 car 1 n'est pas égal 2.