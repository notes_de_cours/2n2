# Exercices supplémentaires sur les listes chainées et fichier <!-- omit in toc -->


Pour cet exercice, vous devez sauvegarder dans un fichier et recharger la structure suivante:

```c
#ifndef STRUCTURES_H
#define STRUCTURES_H
#define LONGUEUR_CHAMPS 30

typedef struct article {
    char description[LONGUEUR_CHAMPS];
    float prix;
} article;


typedef struct articleEnInventaire articleEnInventaire;
struct articleEnInventaire{
    article *ptrArticle;
    articleEnInventaire *suivant;
};

// le début de la liste chainée
typedef struct inventaire {
    articleEnInventaire *premier;
    int taille;
} inventaire;

#endif // STRUCTURES_H
```

Vous devez compléter ce code, incluant les fonctions de libération de mémoire :


```c
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "structures.h"


void sauvegarderArticles(inventaire *aInventaire)
{
    FILE * lFichier = fopen("inventaire.txt", "wb");
    articleEnInventaire *lArticleInventaire = aInventaire->premier;

    // insérez du code ici
   
    fclose(lFichier);

}

void chargerArticles(inventaire *aInventaire)
{
    FILE * lFichier = fopen("inventaire.txt", "rb");
    int continuer = 0;

    do {
        
        // insérez du code ici

    } while(continuer);
    fclose(lFichier);
}
void afficherArticles(inventaire *aInventaire)
{
   articleEnInventaire *lArticleInventaire = aInventaire->premier;

   for(int i = 0; i<aInventaire->taille; i++ ) {
       article *lArticle =lArticleInventaire->ptrArticle;
       printf("%s  %5.2f \n", lArticle->description, lArticle->prix);
       lArticleInventaire = lArticleInventaire->suivant;
   }
}

void viderInventaireSurLaPile(inventaire *aInventaire)
{
    //inserez du code ici
}

void viderInventaireSurLeTas(inventaire *aInventaire)
{
    //inserez du code ici
}

int main()
{
    article lesArticles[4] = { //sur la pile
        {"vis",  0.24},
        {"clou", 0.10},
        {"ecrou", 0.01},
        {"tournevis", 9.99}
    };

    inventaire lInventaire; // sur la pile
    srand(time(NULL));
    int r = rand();
    printf("%i\n", r);
    articleEnInventaire *lArticleInventairePrecedent = NULL;
    for(int i = 0; i<( r % 10) + 5; i++) {
        articleEnInventaire *lArticleInventaire = calloc(1,sizeof(articleEnInventaire)); // sur le tas
        lArticleInventaire->suivant = NULL;
        lArticleInventaire->ptrArticle = &lesArticles[rand()%4];
        if(lArticleInventairePrecedent) {
            lArticleInventairePrecedent->suivant=lArticleInventaire;
            lInventaire.taille++;
        } else {
            lInventaire.premier = lArticleInventaire;
            lInventaire.taille = 1;
        }
        lArticleInventairePrecedent = lArticleInventaire;

    }

    afficherArticles(&lInventaire);
    sauvegarderArticles(&lInventaire);
    viderInventaireSurLaPile(&lInventaire); // ATTENTION: ici, les articles sont sur la pile.
    printf("-----------\n");
    inventaire lInventaire2; 
    chargerArticles(&lInventaire2);
    afficherArticles(&lInventaire2);
    viderInventaireSurLeTas(&lInventaire2); // ATTENTION: ici, les articles sont sur le tas. 

}

```

!> Exceptionnelement, la solution n'est pas donnée, mais vous pouvez faire vérifier votre solution. 

<p>
<!--

<details>
<summary>solution</summary>

<details>
<summary>sauvegarde</summary>

```c
void sauvegarderArticles(inventaire *aInventaire)
{
    FILE * lFichier = fopen("inventaire.txt", "wb");
    articleEnInventaire *lArticleInventaire = aInventaire->premier;

    for(int i = 0; i<aInventaire->taille; i++) {
        article *lArticle =lArticleInventaire->ptrArticle;
        fwrite( lArticle, sizeof(article), 1, lFichier);
        lArticleInventaire = lArticleInventaire->suivant;
    }
    fclose(lFichier);

}
```
</details>

<details>
<summary>lecture et free</summary>

```c
void chargerArticles(inventaire *aInventaire)
{
    FILE * lFichier = fopen("inventaire.txt", "rb");
    int continuer = 0;
    articleEnInventaire *lArticleInventairePrecedent = NULL;

    do {
        article *lArticle = calloc(1, sizeof(article));
        continuer = fread(lArticle, sizeof(article), 1, lFichier);
        if(continuer ) {
            articleEnInventaire *lArticleInventaire = calloc(1,sizeof(articleEnInventaire)); // sur le tas
            lArticleInventaire->suivant = NULL;
            lArticleInventaire->ptrArticle = lArticle;
            if(lArticleInventairePrecedent) {
                lArticleInventairePrecedent->suivant=lArticleInventaire;
                aInventaire->taille++;
            } else {
                aInventaire->premier = lArticleInventaire;
                aInventaire->taille = 1;
            }
            lArticleInventairePrecedent = lArticleInventaire;
        }

    } while(continuer);
    fclose(lFichier);
}

void viderInventaireSurLaPile(inventaire *aInventaire)
{
    articleEnInventaire *lArticleInventaire = aInventaire->premier;
    articleEnInventaire *articleSuivant;
    for(int i = 0; i<aInventaire->taille; i++ ) {
        //les articles sont sur la pile, il ne faut donc pas les libérer
        articleSuivant = lArticleInventaire->suivant;
        free(lArticleInventaire);
        lArticleInventaire = articleSuivant;
    }
}

void viderInventaireSurLeTas(inventaire *aInventaire)
{
    articleEnInventaire *lArticleInventaire = aInventaire->premier;
    articleEnInventaire *articleSuivant;
    for(int i = 0; i<aInventaire->taille; i++ ) {
        free(lArticleInventaire->ptrArticle);
        articleSuivant = lArticleInventaire->suivant;
        free(lArticleInventaire);
        lArticleInventaire = articleSuivant;
    }
}
```
</details>

</details>
)
--> </p>