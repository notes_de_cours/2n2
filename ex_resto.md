# Exemple complet de gestion de mémoire, structure, et fichier <!-- omit in toc -->

Cet exercice servira à mettre ensemble toutes les notions que nous avons apprises jusqu'à maintenant. 

De plus, le fichier qui est généré vous servira dans le TP#2.

Nous allons concevoir un logiciel permettant de faire la gestion du menu d'un restaurant. 

Il contient différents types de gestion de mémoire, de gestion de structure, ainsi que l'écriture et lecture d'une structure sur disque. Si vous le comprenez, vous ne devriez pas avoir trop de difficultés avec le TP#2


# Les structures

À la base, nous avons les items pouvant être commandés par un client. Pour nos besoins, nous auront simplement le titre de l'item (ex: club sandwich) et son prix.

```c
typedef struct item {
    char titre[LONGUEUR_TITRE];
    float prix;
} item;
```

Ces items seront regroupés pour former ce qui est offert pour un repas (ex: le diner, le souper, le dessert ...). Nous allons entreposer ces groupes dans un tableau d'items, contenant aussi le nom du repas et le nombre d'items dans le tableau. 

```c
typedef struct repas {
    char titre[LONGUEUR_TITRE];
    item *listeItem; //un tableau consécutif d'items
    int nombreItem;
} repas;
```

Et finalement, ces repas formeront le menu. Pour faire changement, cette fois-ci c'est un tableau de pointeur vers les repas que nous utiliserons.

```c
typedef struct menu {
    repas **listeRepas; //un tableau de pointeur vers des repas.
    int nombreRepas;
} menu;
```

![structure resto](_media/structure_resto.png ':size=400x800')

Nous allons commencer par simuler la création d'un menu au complet. Nous utiliserons les mêmes structures que dans la solution proposée, et utiliserons les mêmes noms de variables afin que vous puissiez vous y retrouver plus facilement. 

Cette simulation fait exactement les mêmes étapes que la solution complète. 

Voici donc la solution complète. Nous allons nous concentrer sur la fonction `simulation()`. 

Une fois celle-ci bien comprise, nous examinerons le code au complet. 

<details>
<summary>gestionRepas.h</summary>

```c
#ifndef GESTIONREPAS_H
#define GESTIONREPAS_H
#include "structurerepas.h"

void creerItem(repas *aRepas);
void creerMenu(menu *aMenu);
void afficherMenu(menu aMenu);
void sauvegarderMenu(menu aMenu, char *aNomFichier);
void chargerMenu(menu *aMenu, char *aNomFichier);
void viderMenu(menu *aMenu);
#endif // GESTIONREPAS_H


```
</details>

<details>
<summary>structurerepas.h</summary>

```c
#ifndef STRUCTUREREPAS_H
#define STRUCTUREREPAS_H
#define LONGUEUR_TITRE 20

// un item dans un repas
typedef struct item {
    char titre[LONGUEUR_TITRE];
    float prix;
} item;

// un repas contenant une série d'item
typedef struct repas {
    char titre[LONGUEUR_TITRE];
    item *listeItem; //un tableau consécutif d'items
    int nombreItem;
} repas;

// un repas pour la sauvegarde sur disque car item *listeItem ne doit pas être sauvegardé
typedef struct repasFichier {
    char titre[LONGUEUR_TITRE];
    int nombreItem;
} repasFichier;

//un menu contenant une série de repas
typedef struct menu {
    repas **listeRepas; //un tableau de pointeur vers des repas.
    int nombreRepas;
} menu;

#endif // STRUCTUREREPAS_H


```
</details>

<details>
<summary>gestionRepas.c</summary>

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "structurerepas.h"


/**
 * @brief création d'un item qui sera mis dans un repas.
 *
 * La liste des items est un tableau d'item.
 *
 * @param aRepas le repas dans lequel l'item sera ajouté
 */
void creerItem(repas *aRepas){
    char continuer[3];
    char prix[10];
    do {
        if(!aRepas->listeItem) {
            aRepas->listeItem = calloc(1, sizeof(item));
        } else {
            aRepas->listeItem = realloc(aRepas->listeItem, sizeof(item)*(aRepas->nombreItem+1));
        }

        printf("Titre de l'item: ");
        fgets( aRepas->listeItem[aRepas->nombreItem].titre,
                sizeof aRepas->listeItem[aRepas->nombreItem].titre, stdin);
        //fgets capture le \n
        //cette passe-passe remplace le  \n par un 0
        aRepas->listeItem[aRepas->nombreItem].titre[strcspn(aRepas->listeItem[aRepas->nombreItem].titre, "\n")] = 0;

        printf("Prix de l'item: ");
        fgets( prix, sizeof prix, stdin );
        aRepas->listeItem[aRepas->nombreItem].prix = atof(prix);
        aRepas->nombreItem++;
        printf("un autre item (o/n): ");
        fgets(continuer, sizeof continuer, stdin);

    } while(continuer[0] == 'o');
}

/**
 * @brief création d'un menu en y insérant des repas
 *
 * La liste des repas est un tableau de pointeur vers les repas.
 *
 * @param aMenu le menu dans lequel les repas sont insérés
 */
void creerMenu(menu *aMenu){
    repas *lRepas;
    char continuer[3];
    do {
        lRepas = calloc(1, sizeof(repas));
        printf("Titre du menu: ");
        fgets(lRepas->titre, sizeof lRepas->titre, stdin);
        lRepas->titre[strcspn(lRepas->titre, "\n")] = 0;
        if(!aMenu->listeRepas) {
            aMenu->listeRepas = calloc(1, sizeof(repas*));
        } else {
            aMenu->listeRepas = realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
        }
        aMenu->listeRepas[aMenu->nombreRepas] = lRepas;
        aMenu->nombreRepas++;
        creerItem(lRepas);
        printf("un autre menu (o/n): ");
        fgets(continuer, sizeof continuer, stdin);
    } while(continuer[0] == 'o');

}

/**
 * @brief affichage d'un menu au complet
 * @param aMenu le menu à afficher
 */
void afficherMenu(menu aMenu) {
    for(int i= 0; i<aMenu.nombreRepas; i++) {
        printf("%i %s\n", i+1, aMenu.listeRepas[i]->titre);
        for(int j = 0; j< aMenu.listeRepas[i]->nombreItem; j++){
            printf("   %s %3.2f\n", aMenu.listeRepas[i]->listeItem[j].titre,aMenu.listeRepas[i]->listeItem[j].prix );
        }
    }
}

/**
 * @brief sauvegarde sur disque du menu au complet
 *
 * par simplicité, le fichier est écrasé à chaque écriture
 *
 * @param aMenu le menu à sauvegarder
 * @param aNomFichier le nom du fichier dans lequel sauvegarder
 */
void sauvegarderMenu(menu aMenu, char *aNomFichier) {
    FILE *lFichier = fopen(aNomFichier, "w");
    repasFichier lRepasASauvegarder;
    for(int i= 0; i<aMenu.nombreRepas; i++) {
        strcpy(lRepasASauvegarder.titre, aMenu.listeRepas[i]->titre);
        lRepasASauvegarder.nombreItem =  aMenu.listeRepas[i]->nombreItem;
        fwrite(&lRepasASauvegarder, sizeof(repasFichier),1, lFichier);
        for(int j = 0; j<aMenu.listeRepas[i]->nombreItem; j++) {
            fwrite(&aMenu.listeRepas[i]->listeItem[j], sizeof(item), 1, lFichier);
        }
    }
    fclose(lFichier);
}

/**
 * @brief lecture du menu à partir du disque
 * Si le menu contenait déjà des repas, les nouveaux sont ajoutés à la fin
 *
 * @param aMenu le menu dans lequel charger les repas.
 * @param aNomFichier le nom du fichier dans lequel lire
 */
void chargerMenu(menu *aMenu, char *aNomFichier) {
    FILE *lFichier = fopen(aNomFichier, "r");
    repasFichier lRepasACharger;
    repas *lRepasEnMemoire;
    item *lTableauItemsEnMemoire;
    int continuer = 0;
    do {
        continuer = fread(&lRepasACharger, sizeof(repasFichier),1, lFichier);
        if(continuer) {
            lRepasEnMemoire = calloc(1, sizeof(repas));
            strcpy(lRepasEnMemoire->titre, lRepasACharger.titre);
            lRepasEnMemoire->nombreItem = lRepasACharger.nombreItem;

            //le nombre d'item est connue. Le tableau peut donc être dimensionné au complet
            lTableauItemsEnMemoire = calloc(lRepasEnMemoire->nombreItem, sizeof(item));
            for(int i=0; i<lRepasACharger.nombreItem; i++) {
                fread(&lTableauItemsEnMemoire[i], sizeof(item),1, lFichier);
            }
            lRepasEnMemoire->listeItem = lTableauItemsEnMemoire;
            if(!aMenu->listeRepas) {
                aMenu->listeRepas = calloc(1, sizeof(repas*));
            } else {
                aMenu->listeRepas = realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
            }
            aMenu->listeRepas[aMenu->nombreRepas] = lRepasEnMemoire;
            aMenu->nombreRepas++;
        }
    } while(continuer);

    fclose(lFichier);
}

/**
 * @brief effacement complet d'un menu
 * @param aMenu le menu à effacer
 */
void viderMenu(menu *aMenu) {

    for(int i =0; i<aMenu->nombreRepas; i++) {
        repas *lRepas = aMenu->listeRepas[i];
        free(lRepas->listeItem);
        free(lRepas);
    }
    free(aMenu->listeRepas);
    aMenu->listeRepas = NULL;
    aMenu->nombreRepas = 0;
    //free(aMenu); pas besoin car sur la pile
}


```
</details>

<details>
<summary>main.c</summary>

```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "structurerepas.h"
#include "gestionRepas.h"


void simulation(){
    menu lMenu; //sur la pile
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;

    //appel de creerMenu(&lMenu) afin de remplir le menu
    menu *aMenu = &lMenu; //dans creerMenu on a donc un menu *

    repas *lRepas;
    lRepas = calloc(1, sizeof(repas)); //sur le tas
    strcpy(lRepas->titre,"menu midi"); //simule le scanf
    aMenu->listeRepas = calloc(1, sizeof(repas*)); //creation du tableau de pointeur. Notez repas*
    aMenu->listeRepas[0] = lRepas; //pointe le premier pointeur du tableau vers le repas
    aMenu->nombreRepas++; //on a un repas de plus

    //appel de creerItem(lRepas) afin d'ajouter les items
    //dans ce cas, lRepas est déjà un pointeur, donc pas besoin de &lRepas
    repas *aRepas = lRepas;
    aRepas->listeItem = calloc(1, sizeof(item)); //creation du tableau d'item. Notez item n'a pas de *
    strcpy(aRepas->listeItem[0].titre, "soupe"); //simule le scanf
    aRepas->listeItem[0].prix = 3.99;
    aRepas->nombreItem++; //on a un item de plus


    // et voila, on a maintenant un menu avec un repas et un item
    afficherMenu(lMenu);
}

int main()
{
    //simulation();
    //exit(0);
    menu lMenu;
    char *lNomFichier = "menu.txt";
    char entree[10];
    int choix =0;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    do {
        printf("1) ajouter un menu\n");
        printf("2) afficher un menu\n");
        printf("3) sauvegarder un menu\n");
        printf("4) charger un menu\n");
        printf("5) vider le menu\n");
        printf("0) sortir\n");
        printf("votre choix: ");
        fgets(entree, sizeof entree, stdin);
        choix= atoi(entree);

        if(choix == 1) {
            creerMenu(&lMenu);

        } else if(choix == 2) {
            afficherMenu(lMenu);
        } else if(choix == 3) {
            sauvegarderMenu(lMenu, lNomFichier);
        } else if(choix == 4) {
            chargerMenu(&lMenu, lNomFichier);
        } else if(choix == 5) {
            viderMenu(&lMenu);
        }
    } while(choix != 0);


    viderMenu(&lMenu);
}


```
</details>