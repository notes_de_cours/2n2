## Installation de QT-creator

Notre outil de programmation sera QT-creator. 

**QT** (*prononcé Q.T. ou "cute"*) est un logiciel multi-plateforme utilisé pour créer des applications ayant une interface graphique, ainsi que des applications fonctionnants sur plusieurs types de systèmes d'exploitation ou de plateformes matérielles (*tel que Linux, Windos, macOs, Android, ou des systèmes embarqués*) avec peu ou pas du tout de changement dans le code. 

**QT Creator** est un environnement graphique de développement (IDE) facilitant l'utilisation de QT

Nous allons utiliser la version open source de QT Creator. 
Pour en faire l'installation, veuillez suivre les instructions suivantes:


```pdf
_media/installationQT.pdf
```