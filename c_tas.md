# Allocation dynamique: le tas <!-- omit in toc -->

# 1. Introduction

Le tas permet l'allocation dynamique de la mémoire. Il s'agit d'un espace mémoire entièrement géré par le programmeur. Il est très important de comprendre que les espaces mémoire réservés dans le tas ne seront jamais libérés si le programmeur ne le libère pas à un endroit dans le code.

# 2. Pourquoi avoir un espace d'allocation dynamique?

Supposons que vous ayez une fonction qui doit produire une série de nombres, mais que vous ne sachiez pas d'avance combien de ces nombres l'usager va demander. Alors si vous désirez allouer un tableau assez grand pour contenir ces nombres, à quelle taille devriez vous le dimensionner?

L'allocation dynamique résout ce problème: vous demandez à l'usager combien il en veut, et vous allouez le tableau de cette taille dynamiquement. 

Ces variables (pas nécessairement un tableau) allouées dynamiquement sont mises sur le tas. 

# 3. Les routines 
C met à notre disposition 3 routines qui permettent de gérer le tas:

* [calloc](https://devdocs.io/c/memory/calloc): Permet de réserver un espace mémoire;
* [free](https://devdocs.io/c/memory/free): Permet de libérer un espace mémoire;
* [realloc](https://devdocs.io/c/memory/realloc): Permet de modifier la taille d'un espace mémoire déjà réservé.

Ces routines nécessitent l'utilisation de l'entête `stdlib.h`.

Voici plus de détail sur ces fonctionnalités:


## 3.1. [calloc](https://devdocs.io/c/memory/calloc)

<!-- !> Attention: Étant donné que nous utilisons `QT creator` comme IDE, le compilateur est pour le C++. Il faut dont `caster` le type de retour de calloc. Il faut donc écrire `x = (int *) calloc(nombre, taille);` (remplacez `int *` par le type de la variable). Si vous codez purement en `c`, ce cast n'est pas nécessaire. --> 

L'instruction `calloc` permet de réserver une plage d'adresse mémoire d'une taille bien définie et de retourner son adresse. La signature de `calloc` est la suivante:

```C
void* calloc(int nombre, int taille)
```

* L'argument `nombre` indique le nombre d'espaces à réserver,
* L'argument `taille` indique la taille de chaque espace mémoire à réserver,
	* On utilise généralement l'instruction `sizeof(type)` pour connaître la taille d'un type.
* La valeur de retour est l'adresse de l'espace réservé.
	* le type de pointeur `void*` peut représenter un pointeur vers n'importe quel type.
	*  À noter que cette routine peut retourner `NULL` s'il n'y a aucun espace mémoire disponible dans le tas.

Il est à noter qu'il arrive que les programmeurs utilisent [malloc](https://devdocs.io/c/memory/malloc) au lieu de `calloc`. Les deux instructions se comportent de la même manière, à la différence que le `calloc` est plus sure puisqu'il initialise tous les espaces mémoire à `0` tandis que `malloc` laisse l'espace mémoire inchangé.

## 3.2. [free](https://devdocs.io/c/memory/free)

L'instruction `free` permet de libérer un espace mémoire qui a préalablement été initialisé avec `calloc` (ou `malloc`). La signature de `free` est la suivante:

```C
void free(void* adresse)
```
L'argument `adresse` est l'adresse retournée par `calloc` (ou `malloc`).

Exemple:
```c
int *x = (int *) calloc(5, sizeof(int));
...
free(x); //x contient l'adresse de l'espace mémoire à libérer. 
```

!> Il est a noter qu'un tableau est libéré en entier. On ne doit pas libérer chacun des éléments une par un. 


### 3.2.1. Exemple #1

Exécutez le code suivant:

```c
#include <stdio.h>
#include <stdlib.h>
int main(int aParametresTaille, char* aParametres[])
{
    const int taille = 10;
    int x = 1;
    int *px = NULL;
    int *var1 =  calloc(taille, sizeof(int));
    var1[0] = 10;
    int *var2 = calloc(1, sizeof(int));
    *var2 = 20;
    int *var3 = calloc(1, sizeof(int));
    var3[0] = 30;

    printf("sizeof(int) %lld\n", sizeof(int ));
    printf("sizeof(int *) %lld\n", sizeof(int *));

    printf("x                   %u\n", &x);
    printf("px                  %u\n", &px);
    printf("var1    %u, %u\n", var1, &var1);
    for(int i=0; i<taille; i++) {
        printf("var1[%i] %u\n", i, &var1[i]);
    }
    printf("var2    %u, %u\n", var2, &var2);
    printf("var3    %u, %u\n", var3, &var3);

    free(var1);
    int *var4 = calloc(1, sizeof(int));
    printf("var4    %u, %u\n", var4, &var4);

    return 0;
}
```
!> Notez que pour simplifier la lecture, les adresses sont affichées en nombre entier positif. Normalement, `%p` devrait être utilisé au lieu de `%u`.

**À noter:**
* les adresses des variables (la colonne de droite) sont mises sur la pile;
* les adresses de ce qui est alloué par calloc (colonne de gauche) sont mises sur le tas;
* les adresses sur la pile croissent généralement par 4 ou 8 ( 4 * 8 = 32bit, 8 * 8= 64bits). N'oubliez pas qu'une variable est un pointeur vers un espace mémoire. Il est donc normale qu'elle prenne l'espace d'un `int *`;
* les adresses sur le tas croissent selon la taille des éléments. Ici, c'est un tableau de `int`; 
* il peut arriver qu'il y ait des trous (vous devriez en voir un entre var1[9] et var2). C'est du à la taille des éléments;
* il est possible que l'espace libéré par le `free` soit réutilisé immédiatement ou plus tard selon la taille de la prochaine allocation et selon d'autres paramètres;
* var1 c'est le contenu de la variable, donc l'adresse du début du tableau sur le tas;
* &var1 c'est l'adresse de cette variable sur la pile; 
* var1 ou &var1[0] c'est la même chose.


### 3.2.2. Exemple 2 

Voici maintenant un exemple d'une vrai utilisation.

Vous vous rappelez surment de notre cher ami Fibonnaci...

Le problème que résout ce code est le suivant: comment générer un nombre quelconque d'élément de la suite de Fibonnaci?


```c
#include <stdio.h>
#include <stdlib.h>


int* fibonnaci(int aTaille)
{
    int i, lNombre1 = 0, lNombre2 = 1;
    int* lTableauEntier = NULL;
    if (aTaille > 0) {
        lTableauEntier = calloc(aTaille, sizeof(int));
        if (lTableauEntier) {
            lTableauEntier[0] = lNombre1;
            for(i = 1; i < aTaille; i = i + 1) {
                lTableauEntier[i] = lNombre2;
                lNombre2 = lNombre1 + lNombre2;
                lNombre1 = lTableauEntier[i];
            }
        }
    }
    return lTableauEntier;
}

int main(int aParametresTaille, char* aParametres[])
{
    int i;
    const int TAILLE = 10;
    int* lFibonnaci = fibonnaci(TAILLE);
    if (lFibonnaci) {
        for (i = 0; i < TAILLE; i = i + 1){
            printf("Le nombre %d de Fibonnaci: %d\n", i, lFibonnaci[i]);
        }
    } else {
        printf("Ne peut obtenir les nombres de Fibonnaci.\n");
    }
    free(lFibonnaci);
    return 0;
}
```

## 3.3. [realloc](https://devdocs.io/c/memory/realloc)

L'instruction `realloc` permet de changer la taille d'un espace mémoire précédemment alloué avec `calloc` (ou `malloc`). Il est important de comprendre qu'il est possible que l'adresse de la plage mémoire change avec l'utilisation de `realloc`. Il est donc nécessaire d'utiliser, à partir de son utilisation, l'adresse retournée par `realloc`. Également, dans tous les cas, il n'est nécessaire de faire qu'un seul `free` sur la dernière adresse utilisée. En d'autres mots, si l'adresse de l'espace mémoire change, l'ancien espace mémoire est automatiquement libéré. Voici la signature de la routine:

<!-- pour 2024 changer l'exemple pour que le realloc soit dans fibonaci au lieu du main. Dans le main, on se demande pourquoi on realloc au lieu de juste free et calloc.  -->


```C
void* realloc(void* pointeur, int taille)
```
* L'argument pointeur représente le pointeur originalement retourné par `calloc`, `malloc` ou par une précédente utilisation de `realloc`;
* La taille est le nombre d'octets à utiliser dans le nouvel espace mémoire;
	* Contrairement à `calloc`, `realloc` ne prend pas de nombre d'éléments et de tailles d'élément. Si nous voulons utiliser cet argument avec un nombre d'éléments et une taille d'élément, il est nécessaire de faire: `nombre*taille`.
* La valeur de retour est l'adresse de l'espace mémoire réservé.
	* Si le système a pu utiliser la même adresse mémoire, le pointeur retourné est égal au pointeur en argument. Sinon, le pointeur change.
	* Le pointeur retourné sera le pointeur à utiliser dans le `free`.
	* L'espace utilisé par le pointeur original est libéré si le pointeur retourné est différent de l'original. Il ne faut donc pas faire un `free` de l'ancien pointeur.

Voici un exemple d'utilisation de `realloc`:

```c
#include <stdio.h>
#include <stdlib.h>

void fibonnaci(int* aTableauEntier, int aTaille)
{
    int i, lNombre1 = 0, lNombre2 = 1;
    if (aTaille > 0) {
        if (aTableauEntier) {
            aTableauEntier[0] = lNombre1;
            for(i = 1; i < aTaille; i = i + 1) {
                aTableauEntier[i] = lNombre2;
                lNombre2 = lNombre1 + lNombre2;
                lNombre1 = aTableauEntier[i];
            }
        }
    }
}

void afficherTableau(int* aTableau, int aTaille) {
    int i;
    for (i = 0; i < aTaille; i = i + 1) {
        if (i == 0) {
            printf("%d", aTableau[i]);
        } else {
            printf(", %d", aTableau[i]);
        }
    }
}

int main(int aParametresTaille, char* aParametres[])
{
    int* lTableauEntier = NULL;
    int lEntree = -1;
    while(lEntree != 0) {
        printf("Entrez le nombre d'éléments de Fibonnaci à générer (0 pour sortir): ");
        scanf("%d", &lEntree);
        if (lEntree > 0) {
            if (lTableauEntier) { // si le tableau a été alloué précédemment
                lTableauEntier = realloc(lTableauEntier, lEntree * sizeof(int));
            } else { // première fois que le tableau est alloué. 
                lTableauEntier = calloc(lEntree, sizeof(int));
            }
            if (lTableauEntier) {
                fibonnaci(lTableauEntier, lEntree);
                afficherTableau(lTableauEntier, lEntree);
                printf("\n");
            } else {
                printf("Le tableau n'a pas pu être créé");
            }
        } else if (lEntree != 0) {
            printf("Le nombre doit être positif.");
        }
    }
    if (lTableauEntier) { //il pourrait être NULL si l'usager entre 0 la première fois
        free(lTableauEntier);
    }
}
```


