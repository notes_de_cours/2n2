# Allocation dynamique de la mémoire <!-- omit in toc -->

# 1. Gestion manuelle de la mémoire

Il est important de comprendre que le C est un langage sans ramasse-miette (garbage-collector). Il est donc important de comprendre comment la mémoire est traitée par le système.

La plage mémoire virtuelle d'un processus est représentée comme ceci:


![Plage mémoire d'un processus](_media/images_louis/memoire_processus.png)

Dans ce schéma, les flèches correspondent au sens que la pile et le tas grandissent lorsqu'une nouvelle valeur est nécessaire. 

!> Notez que selon l'architecture du processeur et/ou du SE, ces flèches pourraient être inversées. Ne vous étonnez donc pas que les valeurs montent ou descendent. L'important est que la direction de la pile sera toujours l'inverse de celle du tas. 

Lorsque les flèches se croisent (lorsque la pile et le tas prennent toute la mémoire disponible), on obtient ce que nous appelons un "Stack Overflow".



