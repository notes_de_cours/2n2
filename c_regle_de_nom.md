# Convention pour les noms <!-- omit in toc -->

!> Vous trouverez les règles de nommage pour toutes les notions utilisées dans ce cours. Certaines notions seront vues plus tard. 

# 1. Les lois

Dans le cadre de ce cours, il va y avoir des lois et des règles à suivre pour nommer les différents composants de votre code. 

Le non-respect des lois suivantes entrainera automatiquement une perte de points. 

**Loi #1**: avoir un nom significatif. Avant de vous concentrer sur les détails du nom de la variable, fonctions, ou autre (*la syntaxe*),  vous devriez commencer par bien exprimer à quoi sert cette chose (*sémantique*). Ensuite, prenez 2 secondes pour utiliser les bonnes règles de syntaxe. 

Le nommage est une notion très importante en informatique. Dans le passé, les gens croyaient que de connaitre le vrai nom de quelqu'un leur donnait des pouvoirs magiques sur cette personne.

Si vous trouvez le vrai nom pour une fonction ou toute autre notion utilisée dans votre code, alors vous vous donnez à vous et, surtout, à tous ceux qui vous suivront, un vrai pouvoir sur ce code. 

Un nom est le résultat d'un long processus de réflexion sur l'environnement dans lequel cet objet réside. Seul un.e programmeur.e ayant des connaissances profondes du système pourra donner un nom répondant vraiment au rôle de cette notion. Si le nom est approprié, le rôle et les associations de cette notion avec les autres seront (*plus*) claires et (*plus*) faciles à comprendre. 

Si vous ne trouvez pas un meilleur nom que *varA*, *faireLeCalcul*, *unEntier*; alors vous devriez y réfléchir un peu encore. 

**Loi #2**: Les noms doivent être en français :)

**Loi #2.5**: Bien que le nom soit en français, il ne doit **PAS** comporter d'accent. 

**Loi #3**: Le langage `C` est sensible à la casse, c.à.d. toto et Toto sont deux variables différentes. Vous ne devriez jamais avoir deux choses ayant le même nom à l'exception de la casse. 

**Loit #4**: Si vous adoptez un style (les règles ci-dessous), vous devez le respecter dans tout le programme. 


# 2. Les règles

Les règles suivantes devraient être respectées. Par contre, si vous préférez un autre standard (ex: utiliser `_` au lieu de camelCase), vous devez le conserver tout au long de votre programme. 

## 2.1. Nom de fonctions 

Nous utiliserons le standard utilisé en Java en 1N1

- camelCase (toutes les lettres en minuscules, sauf le première lettre de chaque mot à partir du 2e mot)
- doit commencer par une lettre, ou un souligné _
- peut contenir des lettres, des chiffres, et _
- ne doit pas être un mot réservé
- par convention, le premier mot doit être un verbe suivi d'un adjectif ou d'un nom.


    - additionner();
    - obtenirValeur();
    - calculerNombreEleves();
    - trouverUtilisateurParNom();
    - ...

## 2.2. Nom de variables

Nous utiliserons le standard utilisé en Java en 1N1

- camelCase (toutes les lettres en minuscules, sauf le première lettre de chaque mot à partir du 2e mot)
- doit commencer par une lettre, ou un souligné _
- peut contenir des lettres, des chiffres, et _
- ne doit pas être un mot réservé
- Le nom doit être de préférence court (sauf si cela le rend moins significatif)

    - uneVariable
    - nomDeLaPersonne
    - nbrDEtudiant
  

## 2.3. Nom de constante

- Tout en majuscule, les mots séparés par un soulignement _

    - UNE_CONSTANTE
    - NOMBRE_D_ETUDIANT
    - EST_VALIDE

!> Oui, d'autres standards existent. Ce n'est pas une religion, à vous d'utiliser celles qui vous seront demandées en milieu de travail

