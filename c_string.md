# string <!-- omit in toc -->

Les chaines de caractères en C sont *simplement* un tableau de caractères suivit d'un caractère `nul` indiquant la fin. 

# 1. Déclaration et initialisation
Pour déclarer une string en C
```c
char chaine1[10] = "allo";  
char chaine2[] = "tourlou"; 
char chaine3[10] = { 'a','l','l','o', 0};

printf("sizeof chaine1  %i\n", sizeof chaine1); // pourquoi 10?
printf("sizeof chaine2  %i\n", sizeof chaine2); // pourquoi 8?
printf("sizeof chaine3  %i\n", sizeof chaine3); // pourquoi 10?


```

<details>
<summary>Réponse:</summary>
Dans les trois cas, sizeof retourne la taille de ce qui a été réservé, pas la taille de la chaine de caractères jusqu'au NULL. 
Pour chaine2, il y a les 7 caractères + NULL 
</details>


Une fois déclarée, il est possible de modifier le contenue de la string
```c
chaine1[3] = 'b';
printf("%s\n", chaine1); 
```

Il est aussi possible d'assigner une chaine de caractères de la façon suivante:

```c
char *c1 = "allo";
```

Nous verrons plus tard ce que signifie * (*oui, c'est en rapport avec les pointeurs*)

!> Attention: ce type de string ne peut être modifié.

Si vous essayez de modifier c1 de la même façon qu'indiqué plus haut

```c
c1[0] = 'b';
printf("%s", c1);

```
 
Vous obtiendrez une erreur lors de l'exécution. Elle est dû au fait que la chaine "allo" est entreposée dans un espace réservé de la mémoire et que cet espace ne peut être modifié. Nous y reviendrons plus tard. 


# 2. Manipulation

Nous avons vue que `sizeof` retourne le nombre de caractères réservés par le tableau. Mais si je veux connaitre le nombre de caractères jusqu'au '/0' ?

La bibliothèque `<string.h>` contient des fonctions de manipulation de `string` tel que `strlen()`.

```c
#include <string.h>
...
printf("%i\n", strlen(chaine1));
printf("%i\n", strlen(chaine2));
printf("%i\n", strlen(chaine3));

```

Une autre manipulation fréquente est la copie d'une chaine de caractères dans une autre. 

On ne peux pas simplement assigner une chaine à une autre. Une chaine est un tableau, ne l'oubliez pas. 

Exemple:

```c
#include <string.h>

char chaine1[10] = "allo";
char chaine2[10];
chaine2 = chaine1;  //vous donnera une erreur
strcpy(chaine2, chaine1); // oui, c'est destination suivie de source
```
Pour plus de détails sur des fonctions de manipulation de `string`, vous pouvez allez voir  [ici](https://devdocs.io/c-strings/)

# 3. Caractères spéciaux

Voir [https://en.wikipedia.org/wiki/Escape_sequences_in_C](https://en.wikipedia.org/wiki/Escape_sequences_in_C) 