# Tableau <!-- omit in toc -->

Lorsqu'on doit traiter plusieurs éléments du même type, il est souvent nécessaire de les mettre dans un tableau (*array en anglais*)

# 1. Déclaration et initialisation
La déclaration d'un tableau en C est légèrement différente qu'en Java. 

En Java:
```java
int[] unTableauDEntierEnJava ={1,2,3,4,5}; 
ou
unAutreTableauJava = new double[4]; 
```

En C:
```c
int unTableauDEntierEnC[5];
```

On peut donc créer un tableau en C simplement en spécifiant le nombre d'éléments. 

!> Attention: rien ne garantit que les éléments seront initialisés à 0. Bien que la majorité du temps, ca sera vrai, cela dépend de la configuration du compilateur. Et comme toute autre ambiguité, ne prenez rien pour acquis et assurez-vous de la valeur qui sera mise dans l'array en la spécifiant. 

Il existe d'autre syntaxes d'initialisation.

```c
int tab1[5] = {0,0,0,0,0}; //initialise tous les éléments à 0
int tab2[3] = {0}; // initialise tous les éléments à 0
int tab3[3] = {1}; // initialise le premier éléments à 1, aucune garantie pour les autres
                   // la seule valeur qui est garantie, c'est {0}
int tab4[3] = {1,2,3}; // initialise le premier élément à 1, le 2e à 2, le 3e à 3
int tab5[5] = {1,2,3}; // les 3 premiers éléments sont initialisés, mais aucune garantie pour le reste 
int tab6[] = {1,2,3,4,5}; // la taille du tableau est déduite du nombre d'éléments. 
```

# 2. Taille

En Java, un tableau avait l'attribut `.length` permettant de connaitre le nombre d'éléments de celui-ci. En C, pour connaitre la taille d'un tableau, il faut utiliser l'opérateur `sizeof`.

Mais sizeof retourne la taille du tableau, en non pas le nombre d'éléments.

La technique à utiliser est  donc de diviser la taille du tableau par la taille d'un élément. (*nous verrons plus tard que ca ne fonctionne pas toujours, mais une étape à la fois*)

Exemple:
```c
int tab[5];
printf("taille du tableau %i\n",sizeof tab );
printf("taille d'un entier %i\n", sizeof(int));
printf("nombre d'éléments dans tableau %i\n",sizeof tab /sizeof(int));
```
## 2.1. Quelques clarifications à propos de sizeof

sizeof est une opérateur unaire; ce n'est pas une fonction. 

Notez dans l'exemple précédent que `tab` n'est pas entre parenthèse. On aurait tendance à vouloir écrire `sizeof(tab)`, mais les `()` ne sont pas nécessaire car ce n'est pas un appel de fonction avec un paramètre. C'est un peu comme si on écrivait `++(i)`. Ici `++` est un opérateur unaire, tout comme sizeof.   

Par contre, lorsqu'on veut savoir la taille d'un type, on doit mettre les `()`, comme c'est le cas pour `sizeof(int)`. Ici les `()` sont obligatoires.




# 3. Manipulation 

Comme en Java, pour accéder à un élément d'un tableau, il suffit d'indiquer l'indice de l'élément recherché. L'indice débute à 0. 

```c
int tab[5] = { 5,4,3,2,1 };
printf("premier élément %i\n",tab[0]);
printf("dernier élément %i\n", tab[4]);
```


Comme en Java, il est possible de changer un élément une fois le tableau initalisé. 
```c
int tab[5]  = { 5,4,3,2,1 };
tab[0] = 10;
tab[(sizeof tab/sizeof(int))-1] = 6;
printf("premier élément %i\n",tab[0]);
printf("dernier élément %i\n", tab[4]);;

```


