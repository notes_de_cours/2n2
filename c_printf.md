# printf <!-- omit in toc -->

Cette fonction sert à afficher du texte dans la console (l'écran) (stdout). 

La fonction printf est un peu l'équivalent du System.out.println de Java. 


La syntaxe de la commande est :

```c
 int printf (const char* format, ...);
```

!> Nous verrons la signification de `char*` plus tard. Pour l'instant traduisez la par `string`

Comme vous pouvez le remarquer, cette fonction retourne un `int`. Si tout fonctionne correctement, la fonction retournera le nombre de caractères qui ont été affichés. Si une erreur se produit, une valeur négative sera retournée. 

Les ... représentent des paramètres additionnels pouvant être passés à la fonction. 

Voyons quelques exemples: (*vous pouvez les insérer dans la fonction main dans QT*)

```c
printf("allo");

printf("La valeur est %d", 5);

int v = 10;
printf("La deuxième valeur est %i", v);

printf("Les deux valeurs sont %i et %i",5, v);

printf("La somme est %i", 5+v);
```

Si vous essayez ces exemples, vous noterez quelques problèmes d'affichage:
1. tout est sur la même ligne;
2. les accents ne s'affichent pas correctement. 

Pour le premier problème, il suffit d'indiquer à printf de faire un saut de ligne en ajouter `\n`.

```c
printf("allo\n");
printf("La valeur est %d\n", 5);

int v = 10;
printf("La deuxième valeur est %i\n", v);

printf("Les deux valeurs sont %i et %i\n",5, v);

printf("La somme est %i\n", 5+v);
```

Pour le deuxième problème, c'est un peu plus complexe (surtout si vous êtes sur Windows). Pour l'instant, nous l'éviterons en n'utilisant pas d'accent dans notre texte, ou en ne tenant pas compte des caract├¿res bizarres affich├® (*Vous pouvez toujours trouver une solution. Il en existe plusieurs, mais elles ont toutes leurs défauts*).

Vous devez avoir remarqué `%i` et `%d`. Ce sont des **spécificateurs de format**. Il en existe plusieurs. 

Les plus fréquents sont :

| Format 	| Type attendu 	|
|---	|---	|
| %d 	| int 	|
| %u 	| unsigned int 	|
| %ld 	| long 	|
| %f 	| float 	|
| %f 	| double 	|
| %%    | le caractère % | 

Il suffit donc d'indiquer, dans la chaine de caractères, l'endroit où on désire afficher la valeur qui sera indiquée par la suite dans la liste de valeurs suivant la virgule du printf.  

Les spécificateurs de format permettent aussi de spécifier la longueur et la précision du nombre à afficher. Je vous recommande d'aller voir [ce site ](https://devdocs.io/c/io/fprintf) pour plus d'information. 

!>Remarquez que le format pour un double et un float est le même: `%f`. Nous verrons que pour scanf, ce n'est pas le cas. 


