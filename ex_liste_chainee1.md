# Exercices sur les listes chainées <!-- omit in toc -->

Dans la dernière leçons, nous avons créer le code de base pour gérer une liste chainée. Cet exercice consiste à compléter ce code. 


Vous devez donc créer :
* une fonction permettant d'ajouter un element n'importe où dans la liste en spécifiant la position ou on veut ajouter (la position 1 étant après le premier élément de la liste. On ne peut pas ajouter à la position 0; pour ca, utiliser ajouterElementAuDebut. Pas besoin de gérer les positions négatives ni les positions trop grande). 
* une fonction permettant d'effacer un element quelconque selon sa position (la position 0 n'est pas valide, ca part à 1 ). Cette fonction prendre la position de l'élément a enlever, parcoura la liste pour le trouver et changera la série de pointeur afin d'enlever l'élément (n'oublier pas de le libérer après)
* une fonction qui retournera la taille de la liste. Il y a deux approches : 1) parcourir la liste et compter; 2) garder un compteur dans la structure liste et modifier le code pour incrémenter et décrémenter ce compteur. Bien entendu, vous devez faire l'option 2 :smile:
* le code ne libère pas la liste à la fin. Vous devez donc ajouter une fonction qui libère la liste en libérant les éléments qui la compose. 

Vous trouverez UNE solution ci-bas (je me suis permis d'en ajouter un peu plus). 
 

<details>
<summary>Solution</summary>

<details>
<summary>liste_chainee.h</summary>

# liste_chainee.h

```c
#ifndef LISTE_CHAINEE_H
#define LISTE_CHAINEE_H

typedef struct structElement element;
struct structElement
{
    int nombre;
    element *suivant;
};

typedef struct structListe
{
    element *premier;
    int taille;
} liste;

liste *initialiserListe();
element *initialiserElement(int aNombre);
void insererElementAuDebut(liste *aListe, int aNombre);
void insererElementALaFin(liste *aListe, int aNombre);
void supprimerPremier(liste *aListe);
void afficherListe(liste *aListe);
void afficherElement(element *aElement);
void viderListe(liste *aListe) ;
void insererElementParPosition(liste *aListe, int aPosition, int aNombre);
void supprimerParPosition(liste *aListe, int aPosition);
element *elementParPosition(liste *aListe, int aPosition);

#endif // LISTE_CHAINEE_H

```

</details>

<details>
<summary>liste_chainee.c</summary>

liste_chainee.c
===============

```c
#include <stdio.h>
#include <stdlib.h>
#include "liste_chainee.h"

/**
 * @brief allocation de mémoire pour l'entête de la liste chainée
 * @return le pointeur vers la structure
 */
liste *initialiserListe()
{
    liste *lListe = calloc(1, sizeof(liste));

    if (lListe == NULL ) // vérification de l'allocation de l'espace mémoire
    {
        exit(EXIT_FAILURE); //nous n'avons pas vue cette fonction, mais elle fait ce qu'elle dit
                            //... elle termine le programme en retournant un code de sortie
                            // ici c'est un code d'erreur qui est prédéfini
    }
    lListe->premier = NULL;
    lListe->taille = 0;
    return lListe;
}

/**
 * @brief allocation de mémoire pour un élément de la liste et insertion de la valeur dans celui-ci
 * @param aNombre la valeur à mettre dans l'élément
 * @return le pointeur vers l'élément
 */
element *initialiserElement(int aNombre) {
    /* Création du nouvel élément */
    element *nouveau = calloc(1, sizeof(element));
    if ( nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }
    nouveau->nombre = aNombre;
    nouveau->suivant = NULL;
    return nouveau;
}

/**
 * @brief insertion d'un élément au début de la liste.
 * @param aListe la liste dans laquelle insérer l'élément.
 * @param aNombre la valeur à mettre dans l'élément qui sera inséré
 */
void insererElementAuDebut(liste *aListe, int aNombre)
{
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }

    /* Création du nouvel élément */
    element *nouveau = initialiserElement(aNombre);

    /* Insertion de l'élément au début de la liste */
    nouveau->suivant = aListe->premier;
    aListe->premier = nouveau;
    aListe->taille++;
}

/**
 * @brief insertion d'un élément à la fin de la liste.
 * @param aListe la liste dans laquelle insérer l'élément.
 * @param aNombre la valeur à mettre dans l'élément qui sera inséré
 */
void insererElementALaFin(liste *aListe, int aNombre)
{
    element *recherche = NULL;
    element *suivant;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    /* Recherche de la fin de la liste */
    suivant = aListe->premier;
    while(suivant) {
        recherche = suivant;
        suivant = suivant->suivant;
    }

    if(!recherche) { //la liste est vide, il faut ajouter au début de la liste
        insererElementAuDebut(aListe, aNombre);
    } else {
        /* Création du nouvel élément */
        element *nouveau = initialiserElement(aNombre);
        recherche->suivant = nouveau;
        aListe->taille++;
    }
}

/**
 * @brief suppression du premier élément de la liste
 * @param aListe la liste dans laquelle enlever l'élément
 */
void supprimerPremier(liste *aListe)
{
    element *lSupprimer;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    if (aListe->premier != NULL)
    {
        lSupprimer = aListe->premier;
        aListe->premier = aListe->premier->suivant;
        aListe->taille--;
        free(lSupprimer);
    }
}

/**
 * @brief affiche le contenu de la liste
 * @param aListe la liste à afficher
 */
void afficherListe(liste *aListe)
{
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    printf("taille: %i\n", aListe->taille);
    element *actuel = aListe->premier;
    while (actuel != NULL)
    {
        printf("%d -> ", actuel->nombre);
        actuel = actuel->suivant;
    }
    printf("NULL\n");
}

/**
 * @brief affiche le contenu d'un element
 * @param aElement l'élément à afficher
 */
void afficherElement(element *aElement) {
    if(aElement == NULL)
    {
        printf("NULL\n");
    } else {
        printf("valeur: %i\n", aElement->nombre);
    }
}

/**
 * @brief efface tous les éléments de la liste
 * @param aListe la liste dans laquelle effacer tous les éléments
 */
void viderListe(liste *aListe) {
    element *suivant;
    element *lElement = aListe->premier;
    while (lElement ) {
        suivant = lElement->suivant;
        free(lElement);
        lElement = suivant;
    }
    aListe->premier = NULL;
    aListe->taille = 0;
}

/**
 * @brief insère un élément à une position donnée
 *
 * La position est celle de l'élément après lequel le nouvel élément sera ajouté.
 * La position 1 indique d'ajouter après le premier élément
 * (celui qui est pointé par le début de la liste)
 * On ne peut insérer au début de la liste. Pour ca, prendre la fonction insererElementAuDebut()
 *
 * @param aListe la liste dans laquelle insérer l'élément
 * @param aPosition la position après laquelle insérer
 * @param aNombre la valeur à mettre dans l'élément
 */
void insererElementParPosition(liste *aListe, int aPosition, int aNombre)
{
    element *recherche;
    element *ancien;
    int lPosition = 1;
    if (aListe == NULL )
    {
        exit(EXIT_FAILURE);
    }


    /* recherche de l'element à la position demandée  */
    recherche = aListe->premier;
    while (recherche && lPosition != aPosition ) {
        recherche = recherche->suivant;
        lPosition++;
    }
    if (recherche) { // on a trouvé où l'insérer
        ancien = recherche->suivant;
        // l'init se fait ici, sinon on  pourrait perdre l'élément si on a pas trouvé où le mettre
        element *nouveau = initialiserElement(aNombre);
        recherche->suivant = nouveau;
        nouveau->suivant = ancien;
        aListe->taille++;
    } // si on a pas trouvé, simplement oublier l'insertion. On pourrait aussi générer une erreur.
}

/**
 * @brief supprime l'élément à la position indiquée
 * @param aListe la liste dans laquelle supprimer l'élément
 * @param aPosition la position de l'élément à supprimer
 */
void supprimerParPosition(liste *aListe, int aPosition)
{
    element *recherche;
    element *ancien = NULL;
    int lPosition = 1;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }

    /* recherche de l'element à la position demandée  */
    recherche = aListe->premier;
    while (recherche && lPosition != aPosition ) {
        ancien = recherche;
        recherche = recherche->suivant;
        lPosition++;
    }
    if (recherche) { // on a trouvé lequel enlever
        if(aPosition == 1) { // c'est le premier, il faut donc replacer le début de la liste
            //ce qui ressemble beaucoup à supprimerPremier()
            aListe->premier = aListe->premier->suivant;
        } else {
            ancien->suivant = recherche->suivant;
        }
        free(recherche);
        aListe->taille--;

    }
}

element *elementParPosition(liste *aListe, int aPosition) {
    element *recherche;
    int lPosition = 1;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }

    /* recherche de l'element à la position demandée  */
    recherche = aListe->premier;
    while (recherche && lPosition != aPosition ) {
        recherche = recherche->suivant;
        lPosition++;
    }
    return recherche;
}


```

</details>
<details>
<summary>main.c</summary>

main.c
======

```c
#include <stdio.h>
#include <stdlib.h>
#include "liste_chainee.h"

int main()
{
    liste *maListe = initialiserListe();

    insererElementAuDebut(maListe, 4);
    insererElementAuDebut(maListe, 8);
    insererElementAuDebut(maListe, 15);
    insererElementParPosition(maListe, 2, 5);
    afficherListe(maListe);

    supprimerPremier(maListe);
    afficherListe(maListe);

    supprimerParPosition(maListe, 2);
    afficherListe(maListe);
    supprimerParPosition(maListe, 2);
    afficherListe(maListe);
    supprimerParPosition(maListe, 2); //test pour enlever un élément inexistant.
    afficherListe(maListe);

    element *lElement = elementParPosition(maListe, 1);
    afficherElement(lElement);
    lElement = elementParPosition(maListe, 5); //test pour afficher un element inexistant
    afficherElement(lElement);

    supprimerParPosition(maListe,1); // test pour enlever le seul element
    afficherListe(maListe);

    insererElementAuDebut(maListe,3);
    insererElementParPosition(maListe,1,20);
    afficherListe(maListe);
    supprimerParPosition(maListe,1); // test pour enlever le premier
    afficherListe(maListe);

    insererElementALaFin(maListe, 19);
    insererElementALaFin(maListe, 18);
    afficherListe(maListe);

    viderListe(maListe);
    afficherListe(maListe);
    viderListe(maListe); // test pour vider une liste vide
    afficherListe(maListe);

    insererElementALaFin(maListe, 17); // test de l'ajout a la fin d'une liste vide
    insererElementALaFin(maListe, 16);
    afficherListe(maListe);

    viderListe(maListe);
    free(maListe);  // il faut appeler viderListe() avant afin de libérer tous les éléments. 

    return 0;
}
```
</details>
</details>