# Pointeur de pointeur <!-- omit in toc -->

Il est possible d'avoir un pointeur vers un autre pointeur. 

Si je veux un entier, je déclare:  `int x;`

Si je veux un pointeur d'entier, je déclare:  `int *y`;

Si je veux un pointeur de pointeur d'entier, je déclare: `int **z`;

Nous savons que pointeur d'entier peut indiquer le début d'un tableau d'entier.

Par extensions, un pointeur de pointeur, peut donc indiquer le début d'un tableau de pointeur. 

Pourquoi vouloir un pointeur de pointeur?

Imaginez le scénario suivant: j'ai besoin d'un tableau de pointeurs d'entier. 

# 1. sur la pile
Si le nombre de pointeurs d'entiers est connue d'avance, la structure peut être déclarée
 directement sur la pile:

```c
#include <stdio.h>

int main(int aParametresTaille, char* aParametres[])
{
    int *ptrInt[10]; //un tableau de pointeur (donc un pointeur vers un pointeur)
    int x[10] = {1,2,3,4,5,6,7,8,9,10};
    for(int i=0; i<10; i++) {
        ptrInt[i] = &x[i]; // chaque élément du tableau ptrInt pointe maintenant sur une des valeurs du tableau d'entier x
    }

    x[2] = 20;
    for(int i=0; i<10; i++) {
        printf("%i ,", *ptrInt[i]); // notez qu'à l'indice 2, la valeur est changée. Ce qui prouve que
                                    // les éléments de ptrInt pointent sur une élément de x. 
    }
    printf("\n");
    return 0;
}
```

Cet exemple, est un peu trivial, mais démontre l'utilisation d'un tableau de pointeur. 

# 2. sur le tas
Maintenant, si on ne connait pas le nombre d'entiers d'avance, il est alors nécessaire d'allouer dynamiquement le tableau. 

Ce programme demande un nombre à l'usager, et boucle tant que 0 n'est pas entré. 

Un espace mémoire est alloué pour cet entier (ptrInt), un tableau de pointeur (tabPtrInt) est agrandi afin d'ajouter un pointeur vers l'espace mémoire qui vient d'être alloué. 

À la fin, tous les espaces réservés pour les entiers sont libérés via le tableau (c'est le seul endroit à partir duquel on peut accéder aux entiers), et le tableau est lui même libéré par la suite. 

Notez que le calloc pour ptrInt n'alloue qu'un seul espace, pas un tableau. 

```c
#include <stdio.h>
#include <stdlib.h>

int main(int aParametresTaille, char* aParametres[])
{
    int **tabPtrInt = NULL;
    int *ptrInt = NULL;
    int nombre = -1;
    int compteur = 0;
    while(nombre != 0) {
        printf("entrez un nombre: ");
        scanf("%i", &nombre);
        if(nombre != 0) {
            if(!tabPtrInt) {
                tabPtrInt = calloc(1, sizeof(int*));
            } else {
                tabPtrInt = realloc(tabPtrInt, sizeof(int*)*(compteur+1));
            }
            ptrInt = calloc(1, sizeof(int));
            *ptrInt = nombre;
            tabPtrInt[compteur]=ptrInt;
            printf("%i \n", *tabPtrInt[compteur]);
            compteur++;

        }
    }

    for(int i=0; i<compteur; i++) {
        printf("%i ,", *tabPtrInt[i]);
    }
    printf("\n");

    for(int i =0; i<compteur; i++) {
        free(tabPtrInt[i]);
    }
    free(tabPtrInt);
    return 0;
}

```

Ici, au lieu d'utiliser un tableau de pointeurs d'entier, il aurait été possible de prendre un tableau d'entiers (il suffit d'enlever des * par-ci par-là :smile: ).

Mais imaginez que ce n'est pas des entiers qui doivent être entreposés, mais plutôt des structures, ou des tableaux de nombres (plusieurs tableaux de nombres). Dans ces cas, une telle architecture est nécessaire. 

Vous aurez d'ailleurs un exercice du genre un peu plus tard!

