# Les variables <!-- omit in toc -->

La gestion des variables en C ressemble beaucoup à ce que vous avez vue en Java (en vérité, c'est le Java qui descend du C ). Nous passerons assez rapidement, en ne nous attardant que sur le spécificités. 

# 1. Déclaration

La déclaration d'une variable se fait commen en Java. Il suffit d'indiquer le type et un nom, suivit d'un ; .
``` c
int nombreDeVies;
double temperatureExterieur;
int varA, varB, varC;
```

Les types ressemble aussi à ceux de Java: `int, long, float, char`.

Vous noterez l'absence  de certains types plus 'avancés', tel que: `boolean, string`. 

Un boolean sera représenté par un int (0 = `faux`, toute autre valeur = `vrai`). Il est aussi possible d'utiliser <stdbool.h> que nous verrons plus tard. 

Pour les `string` (chaine de caractères), nous utiliserons des pointeurs. Nous verrons cette fonctionnalité en détails dans un proche futur. 

Et bien entendu, le C ne manipule pas d'objets. Pour cela, il faut utiliser un de ses nombreux descendants: C++, C#, objective-C, PHP ... pour n'en nommer que quelques-uns. 


# 2. Limites

| Nom du type 	| Minimum 	| Maximum 	|
|---	|---	|---	|
| signed char 	| -128 	| 127 	|
| int 	| -32 768 	| 32 767 	|
| long 	| -2 147 483 648 	| 2 147 483 647 	|
| float 	| -1 x1038 	| 1 x1038 	|
| double 	| -1 x1038 	| 1 x1038 	|

Pour les types entiers ( `signed char, int, long`), il existe d'autres types dits unsigned(non signés) qui, eux, ne peuvent stocker que des nombres positifs. Pour les utiliser, il suffit d'écrire le mot `unsigned`devant le type :

| Nom du type 	| Minimum 	|
|---	|---	|
| unsigned char 	| 0 à 255 	|
| unsigned int 	| 0 à 65 535 	|
| unsigned long 	| 0 à 4 294 967 295 	|


> Pourquoi avoir créé trois types pour les nombres entiers ? Un seul aurait été suffisant, non ? 
 
Oui, mais on a créé à l'origine plusieurs types pour économiser de la mémoire. Ainsi, quand on dit à l'ordinateur qu'on a besoin d'une variable de type char, on prend moins d'espace en mémoire que si on avait demandé une variable de type int.
Toutefois, c'était utile surtout à l'époque où la mémoire était limitée. Aujourd'hui, nos ordinateurs ont largement assez de mémoire vive pour que ça ne soit plus vraiment un problème. Il ne sera donc pas utile de se prendre la tête pendant des heures sur le choix d'un type. Si vous ne savez pas si votre variable risque de prendre une grosse valeur, mettez int ou double pour un flottant.

# 3. Les constantes

Tout comme en Java, il est possible de déclarer une variable qui devra être initialisée avec une valeur, et cette valeur ne pourra être changée par la suite: une constante.

En Java, une constante est déclarée comme ceci:
```java
final int NOMBRE_DE_FACE_DU_DE = 6;
```

En C, la syntaxe est la suivante:
```c
const int NOMBRE_DE_FACE_DU_DE = 6;
```


<!-- ajouter une section sur le scope. Ils mettent souvent des variables globales dans les .c autres que main.c -->
<!-- ajouter une section sur #define -->