# Écriture d'une liste chainée contenant des allocations dynamiques dans un fichier <!-- omit in toc -->

L'exercice supplémentaire sur les listes chainées contenait une liste chainée dans laquelle il y avait une structure allouée dynamiquement.

Comment faire pour sauvegarder ca sur disque?

La structure était la suivante:

```c
typedef struct personne
{
    char *nom;
    char *prenom;
} personne;

typedef struct animal animal;
struct animal
{
    char *nom;
    personne *proprietaire;
    animal *suivant;
};

typedef struct animaux
{
    animal *premier;
    int taille;
} animaux;
```

Si vous avez fait l'exercice, vous pouvez reprendre votre code, sinon vous pouvez copier la solution proposée. (bien entendu, je vous recommande de faire l'exercice par vous même). 

Nous allons ajouter une fonction permettant de sauvegarder et de lire les données. 



Allons y par étape. 

## Écriture

Comment écrire l'animal dans le fichier ?

Si on regarde la structure animal, c'est un paquet de pointeurs.  Est-il possible de sauvegarder des pointeurs sur disque ?

La réponse est non. Un pointeur ne fait de sens qu'en mémoire. 

Nous allons donc devoir créer une structure additionnel simplement pour faire la sauvegarde. 

!> Notez qu'il serait aussi possible de sauvegarder les structures champs par champs. Mais il faut aussi penser qu'on devra relire ces champs. Si on change la structure (en ajoutant un autre champ) il faudra s'assurer que l'écriture et la lecture sont synchronisées. Nous allons donc prendre l'approche consistant à créer une structure uniquement pour l'écriture et la lecture sur disque. 

Ajoutez la structure suivante dans structures.h :

```c
typedef struct animalFichier
{
    char nom[LONGUEUR_CHAMPS];
} animalFichier;
```

!> Notez ici que je remplace `char *` par `char  []`. C'est nécessaire car l'écriture dans le fichier requiert une longueur fixe. 

Pour les mêmes raisons, je dois ajouter une structure pour sauvegarder personne car c'est aussi une série de pointeurs. 

Ajoutez donc la structure suivante dans structures.h :

```c
typedef struct personneFichier
{
    char nom[LONGUEUR_CHAMPS];
    char prenom[LONGUEUR_CHAMPS];
} personneFichier;
```

Il faut maintenant transférer les structures avec pointeurs vers les structures pour le fichier. 

Voici le squelette de la fonction:

```c
void sauvegarderAnimaux(animaux *aListe, char *aNomFichier)
{
    FILE *lFichier = fopen(aNomFichier, "w");
    // 1
    animal *lAnimalASauvegarder = aListe->premier;
    for(int i=0; i<aListe->taille; i++) {
        //2 ecrire l'animal dans le fichie
        //3 ecrire le proprio dans le fichier
        lAnimalASauvegarder = lAnimalASauvegarder->suivant;
    }

    fclose(lFichier);
}
```

Commencons par animal. Ajoutez une nouvelle variable au début de sauvegarderAnimaux() à //1 :

```c
animalFichier lAnimalFichier;
```

et sont transfert à partir des pointeurs. Ajoutez ces lignes au début du for{} à //2 :

```c
strncpy(lAnimalFichier.nom, lAnimalASauvegarder->nom, sizeof(lAnimalFichier.nom) );
fwrite(&lAnimalFichier, sizeof(lAnimalFichier), 1, lFichier);
```

La fonction [strncpy](https://devdocs.io/c/string/byte/strncpy) copie dans la string de destination (le premier paramètre) la string source (le 2e paramètre), pour un certain nombre de caractères (le 3e paramètre). S'il n'y a pas assez de caractères dans la source, alors le reste est remplie de NULL.

Nous allons faire la même chose avec personne. Ajoutez donc ces lignes à //3

```c
personneFichier lPersonneFichier;
strncpy(lPersonneFichier.nom, lAnimalASauvegarder->proprietaire->nom, sizeof(lPersonneFichier.nom));
strncpy(lPersonneFichier.prenom, lAnimalASauvegarder->proprietaire->prenom, sizeof(lPersonneFichier.prenom));
fwrite(&lPersonneFichier, sizeof(personneFichier), 1, lFichier);
```

Essayez ce code, et vérifiez que c'est bien sauvegardé dans le fichier. 

Voilà, l'animal et son proprio sont sauvegardés!

!> Question: si au lieu de strncpy j'avais utilisé strcpy, que serait-il arrivé?

Essayons-le: changez les strncpy par strcpy (il faut enlever le 3e paramètre).

Ca semble fonctionner... mais allez voir le fichier résultant. 

Pourquoi tous ces symboles biz?

C'est la différence entre strcpy et strncpy. Ce dernier fait le ménage!

## Lecture

La lecture est un peu plus complexe, mais quand même facile à comprendre. 

Commencons par créer la fonction :

```c
void chargerAnimaux(animaux *aListe, char *aNomFichier)
{
    FILE *lFichier = fopen(aNomFichier, "r");
    //1
    int continuer = 0;

    do {
        //2 lire un animal
        // si la lecture a fonctionné
        //    lire une personne
        //   si la lecture a fonctionné
        //       transférer la personne et l'animal en mémoire
        //       associer la personne à l'animal
        //    si non, on a un problème car ca veut dire qu'on à un animal sans proprio!!
        // si non, on a terminé

    } while (continuer);
    fclose(lFichier);

}

```

Cette fois-ci, nous devons lire une structure en format fichier, et la transférer en mémoire dans une liste chainée. 

Nous aurons donc besoin des mêmes variables que lors de l'écriture. Ajoutez donc ces lignes au début de la fonction :

```c
animalFichier lAnimalACharger;
animal *lAnimalEnMemoire;
personneFichier lPersonneACharger;
personne *lPersonneEnMemoire;
```

Allons y pour la lecture du premier animal, ajoutez en //2 :

```c
continuer = fread(&lAnimalACharger, sizeof(animalFichier), 1 , lFichier);
if(continuer) {
//3
}
```

Si `continuer` est vrai (une valeur autre que 0, indiquant que fread() a lu au moins 1 élément ), on peut aller lire son proprio. 

Ajoutez en //3

```c
continuer = fread(&lPersonneACharger, sizeof(personneFichier), 1, lFichier);
    if(continuer) {
        lPersonneEnMemoire = calloc(1, sizeof(personne));
        lPersonneEnMemoire->nom = calloc(LONGUEUR_CHAMPS, sizeof(char));
        lPersonneEnMemoire->prenom = calloc(LONGUEUR_CHAMPS, sizeof(char));
        strncpy(lPersonneEnMemoire->nom, lPersonneACharger.nom, LONGUEUR_CHAMPS);
        strncpy(lPersonneEnMemoire->prenom, lPersonneACharger.prenom, LONGUEUR_CHAMPS);

        //4
    } else {
        printf("oups, un animal sans proprio, y'a un probleme");
    }
```

Bien entendu, ca prendrait une meilleure gestion de l'erreur dans le cas où l'animal n'a pas de proprio. 

Le code que l'on vient d'ajouter fait donc l'allocation mémoire de la structure personne, et y copie les valeurs provenant du fichier. 

On peut maintenant transférer l'animal que nous avions lu précédemment. Ajoutez ces lignes en //4

```c
lAnimalEnMemoire = calloc(1, sizeof(animal));
lAnimalEnMemoire->nom = calloc(LONGUEUR_CHAMPS, sizeof(char));
strncpy(lAnimalEnMemoire->nom, lAnimalACharger.nom, LONGUEUR_CHAMPS);
lAnimalEnMemoire->proprietaire=lPersonneEnMemoire;
lAnimalEnMemoire->suivant = NULL;
//5
```

Notez qu'on mets le suivant à NULL. Pourquoi ?

Réponse: Nous venons de lire le premier animal du fichier, on ne sait pas encore s'il aura un suivant. Pour l'instant nous présumons que non.

Mais s'il y a plusieurs animaux dans le fichier, comment les ajouter les uns aux autres ?

Réponse: Gardons un pointeur sur l'animal (appelons cette variable `lAnimalEnMemoirePrecedent`) que nous venons d'ajouter, et au tour suivant, nous indiquerons que le nouvel animal est le suivant de lAnimalEnMemoirePrecedent. 

Allons y !

Ajoutez une variable en //1 

```c
    animal *lAnimalEnMemoirePrecedent = NULL;
```

Et en //5, ajoutez :

```c
if(lAnimalEnMemoirePrecedent) {
    lAnimalEnMemoirePrecedent->suivant = lAnimalEnMemoire;
    aListe->taille++;
} else {
    aListe->premier = lAnimalEnMemoire;
    aListe->taille = 1;
}
lAnimalEnMemoirePrecedent = lAnimalEnMemoire;
```

Au premier tour, lAnimalEnMemoirePrecedent sera NULL. Le code ira donc dans le else.

Dans le else, on met l'animal que l'on vient de lire au début de la liste, et on démarre la liste avec la taille 1. 

Ensuite on conserve le premier animal dans lAnimalEnMemoirePrecedent. 

Au deuxième tour, s'il y en a un, lAnimalEnMemoirePrecedent ne sera pas NULL, le code ira donc dans le if. 

Dans le if, on va chercher l'animal précédent, et on met l'animal courant comme étant son suivant. Et on incrémente la taille de la liste. 

Et voila, la lecture est faite !



Et comme d'habitude la réponse au complet :

<details>
<summary>Solution</summary>

<details>
<summary>structures.h</summary>

```c
#ifndef STRUCTURES_H
#define STRUCTURES_H

#define LONGUEUR_CHAMPS 30

typedef struct personne
{
    char *nom;
    char *prenom;
} personne;

typedef struct animal animal;
struct animal
{
    char *nom;
    personne *proprietaire;
    animal *suivant;
};

typedef struct animalFichier
{
    char nom[LONGUEUR_CHAMPS];
} animalFichier;

typedef struct personneFichier
{
    char nom[LONGUEUR_CHAMPS];
    char prenom[LONGUEUR_CHAMPS];
} personneFichier;

typedef struct animaux
{
    animal *premier;
    int taille;
} animaux;

#endif // STRUCTURES_H

```

</details>
<details>
<summary>gestionAnimal.h</summary>

```c
#ifndef GESTIONANIMAL_H
#define GESTIONANIMAL_H
#include "structures.h"


animaux *initialisationAnimaux();
void inscrireAnimal(animaux *listeAnimaux);
void insererAuDebut(animaux *aListe, char *aNom,
                    char *aNomProprio, char *aPrenomProprio);
void afficherAnimal(animal *aAnimal);
void afficherAnimaux(animaux *aListe);
animal *trouverAnimalParIndex(animaux *aListe, int aIndex);
void afficherAnimalParIndex(animaux *aListe, int aIndex);
void modifierAnimalParIndex(animaux *aListe, int aIndex);
void liberer(animal *aAnimal);
void effacerAnimalParIndex(animaux *aListe, int aIndex);
void effacerTousLesAnimaux(animaux *aListe);

void sauvegarderAnimaux(animaux *aListe, char *aNomFichier);
void chargerAnimaux(animaux *aListe, char *aNomFichier);

#endif // GESTIONANIMAL_H

```
</details>

<details>
<summary>gestionAnimal.c</summary>

```c
#include "structures.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


animaux *initialisationAnimaux() {
    animaux *lListe = calloc(1, sizeof(animaux));

    if (lListe == NULL ) // vérification de l'allocation de l'espace mémoire
    {
        exit(EXIT_FAILURE);
    }
    lListe->premier = NULL;
    lListe->taille = 0;
    return lListe;
}

void insererAuDebut(animaux *aListe, char *aNom,
                    char *aNomProprio,
                    char *aPrenomProprio)
{
    animal *lAnimal = calloc(1, sizeof(animal));
    personne *lProprio = calloc(1, sizeof(personne));
    char *lNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lPrenomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lNomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    if(!lAnimal || !lProprio || !lNom || !lPrenomProprio || !lNomProprio)
    {
        exit(EXIT_FAILURE);
    }

    strcpy(lNom, aNom);
    strcpy(lPrenomProprio, aPrenomProprio);
    strcpy(lNomProprio, aNomProprio);

    lAnimal->nom = lNom;
    lProprio->prenom = lPrenomProprio;
    lProprio->nom = lNomProprio;

    lAnimal->proprietaire = lProprio;

    //insère l'animal au début de la liste
    lAnimal->suivant = aListe->premier;
    aListe->premier = lAnimal;
    aListe->taille++;

}


void inscrireAnimal(animaux *aListe)
{
    char *lNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lNomProprio= calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lPrenomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    printf("Nom de l'animal :");
    scanf("%s",lNom);
    printf("Prenom du proprio :");
    scanf("%s",lPrenomProprio);
    printf("Nom du proprio :");
    scanf("%s",lNomProprio);

    insererAuDebut(aListe, lNom, lNomProprio, lPrenomProprio);
}

void afficherAnimal(animal *aAnimal) {
    printf("Nom :            %s\n", aAnimal->nom);
    printf("Nom du proprio : %s\n", aAnimal->proprietaire->nom);
    printf("Prenom :         %s\n", aAnimal->proprietaire->prenom);
    printf("-------------------\n");
}

void afficherAnimaux(animaux *aListe) {
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    char animal_aux[8]="animaux";
    if(aListe->taille == 1) {
        strcpy(animal_aux, "animal");
    }
    printf("il y a presentement %i %s dans la liste \n", aListe->taille, animal_aux);
    animal *actuel = aListe->premier;
    while (actuel != NULL)
    {
        afficherAnimal(actuel);
        actuel = actuel->suivant;
    }
    printf("--------------------------\n");

}

animal *trouverAnimalParIndex(animaux *aListe, int aIndex)
{
    int lPosition=1;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    animal *actuel = aListe->premier;
    while(lPosition < aIndex && actuel != NULL)
    {
        lPosition++;
        actuel = actuel->suivant;
    }
    return actuel;
}
void afficherAnimalParIndex(animaux *aListe, int aIndex)
{
    animal *actuel = trouverAnimalParIndex(aListe, aIndex);
    if(actuel) {
        afficherAnimal(actuel);
    } else {
        printf("Animal non trouve!\n");
    }

}

void modifierAnimalParIndex(animaux *aListe, int aIndex)
{
    char *nouveauNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    animal *actuel = trouverAnimalParIndex(aListe, aIndex);
    if(actuel) {
        printf("Valeurs actuelles : \n");
        afficherAnimal(actuel);
        printf("Entrez le nouveau nom de l'animal :");
        scanf("%s", nouveauNom );
        strcpy(actuel->nom, nouveauNom);
        printf("Nouvelles valeurs : \n");
        afficherAnimal(actuel);
    } else {
        printf("Animal non trouve!\n");
    }
}

void liberer(animal *aAnimal)
{
    free(aAnimal->nom);
    free(aAnimal->proprietaire->nom);
    free(aAnimal->proprietaire->prenom);
    free(aAnimal->proprietaire);
    free(aAnimal);
}
void effacerAnimalParIndex(animaux *aListe, int aIndex)
{
    animal *actuel;
    animal *lEffacer;
    if(aListe->taille == 0) {
        printf("la liste est deja vide! \n");
    } else {
        if(aIndex== 1) { // début de la liste, et liste non-vide
            lEffacer = aListe->premier;
            aListe->premier=lEffacer->suivant;
            liberer(lEffacer);
            aListe->taille--;
        } else { // un élément autre que le premier
            actuel = trouverAnimalParIndex(aListe, aIndex-1 ); // -1 pour arrêter a celui d'avant

            if(actuel && actuel->suivant) { //actuel est le précédent
                //actuel peut donc être là, mais le suivant (que l'on cherche) ne le soit pas
                lEffacer = actuel->suivant;
                actuel->suivant = actuel->suivant->suivant;
                liberer(lEffacer);
                aListe->taille--;
            } else {
                printf("Animal non trouve!\n");
            }
        }
    }
}

void effacerTousLesAnimaux(animaux *aListe)
{
    animal *actuel;
    animal *suivant;

    actuel = aListe->premier;
    while(actuel) {
        suivant = actuel->suivant;
        liberer(actuel);
        actuel = suivant;
    }
    aListe->premier = NULL;
    aListe->taille = 0;
}

void sauvegarderAnimaux(animaux *aListe, char *aNomFichier)
{
    FILE *lFichier = fopen(aNomFichier, "w");
    animalFichier lAnimalFichier;
    animal *lAnimalASauvegarder = aListe->premier;
    for(int i=0; i<aListe->taille; i++) {
        strncpy(lAnimalFichier.nom, lAnimalASauvegarder->nom, sizeof(lAnimalFichier.nom) );
        fwrite(&lAnimalFichier, sizeof(lAnimalFichier), 1, lFichier);

        personneFichier lPersonneFichier;
        strncpy(lPersonneFichier.nom, lAnimalASauvegarder->proprietaire->nom, sizeof(lPersonneFichier.nom));
        strncpy(lPersonneFichier.prenom, lAnimalASauvegarder->proprietaire->prenom, sizeof(lPersonneFichier.prenom));
        fwrite(&lPersonneFichier, sizeof(personneFichier), 1, lFichier);

        lAnimalASauvegarder = lAnimalASauvegarder->suivant;
    }

    fclose(lFichier);
}



void chargerAnimaux(animaux *aListe, char *aNomFichier)
{
    FILE *lFichier = fopen(aNomFichier, "r");
    animalFichier lAnimalACharger;
    animal *lAnimalEnMemoire;
    personneFichier lPersonneACharger;
    personne *lPersonneEnMemoire;

    animal *lAnimalEnMemoirePrecedent = NULL;
    int continuer = 0;
    do {
        continuer = fread(&lAnimalACharger, sizeof(animalFichier), 1 , lFichier);
        if(continuer) {

            continuer = fread(&lPersonneACharger, sizeof(personneFichier), 1, lFichier);
            if(continuer) {
                lPersonneEnMemoire = calloc(1, sizeof(personne));
                lPersonneEnMemoire->nom = calloc(LONGUEUR_CHAMPS, sizeof(char));
                lPersonneEnMemoire->prenom = calloc(LONGUEUR_CHAMPS, sizeof(char));
                strncpy(lPersonneEnMemoire->nom, lPersonneACharger.nom, LONGUEUR_CHAMPS);
                strncpy(lPersonneEnMemoire->prenom, lPersonneACharger.prenom, LONGUEUR_CHAMPS);

                lAnimalEnMemoire = calloc(1, sizeof(animal));
                lAnimalEnMemoire->nom = calloc(LONGUEUR_CHAMPS, sizeof(char));
                strncpy(lAnimalEnMemoire->nom, lAnimalACharger.nom, LONGUEUR_CHAMPS);
                lAnimalEnMemoire->proprietaire=lPersonneEnMemoire;
                lAnimalEnMemoire->suivant = NULL;
                if(lAnimalEnMemoirePrecedent) {
                    lAnimalEnMemoirePrecedent->suivant = lAnimalEnMemoire;
                    aListe->taille++;
                } else {
                    aListe->premier = lAnimalEnMemoire;
                    aListe->taille = 1;
                }
                lAnimalEnMemoirePrecedent = lAnimalEnMemoire;
            } else {
                printf("oups, un animal sans proprio, y'a un probleme");
            }
        }
    } while (continuer);
    fclose(lFichier);
    
}

```
</details>

</details>