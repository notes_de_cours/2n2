# Les fichiers <!-- omit in toc -->

La création de fichiers sur disque est un des systèmes les mieux construits du langage C/C++. En effet, en C/C++, il existe des stratégies d'écriture pour 2 types de fichiers sur le disque: le fichier binaire et les fichiers texte.

!>**ATTENTION** Étant donné que nous utilisons QT comme éditeur, il est important de changer un paramètres d'exécution afin d'avoir accès aux fichiers manipulés.\
Allez dans le menu Projets (la clé à molettes), ensuite dans Run, et changez le chemin du Working directory pour qu'il soit le même que celui de votre code c (là où se trouve main.c)\
Habituellement, les fichiers manipulés par le code ne devraient pas être dans le même répertoire que le code, mais pour des fins pédagogiques c'est ce que nous ferons. Il sera plus facile de remettre les TPs. 
![alt](_media/Working_directory.png)



Ouverture et fermeture de fichiers
----------------------------------

Peu importe le type de fichier utilisé (binaire ou texte), l'ouverture se fait exactement de la même manière. Seul l'argument `mode` de `fopen` est différent pour les fichiers binaires et les fichiers texte.

Voici les instructions utilisées pour ouvrir et fermer des fichiers en C/C++:

### [fopen](https://devdocs.io/c/io/fopen)

Permet d'ouvrir un fichier en lecture ou en écriture. La signature de la routine est la suivante:

```c
FILE* fopen(char* nomFichier, char* mode)
```
* L'argument `nomFichier` correspond au nom (ou adresse) du fichier à ouvrir;
* L'argument `mode` est une chaîne de caractères correspondant au type d'ouverture que le programme utilisera. Les types de mode sont:
	* Pour les fichiers textes, les différents modes sont:
		* **"r"**: ouverture en lecture seulement. Le fichier doit exister.
		* **"w"**: création et ouverture du fichier en écriture. Il est important de spécifier que ce type d'ouverture effacera le contenu original d'un fichier si ce dernier existait déjà avant l'ouverture.
		* **"a"**: ouverture du fichier en écriture, à la suite du contenue déjà existant (si le fichier existait déjà; sinon, équivalent à "w").
		* **"r+"**: ouverture en lecture et écriture. Le fichier doit exister.
		* **"w+"**: création et ouverture du fichier en lecture et écriture. Il est important de spécifier que ce type d'ouverture effacera le contenu original d'un fichier si ce dernier existait déjà avant l'ouverture.
		* **"a+"**: ouverture du fichier en lecture et écriture; l'écriture se faisant à la suite du contenue déjà existant (si le fichier existait déjà; sinon, équivalent à "w+").
	* Pour les fichiers binaires, il s'agit des mêmes valeurs de `mode` que pour les fichiers textes, mais en ajoutant le caractère **"b"** à la fin:
		* **"rb"**, **"wb"**, **"ab"**, **"r+b"**, **"w+b"**, **"a+b"**
	* Il est à noter qu'avec des fichiers textes, il peut être dangereux d'utiliser les modes avec de **"+"**. En effet, ces modes permettent de se déplacer dans le fichier (voir `fseek` plus bas). Mais, puisque certains caractères de fichiers textes sont sur plusieurs octets, le résultat pourrait ne pas être ce que le programmeur s'attend.
* La valeur de retour est le pointeur vers le fichier qui permettra d'effectuer des lectures et des écritures.

### [fclose](https://devdocs.io/c/io/fclose)

Permet de fermer et libérer le fichier ouvert précédemment avec `fopen`. 
La signature de la routine est la suivante:

```c
int fclose(FILE* fichier)
```
* L'argument fichier est le pointeur du fichier retourné par `fopen`,
* La valeur de retour est 0 si le fichier a été correctement fermé.

#### Exemple

Voici un exemple d'un programme qui crée un nouveau fichier vide.

```c
#include <stdio.h>

int main(int aParametresTaille, char* aParametres[])
{
    FILE* lFichier = fopen("fichier.txt", "w");
    if (lFichier) {
        printf("Fichier créé\n");
        fclose(lFichier);
    }
    return 0;
}

```

Les fichiers binaires
---------------------

En fait, écrire qu'il existe deux types de fichiers (binaire et texte) est un peu trompeur puisque les fichiers textes sont des cas particuliers de fichier binaire. Les fichiers binaires représentent tous fichiers créés à l'aide d'octets (c'est à dire, tous les fichiers).

Voici les instructions utilisées pour gérer des fichiers binaires en C/C++:

### [fwrite](https://devdocs.io/c/io/fwrite)

Permet d'effectuer une écriture dans le fichier. La signature de la routine est:

```c
int fwrite (void* pointeur, int taille, int nombre, FILE* fichier )
```
* L'argument `pointeur` contient un pointeur vers un espace mémoire contenant l'information qui sera stockée dans le fichier.
	* À noter que cet espace mémoire doit être de taille d'au moins `taille*nombre` octets;
* L'argument `taille` permet d'indiquer la taille de chaque élément à écrire;
	* Il peut être utile d'utiliser `sizeof` avec cet argument.
* L'argument `nombre` représente le nombre d'éléments (ayant `taille` comme taille) à écrire dans cette écriture;
* L'argument `fichier` correspond au pointeur du fichier qui a été retourné par `fopen`.
* La valeur de retour de cette méthode correspond au nombre d'éléments qui ont été écrits dans le fichier.
	* En général, une valeur différente de `nombre` signifie qu'une erreur s'est produite;
	* Il est à noter que pour connaitre la nature de l'erreur, on peut utiliser les instructions [ferror](https://devdocs.io/c/io/ferror). L'utilisation de cette instruction est hors de la portée de ce cours.

Voici un exemple utilisant `fwrite`:

```c
#include <stdio.h>
#include <stdlib.h>

void fibonnaci(int* aTableauEntier, int aTaille)
{
    int i, lNombre1 = 0, lNombre2 = 1;
    if (aTaille > 0) {
        if (aTableauEntier) {
            aTableauEntier[0] = lNombre1;
            for(i = 1; i < aTaille; i = i + 1) {
                aTableauEntier[i] = lNombre2;
                lNombre2 = lNombre1 + lNombre2;
                lNombre1 = aTableauEntier[i];
            }
        }
    }
}

int main(int aParametresTaille, char* aParametres[])
{
    FILE* lFichier = NULL;
    int *lTableau = NULL;
    int lNombre = 0, lError = 0;
    printf("Combien de nombre voulez-vous générer: ");
    scanf("%d", &lNombre);
    if (lNombre > 1) {
        lTableau = calloc(lNombre, sizeof(int));
        fibonnaci(lTableau, lNombre);
        lFichier = fopen("fibonnaci.bin", "wb");
        lError = fwrite(lTableau, sizeof(int), lNombre, lFichier);
        if (lError != lNombre) {
            printf("Une erreur s'est produite.\n");
        }
        free(lTableau);
        fclose(lFichier);
    } else {
        printf("Vous devez entrer un nombre suppérieur à 0.\n");
    }
    return 0;
}
```


### [fread](https://devdocs.io/c/io/fread)

Permet d'effectuer une lecture dans le fichier. La signature de la routine est:

```c
int fread (void* pointeur, int taille, int nombre, FILE* fichier )
```
* L'argument `pointeur` contient un pointeur vers un espace mémoire qui permettra de stocker l'information lu dans le fichier.
	* À noter que cet espace mémoire doit être de taille d'au moins `taille*nombre` octets;
* L'argument `taille` permet d'indiquer la taille de chaque élément à lire;
	* Il peut être utile d'utiliser `sizeof` avec cet argument.
* L'argument `nombre` représente le nombre d'éléments (ayant `taille` comme taille) à lire dans cette lecture;
* L'argument `fichier` correspond au pointeur du fichier qui a été retourné par `fopen`.
* La valeur de retour de cette méthode correspond au nombre d'éléments qui a été lu dans le fichier.
	* En général, une valeur différente de `nombre` signifie que le fichier a été entièrement lu (ou qu'une erreur s'est produite) et que si la lecture se fait dans une boucle, il est temps d'arrêter la boucle;
	* Il est à noter que pour savoir si une erreur s'est produite ou bien si la fin du fichier a été atteinte, on peut utiliser les instructions [ferror](https://devdocs.io/c/io/ferror) et [feof](https://devdocs.io/c/io/feof). L'utilisation de ces instructions est hors de la portée de ce cours.

Voici un exemple utilisant `fread` (dois être lancé après l'exemple de `fwrite` vue plus haut):

```c
#include <stdio.h>

int main(int aParametersCount, char *aParameters[])
{
    long lValeur;
    int lNombre = 1;
    FILE* lFichier = fopen("fibonnaci.bin", "rb");
    if (lFichier) {
        while(lNombre > 0) {
            lNombre = fread(&lValeur, sizeof(long), 1, lFichier);
            if (lNombre == 1) {
                printf("%ld, ", lValeur);
            }
        }
        printf("\n");
        fclose(lFichier);
    } else {
        printf("Le fichier n'existe pas.\n");
    }
    return 0;
}
```

### [fseek](https://devdocs.io/c/io/fseek)

Permet le déplacement dans le fichier (uniquement fonctionnel si ouvert avec un mode contenant **"+"**). La signature de la routine est:

```c
int fseek (FILE* fichier,long  deplacement,int origine)
```
* L'argument `fichier` correspond au pointeur du fichier qui a été retourné par `fopen`;
* L'argument `deplacement` correspond au nombre d'octets à se déplacer par rapport à l'origine (voir argument `origine`);
	* À noter que le déplacement peut être positif ou négatif.
* L'argument `origine` correspond à la référence à partir de laquelle le déplacement s'effectue. Il faut utiliser une des origines suivantes:
	* **`SEEK_SET`**: à partir du début du fichier,
	* **`SEEK_CUR`**: À partir de la position courante,
	* **`SEEK_END`**: À partir de la fin du fichier. Notez que le déplacement doit être négatif.
* La valeur de retour est 0 en cas de succès.
	* Si une erreur s'est produite, retourne une valeur différente de 0.
	* Pour connaitre la nature de l'erreur, on peut utiliser les instructions [ferror](https://devdocs.io/c/io/ferror). L'utilisation de cette instruction est hors de la portée de ce cours.

### [ftell](https://devdocs.io/c/io/ftell)

Permet de savoir la position (en octet) en cours dans le fichier. La signature de la routine est la suivante:

```c
long ftell (FILE* fichier)
```
* L'argument `fichier` correspond au pointeur du fichier qui a été retourné par `fopen`;
* Retourne la position (en octet) en cours dans le fichier.
	* Si une erreur s'est produite, retourne une valeur négative.
	* Pour connaitre la nature de l'erreur, on peut utiliser les instructions [ferror](https://devdocs.io/c/io/ferror). L'utilisation de cette instruction est hors de la portée de ce cours.

Voici un exemple utilisant `fseek` et `ftell`:



```c
#include <stdio.h>

void observer(FILE* aFichier, int aTailleFichier) {
    int lPosition = 0, lErreur = 0;
    char lValeur;
    while (lPosition >= 0){
        printf("Position à observer (-1 pour quitter): ");
        scanf("%d", &lPosition);
        if (lPosition >= 0 && lPosition < aTailleFichier) {
            lErreur = fseek(aFichier, lPosition, SEEK_SET);
            if (lErreur == 0) {
                lErreur = fread(&lValeur, sizeof(char), 1, aFichier);
                if (lErreur == 1) {
                    printf("La valeur: %X\n", lValeur);
                    printf("en ascii %c\n", lValeur);

                } else {
                    printf("Ne peut lire cette valeur.\n");
                }
            } else {
                printf("Une erreur s'est produite.\n");
            }
        } else if (lPosition != -1) {
            printf("La position %d est invalide.\n", lPosition);
        }
    }
}

int main(int aParametersCount, char *aParameters[])
{
    FILE* lFichier;
    char lNomFichier[255];
    int lErreur, lTailleFichier;
    printf("Quel fichier voulez-vous ouvrir: ");
    scanf("%s", lNomFichier);
    lFichier = fopen(lNomFichier, "rb");
    if (lFichier) {
        lErreur = fseek(lFichier, 0, SEEK_END);
        lTailleFichier = ftell(lFichier);
        if (lErreur == 0 && lTailleFichier >= 0) {
            printf("Fichier %s de taille %d ouvert.\n", lNomFichier,
                    lTailleFichier);
            observer(lFichier, lTailleFichier);
        } else {
            printf("Une erreur s'est produite.\n");
        }
    } else {
        printf("Ne peut pas ouvrir le fichier.\n");
    }
    return 0;
}
```


Fichiers textes
---------------

La gestion des fichiers textes est très similaire à l'utilisation de la console avec "stdio.h" (les `printf` et `scanf`). Les instructions de lecture et d'écriture de fichiers textes (`fprintf` et `fscanf`) s'utilisent de la même manière, mais en spécifiant le fichier qui devra être utilisé.

Il est important de bien comprendre que, malgré qu'à la surface, la gestion de fichiers textes soit assez simple, il est particulièrement difficile de gérer tous les formats de fichiers textes. En effet, il existe une grande quantité de formats de fichiers textes qui encode les caractères dans le fichier de manière différente. La gestion de ces formats d'encodage est hors de la portée de ce cours, mais pour les curieux, voici une petite explication pour comprendre ces encodages: [lien](http://sdz.tdct.org/sdz/comprendre-les-encodages.html).

Comme indiqué plus haut, il n'est pas recommandé d'utiliser le déplacement dans un fichier texte. Donc, les instructions de déplacement (`ftell` et `fseek`) ne devrait pas être utilisé dans ce contexte (sauf si vous êtes bien certain d'utiliser un fichier dont tous les caractères sont de même taille; comme l'Ascii, le Latin ou l'UTF-32).

### [fprintf](https://devdocs.io/c/io/fprintf)

Permet d'écrire du texte dans un fichier texte. La signature de la routine est la suivante:

```c
int fprintf (FILE* fichier, char* format, ... )
```
* L'argument `fichier` correspond au pointeur du fichier qui a été retourné par `fopen`;
* L'argument `format` correspond à une chaîne de format (similaire à la chaîne de format de `printf`).
	* Voici quelques "specifiers" qui peuvent être utilisés dans la chaîne de format:
		* **"%d"** (ou **"%i"**): un entier en décimal,
		* **"%u"**: un entier non signé en décimal (un nombre naturel),
		* **"%o"**: un entier non signé en octal,
		* **"%x"**: un entier non signé en hexadécimale minuscule,
		* **"%X"**: un entier non signé en hexadécimale majuscule,
		* **"%f"**: un nombre à virgule en minuscule,
		* **"%F"**: un nombre à virgule en majuscule,
		* **"%e"**: un nombre à virgule en notation scientifique minuscule,
		* **"%E"**: un nombre à virgule en notation scientifique majuscule,
		* **"%c"**: un caractère,
		* **"%s"**: une chaîne de caractère,
		* **"%p"**: un pointeur,
	* Il est à noter que les "specifiers" peuvent avoir des paramètres optionnels entre le `%` et le caractère. Pour plus de détails, voir la documentation [ici](https://devdocs.io/c/io/fprintf).
* Les prochains arguments correspond à une valeur qui sera utilisée pour chaque "specifier" de la chaîne de `format`.
* Si aucune erreur n'est survenue, la valeur de retour correspond au nombre de caractères ayant été écrit dans le fichier.
	* Si une erreur est survenue, la routine retourne une valeur négative.
	* Pour connaitre la nature de l'erreur, on peut utiliser les instructions [ferror](https://devdocs.io/c/io/ferror). L'utilisation de cette instruction est hors de la portée de ce cours.

### [fscanf](https://devdocs.io/c/io/fscanf)

Permet de lire du texte dans un fichier texte. La signature de la routine est la suivante:

```c
int fscanf(FILE* fichier, char* format, ... )
```
* L'argument `fichier` correspond au pointeur du fichier qui a été retourné par `fopen`;
* L'argument `format` correspond à une chaîne de format (similaire à la chaîne de format de `scanf`).
	* En général, la chaîne de format d'un `fscanf` (ou d'un `scanf`) utilise seulement des "specifiers".
	* Les "specifiers" utilisés par `fscanf` sont les mêmes que pour `fprintf` (voir plus haut.)
	* Pour lire des lignes complètes, veuillez utiliser `fgets`.


### [fputs](https://devdocs.io/c/io/fputs)

Permet d'écrire des lignes d'un fichier texte. La signature de la routine est la suivante:

```c
int fputs(char* chaine,  FILE* fichier)
```
* L'argument `chaine` contient une chaine de caractère permettant de stocker la ligne;
* L'argument `fichier` correspond au pointeur du fichier qui a été retourné par `fopen`;
* La valeur de retour est une valeur non-negative en cas de succes ou EOF en cas d'erreur.
* À noter que le caractère NULL de la fin n'est pas écrit dans le fichier.

!> Notez que fputs n'offre pas de formattage pour les nombres. Seul une chaine de caractère peut être écrite. 

Exemple démontrant l'écriture dans un fichier texte:

!> souvenez vous du nom du fichier que vous utiliserez car nous allons relire le fichier dans l'exemple suivant. 

```c
#include <stdio.h>
#include <string.h>

#define TAILLE_CHAINE 255

int main(int aParametersCount, char *aParameters[])
{
    char lNomFichier[TAILLE_CHAINE], lLigne[TAILLE_CHAINE];
    FILE* lFichier = NULL;
    printf("Quel fichier voulez-vous créer: ");
    scanf("%s", lNomFichier);

    fgets(lLigne, TAILLE_CHAINE * sizeof(char), stdin); // Lecture du \n laissé par le scanf
    lFichier = fopen(lNomFichier, "w");
    if (lFichier) {
        printf("Ajoutez des lignes au fichier (ligne vide pour terminer):\n");
        do {
            fgets(lLigne, TAILLE_CHAINE * sizeof(char), stdin);
            if (strcmp(lLigne, "\n") != 0) {
                fputs(lLigne, lFichier);
            }
        }while (strcmp(lLigne, "\n") != 0);
        fclose(lFichier);
    } else {
        printf("Ne peut pas créer le fichier.\n");
    }
    return 0;
}
```

### [fgets](https://devdocs.io/c/io/fgets)

Permet de lire des lignes d'un fichier texte. La signature de la routine est la suivante:

```c
char* fgets(char* chaine, int taille, FILE* fichier)
```
* L'argument `chaine` contient une chaine de caractère permettant de stocker la ligne;
* L'argument `taille` permet de spécifier la taille de la `chaine`.
* L'argument `fichier` correspond au pointeur du fichier qui a été retourné par `fopen`; 
* Notez que jusqu'à maintenant, nous avons utilisé `stdin` comme argument pour le fichier. `stdin` signifie `standard input`, ce qui correspond généralement au clavier. En effet, le clavier peut être considéré comme un fichier (ou un `stream` plus exactement). Mais nous n'irons pas plus loin dans cette discussion. 
* La valeur de retour est le pointeur `chaine` ou NULL en cas d'erreur.
* La différence majeure entre scanf et fgets est le fait que fgets n'offre pas de format pour le texte. Tout est lu dans une chaine de caractères. La conversion en entier ou float devra se faire à l'aide d'appel de fonctions ([strtok](https://devdocs.io/c/string/byte/strtok) [atoi](https://devdocs.io/c/string/byte/atoi) [atof](https://devdocs.io/c/string/byte/atof)) (voir exemple suivant)
* Si `stdin` est utilisé, il est à noter que le `\n` sera introduit dans la chaine. Pour l'enlever, voici un exemple:

```c
char toto[10];
fgets(toto, sizeof toto, stdin);
toto[strcspn(toto, "\n")] = 0;
```
Qui trouvera la position de \n dans toto à l'aide de strcspn, et remplacera le caractère à cette position par le caractère Null (0).


Exemple démontrant la lecture d'un fichier texte:

```c
#include <stdio.h>
#include <string.h>

#define TAILLE_CHAINE 255


int main(int aParametersCount, char *aParameters[])
{
    char lNomFichier[TAILLE_CHAINE], lLigne[TAILLE_CHAINE];
    char* lResultat;
    FILE* lFichier = NULL;
    printf("Quel fichier voulez-vous lire: ");
    scanf("%s", lNomFichier);
    lFichier = fopen(lNomFichier, "r");
    if (lFichier) {
        printf("Voici les lignes du fichier:\n");
        do {
            lResultat = fgets(lLigne, TAILLE_CHAINE * sizeof(char), lFichier);
            if(lResultat) {
                printf("%s", lLigne);
            }
        }while (lResultat);
        fclose(lFichier);
    } else {
        printf("Ne peut pas ouvrir le fichier.\n");
    }
    return 0;
}
```

### Utilisation de strtok, atoi, atof

`strtok` (string token) permet d'extraire une partie d'un texte. 

Le chaine de caractère est lue jusqu'au caractère de séparation. 

Exemple:

```c
char texte2[] = "du texte, separe par une virgule, et encore une virgule";
printf("%s\n", strtok(texte2, ","));
```
retournera "du texte"

Pour lire la suite, il faut utiliser le pointeur null 

```c
printf("%s\n", strtok(NULL, ","));
```

retournera " separe par une virgule"
Notez qu'il y a une espace devant `separe`

`atoi` (ascii to integer) converti une chaine de caractères ascii en entier. 

`atof` (ascii to float) converti une chaine de caractères ascii en float. 


voyons un exemple complet:

```c
#include <stdlib.h>
#include <stdio.h>
#include <string.h>


int main(int aParametersCount, char *aParameters[])
{
    char texteADecomposer[] = "du texte suivi d'un entier, 1234, suivi d'un float, 3.145";
    char *texte;
    int chiffreEntier;
    float chiffreFloat;
    
    char texte2[] = "du texte, separe par une virgule, et encore une virgule";
    printf("debut: \"%s\"\n", strtok(texte2, ","));
    printf("milieu : \"%s\"\n", strtok(NULL, ","));
    printf("fin : \"%s\"\n\n", strtok(NULL, ","));
    
    printf("le texte a decomposer : %s\n", texteADecomposer);
    texte = strtok(texteADecomposer, ",");
    printf("debut : \"%s\" \n", texte);
    
    texte = strtok(NULL, ","); // l'utilisation de NULL fait en sorte que strtok continue sa lecture dans le texte de l'appel précédent
    printf("nombre en str : \"%s\"\n", texte);
    chiffreEntier = atoi(texte);
    printf("nombre en int : %i\n", chiffreEntier);
    
    texte = strtok(NULL, ","); // on continue la lecture de texte
    printf("milieu : \"%s\"\n", texte);
    
    texte = strtok(NULL, ","); // lecture du float à la fin
    printf("nombre en str : \"%s\"\n", texte);
    chiffreFloat = atof(texte);
    printf("nombre en float : %f\n", chiffreFloat);

    return 0;
}


```