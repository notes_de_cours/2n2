# Listes chainées et allocation dynamique <!-- omit in toc -->

Nous allons reprendre  le code ayant servi à gérer la liste chainée de l'exemple précédant, mais cette fois-ci, au lieu d'avoir une structure ne contenant qu'un entier, nous allons avoir une structure contenant une autre structure contenant des données allouées sur le tas. 

Nous allons donc remplacer
```c
typedef struct structElement element;
struct structElement
{
    int nombre;
    element *suivant;
};
```

par 

```c
typedef struct structAdresse
{
    int noCivic;
    char *rue;
} adresse;

typedef struct structPersonne personne;
struct structPersonne
{
    char *nom;
    adresse *adresseCivic;
    personne *suivant;
};
```



Voici le code de base:

```c
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define LONGUEUR_ADRESSE 40

typedef struct structAdresse
{
    int noCivic;
    char *rue;
} adresse;

typedef struct structPersonne personne;
struct structPersonne
{
    char *nom;
    adresse *adresseCivic;
    personne *suivant;
};

typedef struct structListe
{
    personne *premier;
} liste;

liste *initialiserListe()
{
    liste *lListe = calloc(1, sizeof(liste));

    if (lListe == NULL ) // vérification de l'allocation de l'espace mémoire
    {
        exit(EXIT_FAILURE); //nous n'avons pas vue cette fonction, mais elle fait ce qu'elle dit
                            //... elle termine le programme en retournant un code de sortie
                            // ici c'est un code d'erreur qui est prédéfini
    }
    lListe->premier = NULL;
    return lListe;
}

void insererElementAuDebut(liste *aListe, char *aNom, char *aRue, int aNoCivic)
{
    /* Création du nouvel élément */

    personne *nouvellePersonne = calloc(1, sizeof(personne));
    adresse *lAdresse = calloc(1,sizeof(adresse));
    if (aListe == NULL || nouvellePersonne == NULL || lAdresse == NULL)
    {
        exit(EXIT_FAILURE);
    }

    // eh oui, les char * doivent aussi être alloués
    nouvellePersonne->nom = calloc(LONGUEUR_ADRESSE, sizeof(char));
    lAdresse->rue = calloc(LONGUEUR_ADRESSE, sizeof(char));
    strcpy(nouvellePersonne->nom, aNom);
    strcpy(lAdresse->rue, aRue);
    lAdresse->noCivic = aNoCivic;

    //assigne l'adresse à la personne
    nouvellePersonne->adresseCivic = lAdresse;

    /* Insertion de l'élément au début de la liste */
    nouvellePersonne->suivant = aListe->premier;
    aListe->premier = nouvellePersonne;
}


void supprimerPremier(liste *aListe)
{
    personne *lPersonneASupprimer;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }

    if (aListe->premier != NULL)
    {
        lPersonneASupprimer = aListe->premier;
        aListe->premier = aListe->premier->suivant;

        //il faut maintenant libérer tout ce qui a été alloué dynamiquement
        //  la personne
        //      son nom
        //  l'adresse
        //      le nom de la rue
        // le tout dans le bon ordre: en partant de la fin et en revenant vers le début

        free(lPersonneASupprimer->nom);
        free(lPersonneASupprimer->adresseCivic->rue);
        free(lPersonneASupprimer->adresseCivic);
        free(lPersonneASupprimer);
    }
}

void afficherListe(liste *aListe)
{
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }

    personne *lPersonne = aListe->premier;
    while (lPersonne != NULL)
    {
        printf("nom %s, adresse %i %s\n ", lPersonne->nom,
               lPersonne->adresseCivic->noCivic,
               lPersonne->adresseCivic->rue);
        lPersonne = lPersonne->suivant;
    }
}

int main()
{
    liste *maListe = initialiserListe();

    insererElementAuDebut(maListe, "Benoit", "Une Rue", 4);
    insererElementAuDebut(maListe, "Michel", "Autre rue", 18);
    insererElementAuDebut(maListe, "Bob", "La Ruelle", 1);
    supprimerPremier(maListe);

    afficherListe(maListe);
    free(maListe);  //<---- est-ce ok de faire ca ?
    return 0;
}

```

Comme vous pouvez le remarquer, la différence majeure est lors de la création et de la destruction. Il faut faire des calloc, ce qui implique des free.

