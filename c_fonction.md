# Les fonctions en C <!-- omit in toc -->

Comme appris en Java, un programme est décomposé en fonctions. Le langage C utilise aussi les fonctions afin de séparer la logique de l'application.

Comme vous connaissez déjà le fonctionnement des fonctions en Java, nous allons en faire un survol très rapide. 

# 1. Les fonctions
Nous avons déjà vu qu'un programme en C commençait par une fonction appelée main. 

![alt](_media/c_main.png)

En haut, on y trouve les directives de préprocesseur. Ces directives sont  faciles à identifier : elles commencent par un **#** et sont généralement mises tout en haut des fichiers sources.

**stdio.h** et **stdlib.h** contiennent la plupart des fonctions de base dont on a besoin dans un programme.

**stdio.h** en particulier contient des fonctions permettant d'afficher des choses à l'écran (comme printf), mais aussi de demander à l'utilisateur de taper quelque chose (ce sont des fonctions que l'on verra plus tard). Pour plus d'information, voir [https://pubs.opengroup.org/onlinepubs/7908799/xsh/stdio.h.html](https://pubs.opengroup.org/onlinepubs/7908799/xsh/stdio.h.html)

La fonction main est la fonction principale de votre programme. Le programme commence toujours par l'appel de la fonction main. Cette fonction appelle ensuite d'autres fonctions. 

Une fonction exécute des actions et renvoie un résultat. C'est un morceau de code qui sert à faire quelque chose de précis. On dit qu'une fonction possède une entrée et une sortie. La figure suivante représente une fonction schématiquement.

![alt](_media/fonction_schema.jpg)

Lorsqu'on appelle une fonction, il y a trois étapes.
1.	L’entrée : on fait « rentrer » des informations dans la fonction (en lui donnant des informations avec lesquelles travailler).
2.	Les calculs : grâce aux informations qu'elle a reçues en entrée, la fonction exécute une série d'instructions.
3.	La sortie : une fois qu'elle a fini ses calculs, la fonction renvoie un résultat. C'est ce qu'on appelle la sortie, ou encore le retour.

Les lignes de code à l'intérieur d'une fonction sont appelées **instructions**. Toute instruction se termine obligatoirement par un point-virgule **;**.

C'est d'ailleurs comme ça qu'on reconnaît une instruction. Si vous oubliez de mettre un point-virgule à la fin d'une instruction, votre programme ne compilera pas !

Habituellement, une fonction se termine par l'instruction **return()**. Ici, **return 0;** indique qu'on arrive à la fin de notre fonction main et demande de renvoyer la valeur 0.

Pourquoi mon programme renverrait-il le nombre 0 ?
Chaque **programme**, une fois terminé, renvoie une valeur. Habituellement, la valeur 0 indique que tout s'est bien passé. Toute autre valeur indique habituellement une erreur.

## 1.1. Schéma d'une fonction

En C, une fonction respecte le format suivant

![alt](_media/schema_fonction.png)

- type : (correspond à la sortie) c'est le type de la valeur retournée par la fonction. Comme les variables, les fonctions ont un type. Ce type dépend du résultat que la fonction renvoie : si la fonction renvoie un nombre décimal, vous mettrez sûrement `double`, si elle renvoie un entier vous mettrez int ou long par exemple. Il y a donc deux sortes de fonctions :
    - les fonctions qui renvoient une valeur : on leur met un des types que l'on connaît (`char, int, double`, etc.)  
    - les fonctions qui ne renvoient pas de valeur : on leur met un type spécial `void` (qui signifie « vide »).
- nomFonction : c'est le nom de votre fonction. Vous pouvez appeler votre fonction comme vous voulez, autant que vous respectiez les règles que nous avons établies précédemment.
- paramètres : (correspond à l'entrée) : entre parenthèses, vous pouvez envoyer des paramètres à la fonction. Ce sont  les valeurs avec lesquelles la fonction va travailler.


Vous remarquerez qu'il n'y a pas de modificateur d'accès (`private, public`) comme il y en avait en Java (bien qu'il soit optionnel). Ce concept n'existe pas en C. 

Par défaut, une fonction est visible par toutes les autres fonctions du programme. Il existe une exception en C : une fonction définie comme étant `static`. Sans entrer dans les détails, une fonction static n'est visible que dans le fichier dans lequel elle est déclarée (n'oubliez pas qu'un programme peut être constitué de plusieurs fichiers .c ). Pour l'instant, nous n'utiliserons pas cette spécificité. Je vous invite à faire vos recherches. Notez qu'en C `static` n'a pas la même signification qu'en Java ou C++. 

## 1.2. Exemple d'appel de fonction

```c
#include <stdio.h>
int doubler(int aNombre);

int main()
{
    printf("%i", doubler(2));
    return 0;
}

int doubler(int aNombre) {
    return aNombre*2;
}
```

**À noter**
- la fonction doubler() est pré-déclarée (prototype ou signature) au début du code;
- la fonction doubler() est ensuite définie après main();
- main() devrait toujours être la première fonction définie. 

Si vous enlevez la déclaration de doubler(), vous aurez un avertissement. Selon le mode de compilation, le code va compiler ou non. 

# 2. .c et .h

Dès que le code commence à être un peu plus complexe, il est d'usage de séparer les fonctions dans plusieurs fichiers. Mais comment faire pour qu'une fonction définie dans un fichier soit accessible dans un autre ?

Reprenons notre exemple ci-haut, mais en le séparant en plusieurs fichiers.


Nous commencerons par créer un .h contenant la définition des fonctions (ici nous n'en avons qu'une)

Nous aurons donc le fichier `fonctions_math.h` permettant de déclarer notre fonction doubler()
```c
#ifndef FONCTION_MATH_H
#define FONCTION_MATH_H

int doubler(int aNombre);
#endif // FONCTION_MATH_H

```

Nous reviendrons sur `#ifndef` 


Ensuite, `fonctions_math.c` permettant de définir cette fonction. 
```c
#include <stdio.h>

int doubler(int aNombre) {
    return aNombre*2;
}
```

Notez que `stdio.h` est inclus ici à titre d'exemple. Il ne sert pas pour cet exemple, mais si le code contenait un printf ou scanf, il serait nécessaire de faire l'include. 

Et finalement le programme principal `main.c`
```c
#include <stdio.h>
#include "fonctions_math.h"

int main()
{
    printf("%i", doubler(2));
    return 0;
}
```

Notez l'include de `"fonction_math.h"` qui est entre guillemets. 

!> À partir de maintenant, vous devez diviser votre code de cette facon. Généralement, le fichier main.c ne devrait contenir que le code pour démarrer le programme, soit l'appel d'une fonction se trouvant dans un autre .c. Pour nos application console, il est acceptable d'avoir le menu principal dans main.c. 

!> Notez que pour des raisons pédagogiques, je vous présenterai souvent du code sans le séparer en .c et .h. Mais c'est uniquement pour simplifier la présentation, ce n'est pas une bonne pratique. 


## 2.1. #ifndef

Comme nous l'avons vu, il est habituel d'inclure tous les `.h` qui serviront dans un `.c`. Par exemple, bien que `main.c` inclus `stdio.h`, il est recommandé de le ré-inclure dans les autres `.c` car on ne peut pas présumer qu'il aura été inclus précédemment. 

Par contre, si un symbole est défini 2 fois, une erreur sera générée. 

C'est donc à ça que sert le `#ifndef` (if not defined). Le précompilateur vérifie si ce symbole (MENU_H) est défini. 

Si le symbole n'est pas défini, il le définit et tout le code jusqu'au `#endif` est compilé. 

Si le symbole existe, cela indique que ce fichier a déjà été inclus et ne doit donc pas être compilé une deuxième fois. 

Notez que `#ifndef` peut avoir d'autre utilisations.



<!-- ajouter une section sur le fait que toto.c doit avoir toto.h et que les fonctions doivent y être déclarées -->