#ifndef STRUCTURES_H
#define STRUCTURES_H

#define LONGUEUR_CHAMPS 30

typedef struct personne
{
    char *nom;
    char *prenom;
} personne;

typedef struct animal animal;
struct animal
{
    char *nom;
    personne *proprietaire;
    animal *suivant;
};

typedef struct animaux
{
    animal *premier;
    int taille;
} animaux;

#endif // STRUCTURES_H
