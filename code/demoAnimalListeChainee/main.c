#include <stdio.h>
#include "structures.h"
#include "gestionAnimal.h"


int main()
{
    int lChoixPrincipale;
    int index = 0;

    animaux *listeAnimaux = initialisationAnimaux();
    do {

        printf("Bienvenue a la Clinique Veterinaire chez Benou\n");
        printf("1) inscrire un animal\n");
        printf("2) afficher un animal\n");
        printf("3) modifier un animal\n");
        printf("4) effacer un animal\n");
        printf("5) afficher tous les animaux\n");
        printf("6) effacer la liste au complet\n");
        printf("0) sortir\n");
        scanf("%i", &lChoixPrincipale);
        switch(lChoixPrincipale) {
        case 1:
            inscrireAnimal(listeAnimaux);
            break;
        case 2:
            printf("Index de l'animal :");
            scanf("%i", &index);
            afficherAnimalParIndex(listeAnimaux, index);

            break;
        case 3:
            printf("Index de l'animal :");
            scanf("%i", &index);
            modifierAnimalParIndex(listeAnimaux, index);
            break;
        case 4:
            printf("Index de l'animal :");
            scanf("%i", &index);
            effacerAnimalParIndex(listeAnimaux, index);
            break;
        case 5:
            afficherAnimaux(listeAnimaux);
            break;
        case 6:
            effacerTousLesAnimaux(listeAnimaux);
            break;
        }
    } while(lChoixPrincipale !=0);
}
