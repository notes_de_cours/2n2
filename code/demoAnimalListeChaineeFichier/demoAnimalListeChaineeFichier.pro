TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        gestionAnimal.c \
        main.c

HEADERS += \
        gestionAnimal.h \
        structures.h
