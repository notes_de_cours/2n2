#ifndef STRUCTURES_H
#define STRUCTURES_H

#define LONGUEUR_CHAMPS 30

typedef struct personne
{
    char *nom;
    char *prenom;
} personne;

typedef struct animal animal;
struct animal
{
    char *nom;
    personne *proprietaire;
    animal *suivant;
};

typedef struct animalFichier
{
    char nom[LONGUEUR_CHAMPS];
} animalFichier;

typedef struct personneFichier
{
    char nom[LONGUEUR_CHAMPS];
    char prenom[LONGUEUR_CHAMPS];
} personneFichier;

typedef struct animaux
{
    animal *premier;
    int taille;
} animaux;

#endif // STRUCTURES_H
