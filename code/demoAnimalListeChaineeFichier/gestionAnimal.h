#ifndef GESTIONANIMAL_H
#define GESTIONANIMAL_H
#include "structures.h"


animaux *initialisationAnimaux();
void inscrireAnimal(animaux *listeAnimaux);
void insererAuDebut(animaux *aListe, char *aNom,
                    char *aNomProprio, char *aPrenomProprio);
void afficherAnimal(animal *aAnimal);
void afficherAnimaux(animaux *aListe);
animal *trouverAnimalParIndex(animaux *aListe, int aIndex);
void afficherAnimalParIndex(animaux *aListe, int aIndex);
void modifierAnimalParIndex(animaux *aListe, int aIndex);
void liberer(animal *aAnimal);
void effacerAnimalParIndex(animaux *aListe, int aIndex);
void effacerTousLesAnimaux(animaux *aListe);

void sauvegarderAnimaux(animaux *aListe, char *aNomFichier);
void chargerAnimaux(animaux *aListe, char *aNomFichier);

#endif // GESTIONANIMAL_H
