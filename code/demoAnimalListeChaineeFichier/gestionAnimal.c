#include "structures.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


animaux *initialisationAnimaux() {
    animaux *lListe = calloc(1, sizeof(animaux));

    if (lListe == NULL ) // vérification de l'allocation de l'espace mémoire
    {
        exit(EXIT_FAILURE);
    }
    lListe->premier = NULL;
    lListe->taille = 0;
    return lListe;
}

void insererAuDebut(animaux *aListe, char *aNom,
                    char *aNomProprio,
                    char *aPrenomProprio)
{
    animal *lAnimal = calloc(1, sizeof(animal));
    personne *lProprio = calloc(1, sizeof(personne));
    char *lNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lPrenomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lNomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    if(!lAnimal || !lProprio || !lNom || !lPrenomProprio || !lNomProprio)
    {
        exit(EXIT_FAILURE);
    }

    strcpy(lNom, aNom);
    strcpy(lPrenomProprio, aPrenomProprio);
    strcpy(lNomProprio, aNomProprio);

    lAnimal->nom = lNom;
    lProprio->prenom = lPrenomProprio;
    lProprio->nom = lNomProprio;

    lAnimal->proprietaire = lProprio;

    //insère l'animal au début de la liste
    lAnimal->suivant = aListe->premier;
    aListe->premier = lAnimal;
    aListe->taille++;

}


void inscrireAnimal(animaux *aListe)
{
    char *lNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lNomProprio= calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lPrenomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    printf("Nom de l'animal :");
    scanf("%s",lNom);
    printf("Prenom du proprio :");
    scanf("%s",lPrenomProprio);
    printf("Nom du proprio :");
    scanf("%s",lNomProprio);

    insererAuDebut(aListe, lNom, lNomProprio, lPrenomProprio);
}

void afficherAnimal(animal *aAnimal) {
    printf("Nom :            %s\n", aAnimal->nom);
    printf("Nom du proprio : %s\n", aAnimal->proprietaire->nom);
    printf("Prenom :         %s\n", aAnimal->proprietaire->prenom);
    printf("-------------------\n");
}

void afficherAnimaux(animaux *aListe) {
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    char animal_aux[8]="animaux";
    if(aListe->taille == 1) {
        strcpy(animal_aux, "animal");
    }
    printf("il y a presentement %i %s dans la liste \n", aListe->taille, animal_aux);
    animal *actuel = aListe->premier;
    while (actuel != NULL)
    {
        afficherAnimal(actuel);
        actuel = actuel->suivant;
    }
    printf("--------------------------\n");

}

animal *trouverAnimalParIndex(animaux *aListe, int aIndex)
{
    int lPosition=1;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    animal *actuel = aListe->premier;
    while(lPosition < aIndex && actuel != NULL)
    {
        lPosition++;
        actuel = actuel->suivant;
    }
    return actuel;
}
void afficherAnimalParIndex(animaux *aListe, int aIndex)
{
    animal *actuel = trouverAnimalParIndex(aListe, aIndex);
    if(actuel) {
        afficherAnimal(actuel);
    } else {
        printf("Animal non trouve!\n");
    }

}

void modifierAnimalParIndex(animaux *aListe, int aIndex)
{
    char *nouveauNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    animal *actuel = trouverAnimalParIndex(aListe, aIndex);
    if(actuel) {
        printf("Valeurs actuelles : \n");
        afficherAnimal(actuel);
        printf("Entrez le nouveau nom de l'animal :");
        scanf("%s", nouveauNom );
        strcpy(actuel->nom, nouveauNom);
        printf("Nouvelles valeurs : \n");
        afficherAnimal(actuel);
    } else {
        printf("Animal non trouve!\n");
    }
}

void liberer(animal *aAnimal)
{
    free(aAnimal->nom);
    free(aAnimal->proprietaire->nom);
    free(aAnimal->proprietaire->prenom);
    free(aAnimal->proprietaire);
    free(aAnimal);
}
void effacerAnimalParIndex(animaux *aListe, int aIndex)
{
    animal *actuel;
    animal *lEffacer;
    if(aListe->taille == 0) {
        printf("la liste est deja vide! \n");
    } else {
        if(aIndex== 1) { // début de la liste, et liste non-vide
            lEffacer = aListe->premier;
            aListe->premier=lEffacer->suivant;
            liberer(lEffacer);
            aListe->taille--;
        } else { // un élément autre que le premier
            actuel = trouverAnimalParIndex(aListe, aIndex-1 ); // -1 pour arrêter a celui d'avant

            if(actuel && actuel->suivant) { //actuel est le précédent
                //actuel peut donc être là, mais le suivant (que l'on cherche) ne le soit pas
                lEffacer = actuel->suivant;
                actuel->suivant = actuel->suivant->suivant;
                liberer(lEffacer);
                aListe->taille--;
            } else {
                printf("Animal non trouve!\n");
            }
        }
    }
}

void effacerTousLesAnimaux(animaux *aListe)
{
    animal *actuel;
    animal *suivant;

    actuel = aListe->premier;
    while(actuel) {
        suivant = actuel->suivant;
        liberer(actuel);
        actuel = suivant;
    }
    aListe->premier = NULL;
    aListe->taille = 0;
}

void sauvegarderAnimaux(animaux *aListe, char *aNomFichier)
{
    FILE *lFichier = fopen(aNomFichier, "w");
    animalFichier lAnimalFichier;
    animal *lAnimalASauvegarder = aListe->premier;
    for(int i=0; i<aListe->taille; i++) {
        strncpy(lAnimalFichier.nom, lAnimalASauvegarder->nom, sizeof(lAnimalFichier.nom) );
        fwrite(&lAnimalFichier, sizeof(lAnimalFichier), 1, lFichier);

        personneFichier lPersonneFichier;
        strncpy(lPersonneFichier.nom, lAnimalASauvegarder->proprietaire->nom, sizeof(lPersonneFichier.nom));
        strncpy(lPersonneFichier.prenom, lAnimalASauvegarder->proprietaire->prenom, sizeof(lPersonneFichier.prenom));
        fwrite(&lPersonneFichier, sizeof(personneFichier), 1, lFichier);

        lAnimalASauvegarder = lAnimalASauvegarder->suivant;
    }

    fclose(lFichier);
}



void chargerAnimaux(animaux *aListe, char *aNomFichier)
{
    FILE *lFichier = fopen(aNomFichier, "r");
    animalFichier lAnimalACharger;
    animal *lAnimalEnMemoire;
    personneFichier lPersonneACharger;
    personne *lPersonneEnMemoire;

    animal *lAnimalEnMemoirePrecedent = NULL;
    int continuer = 0;
    do {
        continuer = fread(&lAnimalACharger, sizeof(animalFichier), 1 , lFichier);
        if(continuer) {

            continuer = fread(&lPersonneACharger, sizeof(personneFichier), 1, lFichier);
            if(continuer) {
                lPersonneEnMemoire = calloc(1, sizeof(personne));
                lPersonneEnMemoire->nom = calloc(LONGUEUR_CHAMPS, sizeof(char));
                lPersonneEnMemoire->prenom = calloc(LONGUEUR_CHAMPS, sizeof(char));
                strncpy(lPersonneEnMemoire->nom, lPersonneACharger.nom, LONGUEUR_CHAMPS);
                strncpy(lPersonneEnMemoire->prenom, lPersonneACharger.prenom, LONGUEUR_CHAMPS);

                lAnimalEnMemoire = calloc(1, sizeof(animal));
                lAnimalEnMemoire->nom = calloc(LONGUEUR_CHAMPS, sizeof(char));
                strncpy(lAnimalEnMemoire->nom, lAnimalACharger.nom, LONGUEUR_CHAMPS);
                lAnimalEnMemoire->proprietaire=lPersonneEnMemoire;
                lAnimalEnMemoire->suivant = NULL;
                if(lAnimalEnMemoirePrecedent) {
                    lAnimalEnMemoirePrecedent->suivant = lAnimalEnMemoire;
                    aListe->taille++;
                } else {
                    aListe->premier = lAnimalEnMemoire;
                    aListe->taille = 1;
                }
                lAnimalEnMemoirePrecedent = lAnimalEnMemoire;
            } else {
                printf("oups, un animal sans proprio, y'a un probleme");
            }
        }
    } while (continuer);
    fclose(lFichier);

}




