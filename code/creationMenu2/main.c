#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "structurerepas.h"
#include "gestionRepas.h"


void simulation(){
    menu lMenu; //sur la pile
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;

    //appel de creerMenu(&lMenu) afin de remplir le menu
    menu *aMenu = &lMenu; //dans creerMenu on a donc un menu *

    repas *lRepas;
    lRepas = calloc(1, sizeof(repas)); //sur le tas
    strcpy(lRepas->titre,"menu midi"); //simule le scanf
    aMenu->listeRepas = calloc(1, sizeof(repas*)); //creation du tableau de pointeur. Notez repas*
    aMenu->listeRepas[0] = lRepas; //pointe le premier pointeur du tableau vers le repas
    aMenu->nombreRepas++; //on a un repas de plus

    //appel de creerItem(lRepas) afin d'ajouter les items
    //dans ce cas, lRepas est déjà un pointeur, donc pas besoin de &lRepas
    repas *aRepas = lRepas;
    aRepas->listeItem = calloc(1, sizeof(item)); //creation du tableau d'item. Notez item n'a pas de *
    strcpy(aRepas->listeItem[0].titre, "soupe"); //simule le scanf
    aRepas->listeItem[0].prix = 3.99;
    aRepas->nombreItem++; //on a un item de plus


    // et voila, on a maintenant un menu avec un repas et un item
    afficherMenu(lMenu);
}

int main()
{
    //simulation();
    //exit(0);
    menu lMenu;
    char *lNomFichier = "menu.txt";
    char entree[10];
    int choix =0;
    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    do {
        printf("1) ajouter un menu\n");
        printf("2) afficher un menu\n");
        printf("3) sauvegarder un menu\n");
        printf("4) charger un menu\n");
        printf("5) vider le menu\n");
        printf("0) sortir\n");
        printf("votre choix: ");
        fgets(entree, sizeof entree, stdin);
        choix= atoi(entree);

        if(choix == 1) {
            creerMenu(&lMenu);

        } else if(choix == 2) {
            afficherMenu(lMenu);
        } else if(choix == 3) {
            sauvegarderMenu(lMenu, lNomFichier);
        } else if(choix == 4) {
            chargerMenu(&lMenu, lNomFichier);
        } else if(choix == 5) {
            viderMenu(&lMenu);
        }
    } while(choix != 0);


    viderMenu(&lMenu);
}
