#ifndef GESTIONREPAS_H
#define GESTIONREPAS_H
#include "structurerepas.h"

void creerItem(repas *aRepas);
void creerMenu(menu *aMenu);
void afficherMenu(menu aMenu);
void sauvegarderMenu(menu aMenu, char *aNomFichier);
void chargerMenu(menu *aMenu, char *aNomFichier);
void viderMenu(menu *aMenu);
#endif // GESTIONREPAS_H
