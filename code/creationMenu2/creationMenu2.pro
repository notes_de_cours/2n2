TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        gestionRepas.c \
        main.c

HEADERS += \
        gestionRepas.h \
        structurerepas.h
