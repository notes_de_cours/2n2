#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "structurerepas.h"


/**
 * @brief création d'un item qui sera mis dans un repas.
 *
 * La liste des items est un tableau d'item.
 *
 * @param aRepas le repas dans lequel l'item sera ajouté
 */
void creerItem(repas *aRepas){
    char continuer[3];
    char prix[10];
    do {
        if(!aRepas->listeItem) {
            aRepas->listeItem = calloc(1, sizeof(item));
        } else {
            aRepas->listeItem = realloc(aRepas->listeItem, sizeof(item)*(aRepas->nombreItem+1));
        }

        printf("Titre de l'item: ");
        fgets( aRepas->listeItem[aRepas->nombreItem].titre,
                sizeof aRepas->listeItem[aRepas->nombreItem].titre, stdin);
        //fgets capture le \n
        //cette passe-passe remplace le  \n par un 0
        aRepas->listeItem[aRepas->nombreItem].titre[strcspn(aRepas->listeItem[aRepas->nombreItem].titre, "\n")] = 0;

        printf("Prix de l'item: ");
        fgets( prix, sizeof prix, stdin );
        aRepas->listeItem[aRepas->nombreItem].prix = atof(prix);
        aRepas->nombreItem++;
        printf("un autre item (o/n): ");
        fgets(continuer, sizeof continuer, stdin);

    } while(continuer[0] == 'o');
}

/**
 * @brief création d'un menu en y insérant des repas
 *
 * La liste des repas est un tableau de pointeur vers les repas.
 *
 * @param aMenu le menu dans lequel les repas sont insérés
 */
void creerMenu(menu *aMenu){
    repas *lRepas;
    char continuer[3];
    do {
        lRepas = calloc(1, sizeof(repas));
        printf("Titre du menu: ");
        fgets(lRepas->titre, sizeof lRepas->titre, stdin);
        lRepas->titre[strcspn(lRepas->titre, "\n")] = 0;
        if(!aMenu->listeRepas) {
            aMenu->listeRepas = calloc(1, sizeof(repas*));
        } else {
            aMenu->listeRepas = realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
        }
        aMenu->listeRepas[aMenu->nombreRepas] = lRepas;
        aMenu->nombreRepas++;
        creerItem(lRepas);
        printf("un autre menu (o/n): ");
        fgets(continuer, sizeof continuer, stdin);
    } while(continuer[0] == 'o');

}

/**
 * @brief affichage d'un menu au complet
 * @param aMenu le menu à afficher
 */
void afficherMenu(menu aMenu) {
    for(int i= 0; i<aMenu.nombreRepas; i++) {
        printf("%i %s\n", i+1, aMenu.listeRepas[i]->titre);
        for(int j = 0; j< aMenu.listeRepas[i]->nombreItem; j++){
            printf("   %s %3.2f\n", aMenu.listeRepas[i]->listeItem[j].titre,aMenu.listeRepas[i]->listeItem[j].prix );
        }
    }
}

/**
 * @brief sauvegarde sur disque du menu au complet
 *
 * par simplicité, le fichier est écrasé à chaque écriture
 *
 * @param aMenu le menu à sauvegarder
 * @param aNomFichier le nom du fichier dans lequel sauvegarder
 */
void sauvegarderMenu(menu aMenu, char *aNomFichier) {
    FILE *lFichier = fopen(aNomFichier, "w");
    repasFichier lRepasASauvegarder;
    for(int i= 0; i<aMenu.nombreRepas; i++) {
        strcpy(lRepasASauvegarder.titre, aMenu.listeRepas[i]->titre);
        lRepasASauvegarder.nombreItem =  aMenu.listeRepas[i]->nombreItem;
        fwrite(&lRepasASauvegarder, sizeof(repasFichier),1, lFichier);
        for(int j = 0; j<aMenu.listeRepas[i]->nombreItem; j++) {
            fwrite(&aMenu.listeRepas[i]->listeItem[j], sizeof(item), 1, lFichier);
        }
    }
    fclose(lFichier);
}

/**
 * @brief lecture du menu à partir du disque
 * Si le menu contenait déjà des repas, les nouveaux sont ajoutés à la fin
 *
 * @param aMenu le menu dans lequel charger les repas.
 * @param aNomFichier le nom du fichier dans lequel lire
 */
void chargerMenu(menu *aMenu, char *aNomFichier) {
    FILE *lFichier = fopen(aNomFichier, "r");
    repasFichier lRepasACharger;
    repas *lRepasEnMemoire;
    item *lTableauItemsEnMemoire;
    int continuer = 0;
    do {
        continuer = fread(&lRepasACharger, sizeof(repasFichier),1, lFichier);
        if(continuer) {
            lRepasEnMemoire = calloc(1, sizeof(repas));
            strcpy(lRepasEnMemoire->titre, lRepasACharger.titre);
            lRepasEnMemoire->nombreItem = lRepasACharger.nombreItem;

            //le nombre d'item est connue. Le tableau peut donc être dimensionné au complet
            lTableauItemsEnMemoire = calloc(lRepasEnMemoire->nombreItem, sizeof(item));
            for(int i=0; i<lRepasACharger.nombreItem; i++) {
                fread(&lTableauItemsEnMemoire[i], sizeof(item),1, lFichier);
            }
            lRepasEnMemoire->listeItem = lTableauItemsEnMemoire;
            if(!aMenu->listeRepas) {
                aMenu->listeRepas = calloc(1, sizeof(repas*));
            } else {
                aMenu->listeRepas = realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
            }
            aMenu->listeRepas[aMenu->nombreRepas] = lRepasEnMemoire;
            aMenu->nombreRepas++;
        }
    } while(continuer);

    fclose(lFichier);
}

/**
 * @brief effacement complet d'un menu
 * @param aMenu le menu à effacer
 */
void viderMenu(menu *aMenu) {

    for(int i =0; i<aMenu->nombreRepas; i++) {
        repas *lRepas = aMenu->listeRepas[i];
        free(lRepas->listeItem);
        free(lRepas);
    }
    free(aMenu->listeRepas);
    aMenu->listeRepas = NULL;
    aMenu->nombreRepas = 0;
    //free(aMenu); pas besoin car sur la pile
}
