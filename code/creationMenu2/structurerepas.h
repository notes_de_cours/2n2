#ifndef STRUCTUREREPAS_H
#define STRUCTUREREPAS_H
#define LONGUEUR_TITRE 20

// un item dans un repas
typedef struct item {
    char titre[LONGUEUR_TITRE];
    float prix;
} item;

// un repas contenant une série d'item
typedef struct repas {
    char titre[LONGUEUR_TITRE];
    item *listeItem; //un tableau consécutif d'items
    int nombreItem;
} repas;

// un repas pour la sauvegarde sur disque car item *listeItem ne doit pas être sauvegardé
typedef struct repasFichier {
    char titre[LONGUEUR_TITRE];
    int nombreItem;
} repasFichier;

//un menu contenant une série de repas
typedef struct menu {
    repas **listeRepas; //un tableau de pointeur vers des repas.
    int nombreRepas;
} menu;

#endif // STRUCTUREREPAS_H
