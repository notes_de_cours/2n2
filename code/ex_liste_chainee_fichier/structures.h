#ifndef STRUCTURES_H
#define STRUCTURES_H
#define LONGUEUR_CHAMPS 30

typedef struct article {
    char description[LONGUEUR_CHAMPS];
    float prix;
} article;


typedef struct articleEnInventaire articleEnInventaire;
struct articleEnInventaire{
    article *ptrArticle;
    articleEnInventaire *suivant;
};

// le début de la liste chainée
typedef struct inventaire {
    articleEnInventaire *premier;
    int taille;
} inventaire;

#endif // STRUCTURES_H
