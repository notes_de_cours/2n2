#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "structures.h"


void sauvegarderArticles(inventaire *aInventaire)
{
    FILE * lFichier = fopen("inventaire.txt", "wb");
    articleEnInventaire *lArticleInventaire = aInventaire->premier;

    for(int i = 0; i<aInventaire->taille; i++) {
        article *lArticle =lArticleInventaire->ptrArticle;
        fwrite( lArticle, sizeof(article), 1, lFichier);
        lArticleInventaire = lArticleInventaire->suivant;
    }
    fclose(lFichier);

}

void chargerArticles(inventaire *aInventaire)
{
    FILE * lFichier = fopen("inventaire.txt", "rb");
    int continuer = 0;
    articleEnInventaire *lArticleInventairePrecedent = NULL;

    do {
        article *lArticle = calloc(1, sizeof(article));
        continuer = fread(lArticle, sizeof(article), 1, lFichier);
        if(continuer ) {
            articleEnInventaire *lArticleInventaire = calloc(1,sizeof(articleEnInventaire)); // sur le tas
            lArticleInventaire->suivant = NULL;
            lArticleInventaire->ptrArticle = lArticle;
            if(lArticleInventairePrecedent) {
                lArticleInventairePrecedent->suivant=lArticleInventaire;
                aInventaire->taille++;
            } else {
                aInventaire->premier = lArticleInventaire;
                aInventaire->taille = 1;
            }
            lArticleInventairePrecedent = lArticleInventaire;
        }

    } while(continuer);
    fclose(lFichier);
}
void afficherArticles(inventaire *aInventaire)
{
   articleEnInventaire *lArticleInventaire = aInventaire->premier;

   for(int i = 0; i<aInventaire->taille; i++ ) {
       article *lArticle =lArticleInventaire->ptrArticle;
       printf("%s  %5.2f \n", lArticle->description, lArticle->prix);
       lArticleInventaire = lArticleInventaire->suivant;
   }
}

void viderInventaireSurLaPile(inventaire *aInventaire)
{
    articleEnInventaire *lArticleInventaire = aInventaire->premier;
    articleEnInventaire *articleSuivant;
    for(int i = 0; i<aInventaire->taille; i++ ) {
        //les articles sont sur la pile, il ne faut donc pas les libérer
        free(lArticleInventaire->ptrArticle);

        articleSuivant = lArticleInventaire->suivant;
        free(lArticleInventaire);
        lArticleInventaire = articleSuivant;
    }
}

void viderInventaireSurLeTas(inventaire *aInventaire)
{
    articleEnInventaire *lArticleInventaire = aInventaire->premier;
    articleEnInventaire *articleSuivant;
    for(int i = 0; i<aInventaire->taille; i++ ) {
        free(lArticleInventaire->ptrArticle);
        articleSuivant = lArticleInventaire->suivant;
        free(lArticleInventaire);
        lArticleInventaire = articleSuivant;
    }
}


int main()
{
    article lesArticles[4] = { //sur la pile
        {"vis",  0.24},
        {"clou", 0.10},
        {"ecrou", 0.01},
        {"tournevis", 9.99}
    };

    inventaire lInventaire; // sur la pile
    srand(time(NULL));
    int r = rand();
    printf("%i\n", r);
    articleEnInventaire *lArticleInventairePrecedent = NULL;
    for(int i = 0; i<( r % 10) + 5; i++) {
        articleEnInventaire *lArticleInventaire = calloc(1,sizeof(articleEnInventaire)); // sur le tas
        lArticleInventaire->suivant = NULL;
        lArticleInventaire->ptrArticle = &lesArticles[rand()%4];
        if(lArticleInventairePrecedent) {
            lArticleInventairePrecedent->suivant=lArticleInventaire;
            lInventaire.taille++;
        } else {
            lInventaire.premier = lArticleInventaire;
            lInventaire.taille = 1;
        }
        lArticleInventairePrecedent = lArticleInventaire;

    }

    afficherArticles(&lInventaire);
    sauvegarderArticles(&lInventaire);
    viderInventaireSurLaPile(&lInventaire);
    printf("-----------\n");
    inventaire lInventaire2; // sur la pile
    chargerArticles(&lInventaire2);
    afficherArticles(&lInventaire2);
    viderInventaireSurLeTas(&lInventaire2);

}
