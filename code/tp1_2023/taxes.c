#include <stdio.h>

float mauvaiseTaxes = 0.0;


float calculerTaxes(float aMontant) {
    const float TAXE = 0.15;

    return aMontant*TAXE;
}

void mauvaisCalculTaxes(float aMontant) {
    mauvaiseTaxes = aMontant*1.15;
}

void mauvaiseImpressionTaxes(){
    printf("mauvaises taxes  %6.2f$\n", mauvaiseTaxes);
}
