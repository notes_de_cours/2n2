#include <stdio.h>
#include <stdlib.h>


int choisirMenuPrincipale() {
  int choix = -1;
  printf("Bienvenue au resto chez Benou\n");
  printf("1) simple\n");
  printf("2) medium\n");
  printf("3) avance\n");
  printf("4) payer\n");
  printf("0) sortir\n");
  scanf("%i", &choix);
  return choix;
}

float choisirPlatsSimple() {
  float total = 0;
  int choix = -1;
  do {
    printf("simple\n");
    printf("Veuillez choisir un de ces articles\n");
    printf("1) hot chicken     12.99$\n");
    printf("2) soupe            4.25$\n");
    printf("3) hot dog          0.99$\n");
    printf("0) retour au menu principal\n");
    scanf("%i", &choix);
    switch (choix) {
    case 1:
      total += 12.99;
      break;
    case 2:
      total += 4.25;
      break;
    case 3:
      total += 0.99;
      break;
    }
  } while (choix != 0);

  return total;
}

float choisirPlatsMedium() {
  const float PRIX_HOT_CHICKEN = 12.99;
  const float PRIX_SOUPE = 4.25;
  #define PRIX_HOT_DOG 0.99

  float lTotal = 0;
  int lChoix = -1;
  do {

    printf("medium\n");
    printf("Veuillez choisir un de ces articles\n");
    printf("1) hot chicken     %5.2f$\n", PRIX_HOT_CHICKEN);
    printf("2) soupe           %5.2f$\n", PRIX_SOUPE);
    printf("3) hot dog         %5.2f$\n", PRIX_HOT_DOG);
    printf("0) retour au menu principal\n");
    scanf("%i", &lChoix);
    switch (lChoix) {
    case 1:
      lTotal += PRIX_HOT_CHICKEN;
      break;
    case 2:
      lTotal += PRIX_SOUPE;
      break;
    case 3:
      lTotal += PRIX_HOT_DOG;
      break;
    }
  } while (lChoix != 0);

  return lTotal;
}

float choisirPlatsAvance() {
  char *lMenu[] = {"hot chicken", "soupe", "hot dog"};
  float lPrix[] = {12.99, 4.25, 0.99};
  const int NOMBRE_DE_PLAT = sizeof lPrix / sizeof(float);
  float lTotal = 0;
  int lChoix = -1;
  int mauvaisChoix=0;
  do {
    do {
      printf("avance\n");
      printf("Veuillez choisir un de ces articles\n");
      for (int i = 0; i < NOMBRE_DE_PLAT; i++) {
        printf("%i) %-20s %5.2f$\n", i + 1, lMenu[i], lPrix[i]);
      }
      printf("0) retour au menu principal\n");

      scanf("%i", &lChoix);
      mauvaisChoix = lChoix < 0 || lChoix > NOMBRE_DE_PLAT;
      if(mauvaisChoix) {
          printf("Entrez un nombre entre 0 et %i\n", NOMBRE_DE_PLAT);
      }
    } while (mauvaisChoix);

    if (lChoix != 0) {
      lTotal += lPrix[lChoix - 1];
    }

  } while (lChoix != 0);

  return lTotal;
}

void mauvaisMenuPrincipal(float aMontant);

void mauvaisChoisirPlatsSimple(float aMontant) {
    float total = aMontant;
    int choix = -1;
    do {
      printf("mauvais plats simple\n");
      printf("Veuillez choisir un de ces articles\n1) hot chicken     12.99$\n2) soupe            4.25$\n3) hot dog          0.99$\n");
      printf("0) retour au menu principal\n");
      scanf("%i", &choix);
      switch (choix) {
      case 1:
        total += 12.99;
        break;
      case 2:
        total += 4.25;
        break;
      case 3:
        total += 0.99;
        break;
      }
    } while (choix != 0);
    mauvaisMenuPrincipal(total);
}

void mauvaisMenuPrincipal(float aMontant) {
    int lChoixPrincipale;
    do {
        lChoixPrincipale = choisirMenuPrincipale();
        switch(lChoixPrincipale) {
        case 1:
            mauvaisChoisirPlatsSimple(aMontant);
            break;

        case 4:
            printf("Le total avant taxes est de %6.2f$\n", aMontant);
            aMontant = 0;
            break;
        }
    } while(lChoixPrincipale !=0);
}

