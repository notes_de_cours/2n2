#include <stdio.h>
#include <stdlib.h>
#include "menu.h"
#include "taxes.h"


int main()
{
    int lChoixPrincipale;
    float lMontantTotal = 0;
    float lTaxes = 0;


    //mauvaisMenuPrincipal(0.0);

    do {
        lChoixPrincipale = choisirMenuPrincipale();
        switch(lChoixPrincipale) {
        case 1:
            lMontantTotal += choisirPlatsSimple();
            break;
        case 2:
            lMontantTotal += choisirPlatsMedium();
            break;
        case 3:
            lMontantTotal += choisirPlatsAvance();
            break;
        case 4:
            printf("Le total avant taxes est de %6.2f$\n", lMontantTotal);
            lTaxes = calculerTaxes(lMontantTotal);
            printf("Les taxes sont de           %6.2f$\n",lTaxes );
            printf("Le total avec taxes est de  %6.2f$\n ", lMontantTotal+lTaxes);

            //mauvaisCalculTaxes(lMontantTotal);
            //mauvaiseImpressionTaxes();

            lMontantTotal = 0;
            break;
        }
    } while(lChoixPrincipale !=0);

    return 0;
}
