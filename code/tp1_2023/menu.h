#ifndef MENU_H
#define MENU_H

int choisirMenuPrincipale();

float choisirPlatsSimple();
float choisirPlatsMedium();
float choisirPlatsAvance();

void mauvaisMenuPrincipal(float aMontant);
void mauvaisChoisirPlatsSimple(float aMontant);


#endif // MENU_H
