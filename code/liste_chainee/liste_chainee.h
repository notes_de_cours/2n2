#ifndef LISTE_CHAINEE_H
#define LISTE_CHAINEE_H

typedef struct structElement element;
struct structElement
{
    int nombre;
    element *suivant;
};

typedef struct structListe
{
    element *premier;
    int taille;
} liste;



liste *initialiserListe();
element *initialiserElement(int aNombre);
void insererElementAuDebut(liste *aListe, int aNombre);
void insererElementALaFin(liste *aListe, int aNombre);
void supprimerPremier(liste *aListe);
void afficherListe(liste *aListe);
void afficherElement(element *aElement);
void viderListe(liste *aListe) ;
void insererElementParPosition(liste *aListe, int aPosition, int aNombre);
void supprimerParPosition(liste *aListe, int aPosition);
element *elementParPosition(liste *aListe, int aPosition);


#endif // LISTE_CHAINEE_H
