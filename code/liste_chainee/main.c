#include <stdio.h>
#include <stdlib.h>
#include "liste_chainee.h"

int main()
{
    liste *maListe = initialiserListe();

    insererElementAuDebut(maListe, 4);
    insererElementAuDebut(maListe, 8);
    insererElementAuDebut(maListe, 15);
    insererElementParPosition(maListe, 2, 5);
    afficherListe(maListe);

    supprimerPremier(maListe);
    afficherListe(maListe);

    supprimerParPosition(maListe, 2);
    afficherListe(maListe);
    supprimerParPosition(maListe, 2);
    afficherListe(maListe);
    supprimerParPosition(maListe, 2); //test pour enlever un élément inexistant.
    afficherListe(maListe);

    element *lElement = elementParPosition(maListe, 1);
    afficherElement(lElement);
    lElement = elementParPosition(maListe, 5); //test pour afficher un element inexistant
    afficherElement(lElement);

    supprimerParPosition(maListe,1); // test pour enlever le seul element
    afficherListe(maListe);

    insererElementAuDebut(maListe,3);
    insererElementParPosition(maListe,1,20);
    afficherListe(maListe);
    supprimerParPosition(maListe,1); // test pour enlever le premier
    afficherListe(maListe);

    insererElementALaFin(maListe, 19);
    insererElementALaFin(maListe, 18);
    afficherListe(maListe);

    viderListe(maListe);
    afficherListe(maListe);
    viderListe(maListe); // test pour vider une liste vide
    afficherListe(maListe);

    insererElementALaFin(maListe, 17); // test de l'ajout a la fin d'une liste vide
    insererElementALaFin(maListe, 16);
    afficherListe(maListe);

    viderListe(maListe);

    free(maListe);

    return 0;
}
