#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#define LONGUEUR_TITRE 20


//*****structure utilisées pour les menus*****
// un item dans un repas
typedef struct item {
    char titre[LONGUEUR_TITRE];
    float prix;
} item;

// un repas contenant une série d'item
typedef struct repas {
    char titre[LONGUEUR_TITRE];
    item *listeItem; //un tableau consécutif d'items
    int nombreItem;
} repas;

// un repas pour la sauvegarde sur disque car item *listeItem ne doit pas être sauvegardé
typedef struct repasFichier {
    char titre[LONGUEUR_TITRE];
    int nombreItem;
} repasFichier;

//un menu contenant une série de repas
typedef struct menu {
    repas **listeRepas; //un tableau de pointeur vers des repas.
    int nombreRepas;
} menu;

//*****structure utilisées pour les commandes*****
// les éléments de la liste chainée composant une commande
typedef struct structItemCommande itemCommande;
struct structItemCommande{
    item *ptrItemCommande;
    itemCommande *suivant;
};

// le début de la liste chainée
typedef struct strucCommande {
    itemCommande *premier;
    int taille;
} commande;


//***** fonctions de gestion du menu
/**
 * @brief lecture du menu à partir du disque
 * Si le menu contenait déjà des repas, les nouveaux sont ajoutés à la fin
 *
 * @param aMenu le menu dans lequel charger les repas.
 * @param aNomFichier le nom du fichier dans lequel lire
 */
void chargerMenu(menu *aMenu, char *aNomFichier) {
    FILE *lFichier = fopen(aNomFichier, "r");
    repasFichier lRepasACharger;
    repas *lRepasEnMemoire;
    item *lTableauItemsEnMemoire;
    int continuer = 0;
    do {
        continuer = fread(&lRepasACharger, sizeof(repasFichier),1, lFichier);
        if(continuer) {
            lRepasEnMemoire = calloc(1, sizeof(repas));
            strcpy(lRepasEnMemoire->titre, lRepasACharger.titre);
            lRepasEnMemoire->nombreItem = lRepasACharger.nombreItem;

            //le nombre d'item est connue. Le tableau peut donc être dimensionné au complet
            lTableauItemsEnMemoire = calloc(lRepasEnMemoire->nombreItem, sizeof(item));
            for(int i=0; i<lRepasACharger.nombreItem; i++) {
                fread(&lTableauItemsEnMemoire[i], sizeof(item),1, lFichier);
            }
            lRepasEnMemoire->listeItem = lTableauItemsEnMemoire;
            if(!aMenu->listeRepas) {
                aMenu->listeRepas = calloc(1, sizeof(repas*));
            } else {
                aMenu->listeRepas = realloc(aMenu->listeRepas, (aMenu->nombreRepas+1) *sizeof(repas*));
            }
            aMenu->listeRepas[aMenu->nombreRepas] = lRepasEnMemoire;
            aMenu->nombreRepas++;
        }
    } while(continuer);

    fclose(lFichier);
}


//***** fonctions de gestion des commandes

commande *initialiserCommande()
{
    commande *lListe = calloc(1, sizeof(commande));
    if (lListe == NULL ) // vérification de l'allocation de l'espace mémoire
    {
        exit(EXIT_FAILURE);
    }
    lListe->premier = NULL;
    lListe->taille = 0;
    return lListe;
}

/**
 * @brief allocation de mémoire pour un élément de la liste et insertion de la valeur dans celui-ci
 * @param aNombre la valeur à mettre dans l'élément
 * @return le pointeur vers l'élément
 */
itemCommande *initialiserItemCommande(item *aItem) {
    /* Création du nouvel élément */
    itemCommande *nouveau = calloc(1, sizeof(itemCommande));
    if ( nouveau == NULL)
    {
        exit(EXIT_FAILURE);
    }
    nouveau->ptrItemCommande = aItem;
    nouveau->suivant = NULL;
    return nouveau;
}

/**
 * @brief insertion d'un élément à la fin de la liste.
 * @param aListe la liste dans laquelle insérer l'élément.
 * @param aNombre la valeur à mettre dans l'élément qui sera inséré
 */
void ajouterItemACommande(commande *aListe, item *aItem)
{
    itemCommande *recherche = NULL;
    itemCommande *suivant;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    /* Recherche de la fin de la liste */
    suivant = aListe->premier;
    while(suivant) {
        recherche = suivant;
        suivant = suivant->suivant;
    }
    itemCommande *nouveau = initialiserItemCommande(aItem);
    if(!recherche) { //la liste est vide, il faut ajouter au début de la liste
        aListe->premier=nouveau;
    } else {
        recherche->suivant = nouveau;
    }
    aListe->taille++;
}

/**
 * @brief affichage de la liste des repas dans un menu
 * @param aMenu le menu à afficher
 */
void afficherRepas(menu aMenu) {
    for(int i =0 ; i<aMenu.nombreRepas; i++) {
       printf("%i) %s\n", i+1, aMenu.listeRepas[i]->titre);
    }
}

/**
 * @brief sélection d'un repas
 * @param aMenu le menu dans lequel choisir le repas
 * @return le repas choisi
 */
repas *choisirRepas(menu aMenu){
    int choix;
    char choixTxt[10];
    do{
        afficherRepas(aMenu);
        printf("Quel est votre choix: ");
        fgets(choixTxt, sizeof choixTxt, stdin);
        choixTxt[strcspn(choixTxt, "\n")] = 0;
        choix = atoi(choixTxt);

    } while(choix < 1 || choix > aMenu.nombreRepas);

    return aMenu.listeRepas[choix-1];
}

void afficherItems(repas *aRepas) {
    item lItem;
    for (int i =0 ; i<aRepas->nombreItem; i++) {
        lItem = aRepas->listeItem[i];
        printf("%i) %-20s  %6.2f\n", i+1, lItem.titre, lItem.prix );
    }
}


/**
 * @brief passer une commande en choisissant des items d'un repas
 * @param aMenu le menu contenant les repas
 * @param aCommande la commande au complet.
 */
void commander(menu aMenu, commande *aCommande) {
    repas *repasChoisi = choisirRepas(aMenu);
    int choix;
    char choixTxt[10];
    int continuer;
    do{
        continuer = 0;
        afficherItems(repasChoisi);
        printf("0) retour au menu\n");
        printf("Quel est votre choix: ");
        fgets(choixTxt, sizeof choixTxt, stdin);
        choix = atoi(choixTxt);
        if (choix >0 && choix<= repasChoisi->nombreItem) {
            continuer = 1;
            ajouterItemACommande( aCommande, &repasChoisi->listeItem[choix-1]);
        }
    } while(continuer);

}

/**
 * @brief affiche une commande au complet
 * @param aCommande la commande à afficher
 */
void afficherCommande(commande *aCommande) {
    itemCommande *suivant;
    float total=0;
    suivant = aCommande->premier;
    if(!suivant) {
        printf("La commande est vide\n ");
    } else {
        while(suivant){
            printf("%-20s %6.2f\n", suivant->ptrItemCommande->titre, suivant->ptrItemCommande->prix);
            total +=suivant->ptrItemCommande->prix;
            suivant = suivant->suivant;
        }
        printf("-------------------------------\n");
        printf("%-20s %6.2f\n","total:", total );
        printf("%-20s %6.2f\n","taxe:", total*0.15 );
        printf("%-20s %6.2f\n","grand total:", total+total*0.15 );


    }
}

/**
 * @brief enregistrement de la commande dans un fichier et efface les éléments de la commande
 * @param aCommande la commande a sauvegarder
 */
void sauvegarderCommande(commande *aCommande, char *aNomFichier){
    FILE *lFichier = fopen(aNomFichier, "a");
    itemCommande *suivant = aCommande->premier;
    itemCommande *aEffacer;
    while(suivant) {
        fwrite(suivant->ptrItemCommande, sizeof(item), 1, lFichier);
        aEffacer = suivant;
        suivant = suivant->suivant;
        free(aEffacer); //efface les itemCommande au fur et à mesure.
            //Les items ne doivent pas être effacés puisqu'ils font parties du menu.
    }
    fclose(lFichier);
    aCommande->premier = NULL;
    aCommande->taille = 0;
}
int main(int argc, char *argv[])
{
    menu lMenu;
    char *lFichierMenu = "menu.txt";
    char *lFichierCommande = "commande.txt";
    char entree[10];
    int choix;



    lMenu.listeRepas = NULL;
    lMenu.nombreRepas = 0;
    chargerMenu(&lMenu, lFichierMenu);

    commande *lCommande = initialiserCommande();

    do {
        printf("1) Commander un item\n");
        printf("2) afficher la commande\n");
        printf("3) Payer\n");
        printf("0) sortir\n");
        printf("votre choix: ");
        fgets(entree, sizeof entree, stdin);
        choix = atoi(entree);
        if(choix == 1) {
            commander(lMenu, lCommande);

        } else if(choix == 2) {
            printf("-----------\n\n");
            afficherCommande(lCommande);
            printf("-----------\n\n");
        } else if(choix ==3) {
            printf("Cette commande est maintenant payée\n");
            afficherCommande(lCommande);
            printf("-----------\n\n");
            sauvegarderCommande(lCommande, lFichierCommande);

        }
    } while(choix != 0);

    //viderMenu(&lMenu);


    return 0;
}
