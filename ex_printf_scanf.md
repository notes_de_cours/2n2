# Exercices prinf et scanf <!-- omit in toc -->

Veuillez créer un nouveau projet dans QT. 

[comment]: #  (note a l'enseignant, les exercices se trouvent sur https://gitlab.com/notes_de_cours/2n2_exercices.git)


Remplacez le code dans `main.c` par :

```c
#include <stdio.h>

void serie1_ex1() {
    /**
     * Comme dans tout bon cours d'introduction, il est d'usage de faire un "Hello world!"
     * afin de vous présenter le langage.
     *
     * Afin de nous assurer que votre environnement de programmation veuillez essayer d'exécuter cette fonction
     * en indiquant que vous désirez exécuter l'exercice #1
    */
    printf("Hello World!\n");
}

void serie1_ex2()
{
    /*
     * complétez cette fonction afin d'afficher "Bonjour" suivit d'un retour de chariot.
     */
     /* enlever cette ligne de commentaire
            ("Bonjour")
        enlever cette ligne de commentaire */
}

void serie1_ex3() {
    /*
     * modifiez ce code afin d'afficher "Bonjour" suivit d'un retour de chariot
     */
    /* enlever cette ligne de commentaire
            system.out.printLn("Bonjour");
      enlever cette ligne de commentaire */
}


void serie1_ex4()
{
    /*
     * modifiez ce code afin d'afficher la question "Entrez un nombre:"
     * ensuite demandez à l'usager d'entrer un entier
     * affichez ensuite le nombre entré.
     */
    /* insérez votre code ici */
}

void serie1_ex5()
{
    /*
     * Corrigez ce code afin qu'il affiche le nombre qui sera fourni en entré.
     */
    int valeur;
    /*enlever cette ligne de commentaire
    scan(valeur);
    enlever cette ligne de commentaire */
    scanf("%i", &valeur);
    printf("%i\n",valeur);
}

void serie1_ex6()
{
    /*
     * Corrigez ce code afin qu'il demander à l'usager d'entrer une chaine de caractères.
     */
    char  v[20];
    printf("Entrez un texte :");

    /* insérez votre code ici */

    printf("La chaine est %s\n",v);
}


void serie1_ex7()
{
    /*
     * Complétez le code suivant afin de déclarer la
     * variable "varA" comme étant du type int
     * et ayant la valeur 1234
     * et afficher cette variable
     */

    /* enlever cette ligne de commentaire
    ??? varA ????
    println(varA);
    enlever cette ligne de commentaire */


}

int main()
{
    //printf et scanf
    //serie1_ex1();
    //serie1_ex2();
    //serie1_ex3();
    //serie1_ex4();
    //serie1_ex5();
    //serie1_ex6();
    //serie1_ex7();

    // boucle et if
    //serie2_ex1();
    //serie2_ex2();
    //serie2_ex3();
    //serie2_ex4();
    //serie2_ex1();

    // boucle array et string
    //serie3_ex1();
    //serie3_ex2();

    return 0;
}

```

Vous devez compléter le code des fonctions ayant un nom commencant par `serie1_`

!> Comme vous pouvez le voir dans `main.c`, il y aura d'autres séries d'exercices plus tard. Veuillez donc concerver ce projet. 

Ensuite enlevez le commentaire dans `main.c` pour la fonction que vous désirez essayer. 

Vous trouverez les réponses des exercices ci-bas. 

Ne regardez les réponses qu'après avoir trouvé la solution, ou avoir cherché longtemps. 

<details>
<summary> Serie1_ex1 solution  </summary>

```c
void serie1_ex1() {
    printf("Hello World!\n");
}
```
</details>
<details>
<summary> Serie1_ex2 solution  </summary>

```c
void serie1_ex2()
{
    /*
     * complétez cette fonction afin d'afficher "Bonjour" suivit d'un retour de chariot.
     */
            printf("Bonjour\n");
}
```    
</details>
<details>
<summary> Serie1_ex3 solution  </summary>

```c
void serie1_ex3()
{
    /*
     * modifiez ce code afin d'afficher "Bonjour" suivit d'un retour de chariot.
     */
            printf("Bonjour\n");
}
``` 
!>Oui, c'est la même réponse que le numéro précédent. Ne mélangez pas le code Java avec le code C. La syntaxe est différente. 


</details>
<details>
<summary> Serie1_ex4 solution  </summary>

```c
void serie1_ex4()
{
    /*
     * modifiez ce code afin d'afficher la question "Entrez un nombre:"
     * ensuite demandez à l'usager d'entrer un entier
     * affichez ensuite le nombre entré.
     */
    /* insérez votre code ici */
    int valeur;
    printf("Entrez un nombre:");
    scanf("%i", &valeur);
    printf("Le nombre entré est %i\n", valeur);
}
```
</details>
<details>
<summary> Serie1_ex5 solution  </summary>

```c
void serie1_ex5()
{
    /*
     * Corrigez ce code afin qu'il affiche le nombre qui sera fourni en entré.
     */
    int valeur;
    scanf("%i", &valeur);
    printf("%i\n",valeur);
}
```
</details>
<details>
<summary> Serie1_ex6 solution  </summary>

```c
void serie1_ex6()
{
   /*
     * Corrigez ce code afin qu'il demander à l'usager d'entrer une chaine de caractères.
     */
    char  v[10];
    printf("Entrez un texte :");
    scanf("%s",v);
    printf("La chaine est %s\n",v);
}
```
</details>
<details>
<summary> Serie1_ex7 solution  </summary>

```c
void serie1_ex7()
{
   /*
     * Complétez le code suivant afin de déclarer la
     * variable "varA" comme étant du type int
     * et ayant la valeur 1234
     * et afficher cette variable
     */

    int varA = 1234;
    printf("%i\n", varA);
}
```
!> oui, le println était voulu :smile:

</details>



------