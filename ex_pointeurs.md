# Exercices sur les pointeurs <!-- omit in toc -->


Pour ces exercices, télécharger `demo_allocation_en_c.zip` sur Léa .  

[comment]:# ( note pour l'enseignant: le fichier zip se trouve dans _media. Le git des exercices est sur https://gitlab.com/notes_de_cours/2n2_demo_allocation_en_c.git Les réponses se trouvent dans DessinTableauDeStruct.pdf qui est dans _media)

Décompressez le fichier zip.

Ouvrez le projet dans QT (ouvrez le fichier `.pro`). 

Vous trouverez dans `main.c` plusieurs fonctions (demo1, demo2...)

Au bas de `main.c`, dans la fonction `main()` vous pouvez enlevez le commentaire de la fonction que vous désirez essayer. 

Nous allons examiner les fonctions une par une. Le but de l'exercice est de mieux comprendre les différentes formes que peut prendre l'allocation dynamique. Vous n'avez pas a exécuter le code pour le comprendre. Tout ce qu'il fait c'est de demander un chaine de caractère et un nombre à l'usager (à répétition pour les démos de tableau). 

Veuillez former des équipes de 3-4. Chaque équipe devra produire un dessin représentant la structure de l'allocation dynamique de chacun des démos. Vous aurez quelques minutes pour en discuter, ensuite chaque équipe devra présenter sa représentation. 

La *bonne* réponse vous sera ensuite présentée avec des explications. 



**Notes**

Ces exercices utilisent la structure `demoStruct` que vous pouvez trouver dans `demoStruct.h`.

Celle-ci consiste en un `int`, suivi d'un pointeur vers une `string`. Étant donné que cette `string` n'a pas une longueur fixe, elle sera allouée dynamiquement sur le tas. 

Vous trouverez les fonctionns de manipulations de demoStruct dans `demoStruct.c`.

Vous remarquerez qu'il y a beaucoup de commentaires dans ce code, incluant des indications qui pourront vous aider à savoir si un élément est sur la pile ou sur le tas. Mais efforcez-vous de comprendre l'allocation en comprenant le type de variable et son utilisation (eh non! le code ne sera pas toujours si bien documenté :smile:)

Notez aussi qu'au lieu de faire un `scanf`, c'est `fget()` qui a été utilisé dans `remplirDemoStruct`. Ne vous attardez pas à ce détail. Ce que vous devez savoir c'est que remplirDemoStruct demande une chaine de caractères et un entier et les mets dans la structure. 


Notez aussi que pour le demo6, c'est une structure légèrement différente qui est utilisée. Elle s'appel `demoSTruct2`. Vous trouverez sa définition dans `demoStruct.h`. 




