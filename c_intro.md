# Introduction au C <!-- omit in toc -->

!> Avant propos : Tel qu'indiqué dans l'introduction, nous présumons que vous avez déjà une base de programmation et que vous maitrisez les concepts de base. 



# 1. Hello World

Commençons par le classique Hello World en C 

Démarrez QT Creator et créez un nouveau projet en appuyant sur **Create Project** et choisissez **Non-QT Project / Plain C Application**

![alt](_media/create_qt_project_not_qt_project.png)

Donnez un nom au projet, et sauvegardez le dans un répertoire dans lequel vous mettrez vos projets QT (*créez ce répertoire et souvenez vous en*). 

Sélectionnez **qmake**

Faites suivant sur l'écran **Kit Selection** et **Project Management**

Et **Terminer**

Vous devriez obtenir ce code:
 
```c
#include <stdio.h>

int main()
{
    printf("Hello World!\n");
    return 0;
}
```

!> Attention: avant d'exécuter votre code vous devez changer un paramètre d'exécution afin d'afficher votre programme dans un terminal. Sélectionnez l'onglet **Projets**

![alt](_media/run_in_terminal.png)


Vous pouvez maintenant exécuter votre Hello World. 

Maintenant que nous savons que l'environnement fonctionne, prenons un peu de temps pour faire connaissance avec le langage C

# 2. Le langage C


Le C est un langage bas niveau. C’est donc a priori plus difficile. Mais grâce à ces bases solides, vous serez ensuite capable d'apprendre beaucoup plus rapidement un autre langage de programmation si vous le désirez. Et avec plus d’autonomie !

Par ailleurs, le C est un langage très populaire. Il est utilisé pour programmer une grande partie des logiciels que vous connaissez. C'est un des langages les plus connus et les plus utilisés qui existent. Il est très fréquent qu'il soit enseigné lors d'études supérieures en informatique. Le langage C est à la base des plus grands systèmes d'exploitation tels UNIX (et donc Linux et Mac OS) ou Windows.

Voici le strict minimum pour un programmeur :

- un éditeur de texte pour écrire le code source du programme. En théorie un logiciel comme le bloc-notes sous Windows, ou "vi" sous Linux, fait l'affaire. L'idéal, c'est d'avoir un éditeur de texte intelligent qui colore tout seul le code, ce qui vous permet de vous y repérer bien plus facilement ;
- un compilateur pour transformer (compiler) votre source en binaire ;
- un débogueur pour vous aider à traquer les erreurs dans votre programme.

Soit on récupère chacun de ces trois programmes séparément. C'est la méthode la plus compliquée, mais elle fonctionne.
Soit on utilise un IDE (environnement de développement). C'est un programme 3-en-1 qui combine éditeur de texte, compilateur et débogueur. 

Il faut savoir qu'en fait il existe deux types de programmes :
1.	Les programmes graphiques.
2.	Les programmes en console.

Les programmes graphiques sont des programmes qui affichent des fenêtres que l'on peut ouvrir, réduire, fermer, agrandir… Ce sont ceux que vous connaissez sûrement le mieux : Word ou encore Paint sont des programmes graphiques, par exemple.

Les programmeurs parlent de GUI (Graphical User Interface – interface utilisateur graphique, en français). 

Les programmes en console ont été les premiers à apparaître. À l'époque, l'ordinateur ne gérait que le noir et blanc, et il n'était pas assez puissant pour créer des fenêtres comme on le fait aujourd'hui.


## 2.1. Historique

C est un langage de programmation impératif généraliste, de bas niveau. Inventé au début des années 1970 pour réécrire Unix, C est devenu un des langages les plus utilisés, encore de nos jours. De nombreux langages plus modernes comme C++, C#, Java et PHP ou JavaScript ont repris une syntaxe similaire au C et reprennent en partie sa logique. C offre au développeur une marge de contrôle importante sur la machine (notamment sur la gestion de la mémoire) et est de ce fait utilisé pour réaliser les « fondations » (compilateurs, interpréteurs…) de ces langages plus modernes. 

Le langage C a été inventé au cours de l'année 1972 dans les Laboratoires Bell. Il était développé en même temps qu'Unix par Dennis Ritchie et Kenneth Thompson. Kenneth Thompson avait développé le prédécesseur direct de C, le langage B, qui est lui-même largement inspiré de BCPL. Dennis Ritchie a fait évoluer le langage B dans une nouvelle version suffisamment différente, en ajoutant notamment les types, pour qu'elle soit appelée C.
[wikipedia](https://fr.wikipedia.org/wiki/C_(langage))

## 2.2. Structure de base

![alt](_media/c_main.png)


## commentaire

<script defer src="https://commento420.cegepdrummond.ca/js/commento.js"></script>
<div id="commento"></div>