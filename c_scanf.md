# scanf <!-- omit in toc -->

# Intro
Cette fonction sert à lire la console (le clavier) (stdin)

C'est un peu l'équivalent du Scanner de Java. 

La syntaxe est très similaire à celle de printf:

```c
 int scanf (const char* format, ...);
```

Voyons un exemple:

```c
int v;
printf("Entrez un nombre entier :");
scanf("%i", &v);
printf("Le nombre est %i\n",v);

```

Dans cet exemple, nous pouvons noter les points suivants:

- un printf est nécessaire afin d'afficher le texte de la question. Vous ne devez rien mettre d'autre que des spécificateurs de format dans un scanf;
- il n'y a pas de `\n` dans le premier printf. De cette façon, le curseur restera au bout de la ligne en attendant la réponse;
- la variable v est déclarée en tant qu'`int`. Mais dans le scanf, on doit la précéder du caractère `&`. Nous verrons plus tard ce que cela signifie (*oui, c'est encore en rapport avec les pointeurs*);
- le deuxième printf se termine par un `\n`.

Ici aussi, la syntaxe du scanf comporte d'autres détails que nous n'étudierons pas. Je vous recommande [ce lien](https://cplusplus.com/reference/cstdio/scanf/) pour en savoir plus. 

Comme indiqué dans le prinf, si vous désirez faire l'entrée d'un float ou d'un double, il faut utiliser différents spécificateurs de format. 

- Pour un float: `%f`
- Pour un double: `%lf`



Si tout se passe bien, scanf retourne le nombre d'entrées lues. 

# scanf et les chaines de caractères

Nous verrons la gestion des chaines de caractères (string) un peu plus tard. Mais je me permet de faire une petite parenthèse afin d'expliquer comment utiliser scanf avec une chaine de caractères sans trop rentrer dans les technicalités. 

Voyons un exemple:

```c
char  v[20];
printf("Entrez un texte :");
scanf("%s",v);
printf("La chaine est %s\n",v);

```

À remarquer:

- la chaine de caractère ne pourra être de plus de 20 caractères. Si vous essayez ce code en mode debug, vous verrez une *segmentation fault* apparaitre si vous entrez plus de 20 caractères;
- dans le scanf, v n'est **pas** précédé de `&` (nous verrons pourquoi quand nous parlerons des pointeurs)

# Problèmes du scanf

Ré-essayez le dernier bout de code, mais cette fois-ci, entrez 2 mots séparés par un espace. Quel est le résultat affiché?

Scanf lit jusqu'au premier espace et s'arrête là. 

Pour la lecture d'une chaine de caractères, je vous recommande [gets](https://devdocs.io/c/io/gets). Mais cette fonction ne fonctionne que pour les chaines de caractères.

Par contre, il ne faut pas mélanger gets et scanf car vous obtiendrez un comportement bizarre. 

```c
#include <stdio.h>

int main()
{
    char  v1[20];
    char v2[20];
    char v3[20];
    printf("Entrez un texte :");
    scanf("%s",v1);
    printf("Entrez un autre texte :");
    gets(v2);
    printf("Entrez un troisième texte :");
    scanf("%s", v3);
    printf("Les chaines sont\n%s\n%s\n%s\n",v1, v2, v3);

}
```

Comme vous pouvez le voir, le gets est sauté. 

Cela est dû au fait que le `\n` n'est pas lu par le `scanf`. Il est donc encore dans le buffer et est lu par le `gets`. 

Oui, c'est mélangeant ... mais les exercices ont été pensés en conséquence et vous n'aurez pas à gérer ca avant d'y avoir été préparé.e. 


