# Exercices de manipulation de fichier <!-- omit in toc -->

!>**ATTENTION** Étant donné que nous utilisons QT comme éditeur, il est important de changer un paramètres d'exécution afin d'avoir accès aux fichiers manipulés.\
Allez dans le menu Projets (la clé à molettes), ensuite dans Run, et changez le chemin du Working directory pour qu'il soit le même que celui de votre code c (là où se trouve main.c)\
Habituellement, les fichiers manipulés par le code ne devraient pas être dans le même répertoire que le code, mais pour des fins pédagogiques c'est ce que nous ferons. Il sera plus facile de remettre les TPs. 



### 1 Écriture et lecture d'une chaine de caractère dans un fichier texte à l'aide de `fputs` et `fgets`. 


Votre code doit:
* écrire le mot "allo toi" dans un fichier text en utilisant `fputs`;
* fermer le fichier;
* le réouvrir et relire le texte avec `fgets`;
* afficher le texte à l'écran. 

<details>
<summary>Solution</summary>

```c
#include <stdio.h>

int main(int argc, char *argv[])
{

    char ligne[255];
    FILE *lFichier = fopen("exemple.txt", "w+");

    fputs("allo toi", lFichier);
    fclose(lFichier);

    lFichier = fopen("exemple.txt", "r");
    fgets(ligne, 255, lFichier);
    printf("%s\n", ligne);
    fclose(lFichier);

    return 0;
}
```

Notez que "allo toi" est lu dans une seule chaine. 
</details>

### 2 Écriture et lecture de nombres dans un fichier texte à l'aide de `fprintf` et `fscanf`.

Votre code doit;
* écrire les nombre (float) 3.1415, (float) 1.234, (entier) 456 dans un fichier text en utilisant `fprintf`,
* fermer le fichier, 
* le réouvrir,
* relire l'info avec `fscanf`,
* afficher l'info à l'écran. 


<details>
<summary>Solution</summary>

```c
#include <stdio.h>

int main(int argc, char *argv[])
{

    float nombre, nombre2;
    int nombre3;
    FILE *lFichier = fopen("exemple.txt", "w+");

    fprintf(lFichier, "%f %f %i", 3.1415, 1.234, 456);
    fclose(lFichier);

    lFichier = fopen("exemple.txt", "r");
    fscanf(lFichier, "%f%f%i", &nombre, &nombre2, &nombre3);
    printf("%f  %f %i\n", nombre, nombre2, nombre3);
    fclose(lFichier);

    return 0;
}
```
</details>

### 3 Écriture et lecture de chaines de caractère ET de nombres à l'aide de `fprintf` et `fscanf`

Votre code doit:
* écrire les nombre (float) 3.1415, (float) 1.234, et le texte "allo toi" dans un fichier text en utilisant `fprintf`, 
* fermer le fichier, 
* le réouvrir et relire l'info avec `fscanf`, 
* et l'afficher à l'écran. 

!> Attention ... est-ce que lors de la lecture vous avez obtenu "allo toi", ou simplement "allo" ?  

<details>
<summary>Solution</summary>

```c
#include <stdio.h>

int main(int argc, char *argv[])
{

    float nombre, nombre2;
    char chaine[10], chaine2[10];
    FILE *lFichier = fopen("exemple.txt", "w+");

    fprintf(lFichier, "%f %f %s", 3.1415, 1.234, "allo toi");
    fclose(lFichier);

    lFichier = fopen("exemple.txt", "r");
    fscanf(lFichier, "%f%f%s%s", &nombre, &nombre2, &chaine, &chaine2);
    printf("%f  %f %s %s\n", nombre, nombre2, chaine, chaine2);
    fclose(lFichier);

    return 0;
}
```

Notez que "allo toi" est séparé en deux chaines lors de la lecture. 

Il peut donc être difficile de lire du texte avec fscanf. 

</details>


### 4 Écriture et lecture de chaines de caractère ET de nombres à l'aide de `fprintf` et `fgets`, et utilisation de `strtok` et `atoi` et `atof`.

Votre code doit:
* écrire les nombre (float) 3.1415, (float) 1.234, le texte "allo toi", suivit de (entier) 456 dans un fichier text en utilisant `fprintf`,
* fermer le fichier, 
* le réouvrir et relire l'info avec `fgets`,
* l'afficher à l'écran.  

Lors de l'écriture, séparez les champs par une virgule `,`

Lors de la lecture, étant donné que c'est fait avec fgets, vous obtiendrez une seule ligne. Vous pouvez la séparer en utiliant les fonctions suivantes:

[strtok](https://devdocs.io/c/string/byte/strtok)
[atoi](https://devdocs.io/c/string/byte/atoi)
[atof](https://devdocs.io/c/string/byte/atof)

<details>
<summary>Solution</summary>

```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{

    float nombre1, nombre2;
    int nombre3;
    char grandeChaine[255];


    char *chaine;
    char chaine2[20] = "allo toi";
    FILE *lFichier = fopen("exemple.txt", "w+");

    fprintf(lFichier, "%f,%f,%s,%i", 3.1415, 1.234, chaine2,456);
    fclose(lFichier);

    lFichier = fopen("exemple.txt", "r");
    fgets(grandeChaine, 255, lFichier); //lecture de la ligne au complet
    printf("1 %s\n", grandeChaine);

    chaine = strtok(grandeChaine, ","); //séparation du premier chiffre
    printf("2 %s\n", chaine);
    nombre1= atof(chaine) + 1;  //conversion en float
    printf("2.5 %f\n", nombre1);

    chaine = strtok(NULL, ","); //deuxième chiffre
    printf("3 %s\n", chaine);
    nombre2= atof(chaine) + 2;
    printf("3.5 %f\n", nombre2);


    chaine = strtok(NULL, ","); //la chaine de caractères
    printf("4 %s\n", chaine);

    chaine = strtok(NULL, ","); //dernier chiffre
    printf("5 %s\n", chaine);
    nombre3= atoi(chaine) + 3; //conversion en int
    printf("5.5 %i\n", nombre3);

    fclose(lFichier);

    return 0;
}

```
Vous pouvez vérifier que bien que ce soit chaine2 ayant une longueur de 20 qui est écrit dans le fichier, la longueur réservée n'est que de 8 caractères.

Lors de la conversion en float et int, il y a une addition uniquement pour démontrer que c'est bien un valeur numérique. 

Si on respect bien la structure, il est possible de lire du texte. Il faut avoir un séparateur qui ne se retrouvera pas dans le texte.\
Ici l'utilisation de la virgule n'est peut-être pas le meilleur des choix. 

</details>


### 5 Écriture et lecture d'une structure avec `fwrite` et `fread`.

Votre code doit 

* écrire la structure `personne` dans un fichier texte en utilisant `fwrite`;
* fermer le fichier;
* le réouvrir et relire l'info avec `fread`;
* afficher l'info à l'écran. 

Vous devez utiliser cette structure et y mettre de l'information avant de la sauvegarder.



```c
typedef struct personne {
    char prenom[30];
    char nom[30];
    int age;
} personne;
```

Une fois réussi, allez voir le fichier que vous venez de créer (ouvrir avec notepad++). 

Que remarquez-vous ?


<details>
<summary>Solution</summary>

```c
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

typedef struct personne {
    char prenom[30];
    char nom[30];
    int age;
} personne;


int main(int argc, char *argv[])
{

    personne p1 = {"John", "Smith", 21};
    personne p2 = {"Albert", "Einstein", 75};
    personne px;
    FILE *lFichier = fopen("exemple.txt", "w+");

    fwrite(&p1, sizeof(personne), 1, lFichier);
    fwrite(&p2, sizeof(personne), 1, lFichier);

    fclose(lFichier);

    lFichier = fopen("exemple.txt", "r");

    while(fread(&px, sizeof(personne),1,lFichier)) {
        printf("nom %s, prenom %s, age %i\n", px.nom, px.prenom, px.age);
    }

    fclose(lFichier);

    return 0;
}

```
Si vous allez voir le fichier exemple.txt,  vous verrer que les champs sont "paddés" avec des NUL.

L'écriture d'une structure a donc le désavantage de prendre beaucoup d'espace disque; mais a l'avantage d'être facile a gérer dans le code. 

</details>
