# Boucles <!-- omit in toc -->

Les boucles en C sont les mêmes qu'en Java. 

```c
for (initialisation; condition; modification) {
    // bloc de code
}

while (condition) {
    // bloc de code

do {
    // bloc de code 
} while (condition);
```

