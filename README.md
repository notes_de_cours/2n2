# 420-2N2-DM Développement d’applications natives I

    Hiver 2023

    Benoit Desrosiers

[comment]: #  (la source pour ce cours se trouve sous  https://gitlab.com/notes_de_cours/2n2.git)


Ce cours sert à introduire la programmation sur plateforme native à l’aide d’un langage interprété ou d’un langage compilé (non-croisé).

Ce cours a comme préalable **420-1N1-DM initiation à la programmation**

Il est donc sous-entendu que vous avez une base en programmation et que vous maitrisez les concepts de base tels que:
* variables
* boucles
* conditions

Je vous invite à relire les notes de cours de 1N1 afin de revoir ces concepts. Plusieurs notions seront couvertes très rapidement car elles sont pratiquement identiques à celles vues en Java. Si vous ne maitrisez pas une de ces notions, n'hésitez pas à demander plus d'explication, il est possible que d'autres ne les maitrisent pas non plus. 

Vous trouverez les notes de cours de 1N1 sur [https://note-1n1.robinhoodsj.org/#/](https://note-1n1.robinhoodsj.org/#/)

Dans le cadre de ce cours nous allons utiliser le langage C. Nous utiliserons l'environnement QT afin d'explorer la manipulation d'image et de son. 

Veuillez ouvrir le plan de cours se trouvant sur Léa. 


Merci à Louis Marchand pour la base d'une grande partie de ces notes de cours. 


