# Exercices supplémentaires sur les listes chainées <!-- omit in toc -->


Vous devez faire un `CRUD` pour les éléments d'une structure. 

> Qu'est ce qu'un CRUD?
> `C`reate `R`ead `U`pdate `D`elete  
> C'est donc les 4 opérations de base pouvant être faites sur une structure (ou une classe)

Il vous faut donc créer une fonction permettant 
* d'ajouter (Create) un élément; 
* d'afficher un éléments présents dans la liste (Read); 
* de modifier un élément (Update);
* et finalement d'effacer un élément (Delete)  


La structure à utiliser est la suivante:
```c
typedef struct personne
{
    char *nom;
    char *prenom;
} personne;

typedef struct animal animal;
struct animal
{
    char *nom;
    personne *proprietaire;
    animal *suivant;
};

typedef struct animaux
{
    animal *premier;
    int taille;
} animaux;
```

Comme vous avez pu remarquer, c'est une liste chainée :) 

Notez que j'ai utilisé le même identifiant pour le nom du type et pour le nom de la structure. C'est ce que vous verrez habituellement. 

Le code suivant vous donne la solution pour la création ainsi que affichage de TOUTES la liste. À vous de faire les 3 autres fonctions: 
* l'affichage d'UN animal en spécifiant l'index;
* la modification d'un animal en spécifiant l'index (vois ci-bas);
* l'effacement d'un animal en spécifiant son index.
  
De plus, vous devez ajouter une fonction pour vider la liste au complet. 


Pour la modification, `seul le nom de l'animal` peut être changé. Vous devez donc demander un nouveau nom, et le strcpy dans la structure. Vous n'avez pas à permettre de changer le prénom et nom du proprio (mais vous pouvez le faire pour vous pratiquer).

Voici donc un début de solution que vous pouvez utiliser :

<details>
<summary>structures.h</summary>

```c
#ifndef STRUCTURES_H
#define STRUCTURES_H

#define LONGUEUR_CHAMPS 30

typedef struct personne
{
    char *nom;
    char *prenom;
} personne;

typedef struct animal animal;
struct animal
{
    char *nom;
    personne *proprietaire;
    animal *suivant;
};

typedef struct animaux
{
    animal *premier;
    int taille;
} animaux;

#endif // STRUCTURES_H
```

</details>

<details>
<summary>gestionAnimal.h</summary>

```c
#ifndef GESTIONANIMAL_H
#define GESTIONANIMAL_H
#include "structures.h"


animaux *initialisationAnimaux();
void inscrireAnimal(animaux *listeAnimaux);
void insererAuDebut(animaux *aListe, char *aNom,
                    char *aNomProprio, char *aPrenomProprio);
void afficherAnimaux(animaux *aListe);

#endif // GESTIONANIMAL_H

```

</details>

<details>
<summary>gestionAnimal.c</summary>

```c
#include "structures.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

animaux *initialisationAnimaux() {
    animaux *lListe = calloc(1, sizeof(animaux));

    if (lListe == NULL ) // vérification de l'allocation de l'espace mémoire
    {
        exit(EXIT_FAILURE);
    }
    lListe->premier = NULL;
    lListe->taille = 0;
    return lListe;
}

void insererAuDebut(animaux *aListe, char *aNom,
                    char *aNomProprio,
                    char *aPrenomProprio)
{
    animal *lAnimal = calloc(1, sizeof(animal));
    personne *lProprio = calloc(1, sizeof(personne));
    char *lNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lPrenomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lNomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    if(!lAnimal || !lProprio || !lNom || !lPrenomProprio || !lNomProprio)
    {
        exit(EXIT_FAILURE);
    }

    strcpy(lNom, aNom);
    strcpy(lPrenomProprio, aPrenomProprio);
    strcpy(lNomProprio, aNomProprio);

    lAnimal->nom = lNom;
    lProprio->prenom = lPrenomProprio;
    lProprio->nom = lNomProprio;

    lAnimal->proprietaire = lProprio;

    //insère l'animal au début de la liste
    lAnimal->suivant = aListe->premier;
    aListe->premier = lAnimal;
    aListe->taille++;

}


void inscrireAnimal(animaux *aListe)
{
    char *lNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lNomProprio= calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lPrenomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    printf("Nom de l'animal :");
    scanf("%s",lNom);
    printf("Prenom du proprio :");
    scanf("%s",lPrenomProprio);
    printf("Nom du proprio :");
    scanf("%s",lNomProprio);

    insererAuDebut(aListe, lNom, lNomProprio, lPrenomProprio);
}

void afficherAnimaux(animaux *aListe) {
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    char animal_aux[8]="animaux";
    if(aListe->taille == 1) {
        strcpy(animal_aux, "animal");
    }
    printf("il y a presentement %i %s dans la liste \n", aListe->taille, animal_aux);
    animal *actuel = aListe->premier;
    while (actuel != NULL)
    {
        printf("Nom :            %s\n", actuel->nom);
        printf("Nom du proprio : %s\n", actuel->proprietaire->nom);
        printf("Prenom :         %s\n", actuel->proprietaire->prenom);
        printf("-------------------\n");
        actuel = actuel->suivant;
    }
    printf("--------------\n");

}

```

</details>

<details>
<summary>main.c</summary>

```c
#include <stdio.h>
#include "structures.h"
#include "gestionAnimal.h"


int main()
{
    int lChoixPrincipale;

    animaux *listeAnimaux = initialisationAnimaux();
    do {

        printf("Bienvenue a la Clinique Veterinaire chez Benou\n");
        printf("1) inscrire un animal\n");
        printf("2) afficher un animal\n");
        printf("3) modifier un animal\n");
        printf("4) effacer un animal\n");
        printf("5) afficher tous les animaux\n");
        printf("6) effacer la liste au complet\n");
        printf("0) sortir\n");
        scanf("%i", &lChoixPrincipale);
        switch(lChoixPrincipale) {
        case 1:
            inscrireAnimal(listeAnimaux);
            break;
        case 2:

            break;
        case 3:

            break;
        case 4:

            break;
        case 5:
            afficherAnimaux(listeAnimaux);
            break;
        case 6:
            break;
        }
    } while(lChoixPrincipale !=0);
}

```

</details>


et la solution au complet que vous ne devriez regarder qu'une fois votre solution développée :

<details>
<summary>structures.h</summary>

```c
#ifndef STRUCTURES_H
#define STRUCTURES_H

#define LONGUEUR_CHAMPS 30

typedef struct personne
{
    char *nom;
    char *prenom;
} personne;

typedef struct animal animal;
struct animal
{
    char *nom;
    personne *proprietaire;
    animal *suivant;
};

typedef struct animaux
{
    animal *premier;
    int taille;
} animaux;

#endif // STRUCTURES_H

```

</details>

<details>
<summary>gestionAnimal.h</summary>

```c
#ifndef GESTIONANIMAL_H
#define GESTIONANIMAL_H
#include "structures.h"


animaux *initialisationAnimaux();
void inscrireAnimal(animaux *listeAnimaux);
void insererAuDebut(animaux *aListe, char *aNom,
                    char *aNomProprio, char *aPrenomProprio);
void afficherAnimal(animal *aAnimal);
void afficherAnimaux(animaux *aListe);
animal *trouverAnimalParIndex(animaux *aListe, int aIndex);
void afficherAnimalParIndex(animaux *aListe, int aIndex);
void modifierAnimalParIndex(animaux *aListe, int aIndex);
void liberer(animal *aAnimal);
void effacerAnimalParIndex(animaux *aListe, int aIndex);
void effacerTousLesAnimaux(animaux *aListe);
#endif // GESTIONANIMAL_H


```

</details>

<details>
<summary>gestionAnimal.c</summary>

```c
#include "structures.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

animaux *initialisationAnimaux() {
    animaux *lListe = calloc(1, sizeof(animaux));

    if (lListe == NULL ) // vérification de l'allocation de l'espace mémoire
    {
        exit(EXIT_FAILURE);
    }
    lListe->premier = NULL;
    lListe->taille = 0;
    return lListe;
}

void insererAuDebut(animaux *aListe, char *aNom,
                    char *aNomProprio,
                    char *aPrenomProprio)
{
    animal *lAnimal = calloc(1, sizeof(animal));
    personne *lProprio = calloc(1, sizeof(personne));
    char *lNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lPrenomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lNomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    if(!lAnimal || !lProprio || !lNom || !lPrenomProprio || !lNomProprio)
    {
        exit(EXIT_FAILURE);
    }

    strcpy(lNom, aNom);
    strcpy(lPrenomProprio, aPrenomProprio);
    strcpy(lNomProprio, aNomProprio);

    lAnimal->nom = lNom;
    lProprio->prenom = lPrenomProprio;
    lProprio->nom = lNomProprio;

    lAnimal->proprietaire = lProprio;

    //insère l'animal au début de la liste
    lAnimal->suivant = aListe->premier;
    aListe->premier = lAnimal;
    aListe->taille++;

}


void inscrireAnimal(animaux *aListe)
{
    char *lNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lNomProprio= calloc(LONGUEUR_CHAMPS, sizeof(char));
    char *lPrenomProprio = calloc(LONGUEUR_CHAMPS, sizeof(char));
    printf("Nom de l'animal :");
    scanf("%s",lNom);
    printf("Prenom du proprio :");
    scanf("%s",lPrenomProprio);
    printf("Nom du proprio :");
    scanf("%s",lNomProprio);

    insererAuDebut(aListe, lNom, lNomProprio, lPrenomProprio);
}

void afficherAnimal(animal *aAnimal) {
    printf("Nom :            %s\n", aAnimal->nom);
    printf("Nom du proprio : %s\n", aAnimal->proprietaire->nom);
    printf("Prenom :         %s\n", aAnimal->proprietaire->prenom);
    printf("-------------------\n");
}

void afficherAnimaux(animaux *aListe) {
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    char animal_aux[8]="animaux";
    if(aListe->taille == 1) {
        strcpy(animal_aux, "animal");
    }
    printf("il y a presentement %i %s dans la liste \n", aListe->taille, animal_aux);
    animal *actuel = aListe->premier;
    while (actuel != NULL)
    {
        afficherAnimal(actuel);
        actuel = actuel->suivant;
    }
    printf("--------------------------\n");

}

animal *trouverAnimalParIndex(animaux *aListe, int aIndex)
{
    int lPosition=1;
    if (aListe == NULL)
    {
        exit(EXIT_FAILURE);
    }
    animal *actuel = aListe->premier;
    while(lPosition < aIndex && actuel != NULL)
    {
        lPosition++;
        actuel = actuel->suivant;
    }
    return actuel;
}

void afficherAnimalParIndex(animaux *aListe, int aIndex)
{
    animal *actuel = trouverAnimalParIndex(aListe, aIndex);
    if(actuel) {
        afficherAnimal(actuel);
    } else {
        printf("Animal non trouve!\n");
    }

}

void modifierAnimalParIndex(animaux *aListe, int aIndex)
{
    char *nouveauNom = calloc(LONGUEUR_CHAMPS, sizeof(char));
    animal *actuel = trouverAnimalParIndex(aListe, aIndex);
    if(actuel) {
        printf("Valeurs actuelles : \n");
        afficherAnimal(actuel);
        printf("Entrez le nouveau nom de l'animal :");
        scanf("%s", nouveauNom );
        strcpy(actuel->nom, nouveauNom);
        printf("Nouvelles valeurs : \n");
        afficherAnimal(actuel);
    } else {
        printf("Animal non trouve!\n");
    }
}

void liberer(animal *aAnimal)
{
    free(aAnimal->nom);
    free(aAnimal->proprietaire->nom);
    free(aAnimal->proprietaire->prenom);
    free(aAnimal->proprietaire);
    free(aAnimal);
}

void effacerAnimalParIndex(animaux *aListe, int aIndex)
{
    animal *actuel;
    animal *lEffacer;
    if(aListe->taille == 0) {
        printf("la liste est deja vide! \n");
    } else {
        if(aIndex== 1) { // début de la liste, et liste non-vide
            lEffacer = aListe->premier;
            aListe->premier=lEffacer->suivant;
            liberer(lEffacer);
            aListe->taille--;
        } else { // un élément autre que le premier
            actuel = trouverAnimalParIndex(aListe, aIndex-1 ); // -1 pour arrêter a celui d'avant

            if(actuel && actuel->suivant) { //actuel est le précédent
                //actuel peut donc être là, mais le suivant (que l'on cherche) ne le soit pas
                lEffacer = actuel->suivant;
                actuel->suivant = actuel->suivant->suivant;
                liberer(lEffacer);
                aListe->taille--;
            } else {
                printf("Animal non trouve!\n");
            }
        }
    }
}

void effacerTousLesAnimaux(animaux *aListe)
{
    animal *actuel;
    animal *suivant;

    actuel = aListe->premier;
    while(actuel) {
        suivant = actuel->suivant;
        liberer(actuel);
        actuel = suivant;
    }
    aListe->premier = NULL;
    aListe->taille = 0;
}

```

</details>

<details>
<summary>main.c</summary>

```c
#include <stdio.h>
#include "structures.h"
#include "gestionAnimal.h"


int main()
{
    int lChoixPrincipale;
    int index = 0;

    animaux *listeAnimaux = initialisationAnimaux();
    do {

        printf("Bienvenue a la Clinique Veterinaire chez Benou\n");
        printf("1) inscrire un animal\n");
        printf("2) afficher un animal\n");
        printf("3) modifier un animal\n");
        printf("4) effacer un animal\n");
        printf("5) afficher tous les animaux\n");
        printf("6) effacer la liste au complet\n");
        printf("0) sortir\n");
        scanf("%i", &lChoixPrincipale);
        switch(lChoixPrincipale) {
        case 1:
            inscrireAnimal(listeAnimaux);
            break;
        case 2:
            printf("Index de l'animal :");
            scanf("%i", &index);
            afficherAnimalParIndex(listeAnimaux, index);

            break;
        case 3:
            printf("Index de l'animal :");
            scanf("%i", &index);
            modifierAnimalParIndex(listeAnimaux, index);
            break;
        case 4:
            printf("Index de l'animal :");
            scanf("%i", &index);
            effacerAnimalParIndex(listeAnimaux, index);
            break;
        case 5:
            afficherAnimaux(listeAnimaux);
            break;
        case 6:
            effacerTousLesAnimaux(listeAnimaux);
            break;
        }
    } while(lChoixPrincipale !=0);
}


```

</details>