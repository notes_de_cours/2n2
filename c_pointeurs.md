# Pointeur <!-- omit in toc -->

Nous allons ici apprendre une nouvelle notion qui pourra vous sembler assez difficile, mais elle est primordiale pour programmer en C et mieux comprendre plusieurs concepts d'autres langages. 

Nous avons déjà un peu effleuré le sujet en utilisant certaines commandes telles que `scanf`, ou même en utilisant les tableaux. 

# 1. Adresse mémoire
Commencons par comprendre ce qu'est fondamentalement une variable. 

Lorsqu'on écrit 
```c
int x = 10;
```
Cela veut en réalité dire qu'on demande au langage de nous réserver un espace en mémoire assez grand pour mettre un entier, et d'y mettre la valeur 10. Afin de faciliter la vie au programmeur, on donne le nom `x` à cet espace mémoire. 

Ensuite dans le code, lorsqu'on réfère à `x`, en réalité on réfère à cet espace mémoire. 

Cet espace mémoire a une adresse en mémoire. Essayez le code suivant:

```c
int x = 10;
printf("%i\n", x);
printf("%p\n", &x);
```

Le premier `printf` retourne la valeur de `x` tel qu'on s'y attendait. 

Mais qu'est-ce qui est retourné par le deuxième?

Le format `%p` indique d'imprimer la valeur en utilisant le format d'un pointeur. C'est donc l'adresse de l'espace mémoire occupé par `x` . 

*Notez qu'il serait aussi possible de prendre `%x` pour afficher la valeur en hexadécimal, mais c'est moins 'beau'.*

# 2. Pointeur

Maintenant que nous comprenons qu'une variable n'est en fait que l'adresse en mémoire de la valeur, que peut-on faire avec ca?

Nous allons utiliser un nouveau 'type' de variable: les pointeurs. Ils serviront 
à entreposer cette fameuse adresse. 

Pour créer un pointeur, il faut indiquer le type de pointeur que l'on désire. 
```c
int *unPointeur;
```
Notez `*` devant le nom de la variable. Ici, c'est un pointeur vers un entier.

Il est aussi possible d'utiliser la syntaxe suivante:
```c
int* unPointeur;
```
C'est la même chose. Mais il est recommandé d'utiliser la première syntaxe puisqu'elle est nécessaire si vous avez plusieurs pointeurs à déclarer.
```c
int *pointeur1, *pointeur2, *pointeur3;
```

Maintenant, que fait-on avec un pointeur?  Et bien, on lui assigne une adresse. 

```c
int x = 10;
int *pointeur;
pointeur = &x;
printf("%i\n", *pointeur);
printf("%p\n", &x);
printf("%p\n", pointeur);
printf("%p\n", &pointeur);

```

Notez `&` devant `x`. Cet opérateur retourne l'adresse de la variable x. 

Notez `*` dans le premier `printf`. Ca veut dire: la valeur stockée à l'adresse contenue par le pointeur (*on dit qu'on [déréférence](https://fr.wikipedia.org/wiki/Op%C3%A9rateur_de_d%C3%A9r%C3%A9f%C3%A9rencement) le pointeur*).
Et `pointeur` a été mis égal à l'adresse de `x`. Si on déréférence `pointeur`, on va donc chercher la valeur 10. 

Le deuxième printf affiche l'adresse de la variable `x`. 

Le troisième affiche la valeur contenue dans `pointeur`, c-a-d l'adresse de `x`. 

Le dernier printf peut sembler bizarre : afficher l'adresse d'un pointeur??? Mais un pointeur est aussi une variable, il est donc entreposé à quelquepart en mémoire et a donc une adresse lui aussi. (on verra plus tard qu'on peut même avoir des pointeurs de pointeur!).

On a donc trois nouveaux opérateurs:  
* `*` dans la déclaration d'une variable, indiquant que le type est en réalité un pointeur;
* `*` dans l'utilisation d'un pointeur, servant à déréférencer le pointeur;
* `&` dans l'utilisation d'une variable, indiquant qu'on veut l'adresse de la variable, pas son contenue. 

Si le `&` vous dit quelque chose, c'est que vous suivez bien le cours ;)

Nous l'avons rencontré dans le scanf.

```c
int x;
scanf("%i", &x);
```
Pourquoi doit on mettre `&x` ?

La fonction scanf doit lire le clavier et retourner cette valeur dans `x`. 

Essayons quelque chose:
```c
#include <stdio.h>

void fct(int a)
{
    printf("en entrant %i\n", a);
    a = 20;
}
int main()
{
    int x = 10 ;
    fct(x);
    printf("en sortant %i\n", x);
}
```

Pourquoi la valeur de `x` n'est pas changée à la sortie de `fct` ?

On dit que le passage de valeur se fait par `valeur`. 

Si on regarde le fonctionnement de ce code, on verrait que 
* un espace mémoire est alloué pour `x`;
* la valeur 10 est entreposé dans cet espace;
* lors de l'appel à `fct()`, un espace mémoire est alloué pour le paramètre, une variable de type entier appelée `a`;
* la **valeur** de `x` (soit 10) est **copiée** dans l'espace mémoire de `a`;
* l'espace mémoire de `a` est mis à 20;
* lorsque `fct()` termine, l'espace mémoire pour `a` est 'oublié' (*nous verrons ce que ca veux dire exactement dans la prochaine section*);
* dans le `main()` la valeur de `x` n'a pas changé. 

Maintenant, modifions un peu ce code:
```c
#include <stdio.h>

void fct(int *a)
{
    printf("valeur de a  : %p\n", a);
    printf("en entrant %i\n", *a);
    *a = 20;
}
int main()
{
    int x = 10 ;
    printf("adresse de x : %p\n", &x);
    fct(&x);
    printf("en sortant %i\n", x);
}
```

Si on regarde le fonctionnement de ce code, on verrait que:
* un espace mémoire est alloué pour `x`;
* la valeur 10 est entreposée dans cet espace;
* lors de l'appel à `fct()`, un espace mémoire est alloué pour le paramètre, un **pointeur vers un entier** appelé `a` (*notez `*` devant `a`*);
* l'**adresse** de `x` (*notez le `&` devant `x`*) est **copiée** dans l'espace mémoire de `a`;
* notez que les deux adresses (`&x` et `a`) sont les mêmes;
* le printf déréférence `a` (*notez le `*`devant `a`*), et affiche donc 10;
* l'espace mémoire pointé par `a` est mis à 20 (*notez le `*` devant `a`*). Mais cette espace mémoire est le même que pour `x`;
* lorsque `fct()` termine, l'espace mémoire pour `a` est 'oublié' (mais c'est juste une copie de l'adresse de `x`);
* dans le `main()` la valeur de `x` a changée.  

Comprenez vous maintenant comment fonctionne `scanf()` ?

Une autre facon d'avoir écrit le `scanf()` aurait été de retourner la valeur lue au clavier. L'utilisation de `scanf` aurait donc été:
```c
int x;
x = scanf();
```

Mais souvenez-vous que `scanf` retourne déjà une valeur: le nombre de bonnes entrées lues, ou un code d'erreur. Donc si on utilise le `return` pour retourner la valeur lue, on ne peut pas avoir de code d'erreur. (*Vous verrez dans des langages de plus haut niveau qu'ils utilisent le mécanisme d'exception pour réaliser la même chose.*) 

De plus, on sait qu'une fonction à un type. Donc `scanf` devrait être déclaré comme suit:
```c
int scanf();
``` 

Mais si on veut faire l'entré au clavier d'une chaine de caractère, ca prendrait
```c
char * scanf();
```

Mais on ne peut avoir 2 fonctions avec le même nom (en C). Il faudrait donc avoir `scanInt()`, `scanStr()`, `scanFloat()`...

Et `scanf()` permet aussi de faire la lecture de plusieurs choses en même temps... comment faire ca avec la syntaxe proposée ??? 

Voilà donc pourquoi `scanf` est comme ca ... et pourquoi les pointeurs c'est très utile (*une des raisons, nous allons en voir d'autre*). 

# 3. La valeur NULL

Lorsqu'on crée un pointeur, il est de coutume de lui assigner la valeur `NULL` s'il n'y a pas d'autre adresse à lui assigner.

`NULL` est une valeur spéciale en C. Habituellement, elle prend la valeur 0. 

On peut aussi vérifier si un pointeur est null ou non. 

```c
if (ptr)  // sera vrai si ptr a une valeur autre que NULL

if (!ptr)  // sera vrai si ptr est NULL
```

# 4. Les tableaux dans tout ca

Pourquoi parle-t-on des tableaux dans une section pour les pointeurs?

Simplement parce qu'un tableau, c'est en réalité un pointeur.

Voyons un exemple:

```c
#include <stdio.h>

void fct(int *a)
{
    printf("valeur de a  : %p\n", a);
    printf("valeur de l'espace mémoire pointé par a en entrant: %i\n", *a);
    *a = 20;
}
int main()
{
    int x[5] = {1,2,3,4,5} ;
    int *y;
    y = x;
    printf("adresse de x : %p\n", x);
    printf("valeur du premier élément de x: %i\n", x[0]);
    printf("valeur de l'espace mémoire pointé par y: %i\n", *y);

    *y = 3;
    printf("valeur du premier élément de x après l'avoir changé via *y: %i\n", x[0]);

    fct(x);
    printf("valeur du tableau apres l'appel de fct: ");
    for(int i=0; i< 5; i++){
        printf("%i ",x[i]);
    }
    printf("\n");

    printf("valeur de y (en décimal non signé) %u\n", y);
    y++;
    printf("valeur de y après un incréement: %u\n", y);
    printf("valeur de l'espace mémoire pointé par y: %i\n", *y);

    *y = 10;
    printf("valeur du tableau apres avoir changé via *y qui vient d'être incrémenté: ");

    for(int i=0; i< 5; i++){
        printf("%i ",x[i]);
    }
    printf("\n");
}

```

Un tableau est une suite d'élément de même type, entreposés dans un espace contigu en mémoire.

Afin de trouver un élément par son indice, il suffit de prendre l'adresse du tableau et de lui additionner (la taille d'un élément multiplié par l'indice). C'est pour ca que les indice commence à 0. 

Lorsqu'on écrite `tableau[4]`, c'est converti en: ` adresse du début du tableau + 4*longueur d'un élément ` 

La longueur d'un élément, c'est son type. Un `int` prend 4 octets. 

Si vous avez remarqué, dans l'exemple, après la ligne `y++` la valeur de y a augmentée de 4, car l'opérateur `++` sait que ca ne fait pas de sens d'incrémenter un pointeur d'entier juste de 1 car on arriverait entre deux entiers. 

## 4.1. Passage d'un tableau en paramètre. 

Dans l'exemple précédent, le tableau a été passé en paramètre à la fonction. Mais si dans cette fonction je désirais parcourir le tableau, comment faire pour savoir quand arrêter?

C'est pour cela qu'il est courant que les fonctions acceptant un tableau prendront aussi en paramètre la taille de ce tableau. 

Modifions donc notre exemple pour y ajouter une fonction affichant le tableau:
```c
#include <stdio.h>

void fct(int *a)
{
    printf("valeur de a  : %p\n", a);
    printf("valeur de l'espace mémoire pointé par a en entrant: %i\n", *a);
    *a = 20;
}

void afficherTableau(int aTaille, int *aTableau) {
    for(int i=0; i< aTaille; i++){
        printf("%i ",aTableau[i]);
    }
}

int main()
{
    const int nombreElementDansLeTableau = 5;
    int x[nombreElementDansLeTableau] = {1,2,3,4,5} ;
    int *y;
    y = x;
    printf("adresse de x : %p\n", x);
    printf("valeur du premier élément de x: %i\n", x[0]);
    printf("valeur de l'espace mémoire pointé par y: %i\n", *y);

    *y = 3;
    printf("valeur du premier élément de x après l'avoir changé via *y: %i\n", x[0]);

    fct(x);
    printf("valeur du tableau apres l'appel de fct: ");
    afficherTableau(nombreElementDansLeTableau, x);
    printf("\n");

    printf("valeur de y (en décimal non signé) %u\n", y);
    y++;
    printf("valeur de y après un incréement: %u\n", y);
    printf("valeur de l'espace mémoire pointé par y: %i\n", *y);

    *y = 10;
    printf("valeur du tableau apres avoir changé via *y qui vient d'être incrémenté: ");
    afficherTableau(nombreElementDansLeTableau,x);
    printf("\n");
}

```

# 5. Les strings dans tout ca

Si un `tableau` est un `pointeur` et une `string` est un `tableau`, alors une `string` est un `pointeur` aussi !

Il est donc possible de déclarer une `string` avec  `char *` !

Mais attention, essayez le code suivant:
```c
int main()
{
    char  chaine[5] = "allo";

    chaine[0] = 'b';
    printf("%s\n", chaine);
}
```

Rien de spécial...

Essayez celui-ci maintenant:
```c
int main()
{
    char  *chaine = "allo";

    chaine[0] = 'b';
    printf("%s\n", chaine);
}
```

Pourquoi un "segmentation fault" ?

Dans le premier cas, un espace de 5 caractères est réservé, `chaine` pointe sur cet espace, et "allo" est copié dans l'espace mémoire de 5 caractères .

Dans le deuxième cas, c'est l'adresse de "allo" qui a été copiée dans chaine. Mais où est entreposé "allo"?

Une espace en lecture seul est alloué pour les "constantes" et "allo" est une constante. 

Essayez le code suivant:
```c
int main()
{
    char  *chaine1 = "allo";
    char *chaine2 = "allo";
    printf("1) %p\n", chaine1);
    printf("2) %p\n", chaine2);

    char chaine3[5] = "allo";
    char chaine4[5] = "allo";

    printf("3) %p\n", &chaine3[0]);
    printf("3a) %p\n", chaine3);

    printf("4) %p\n", chaine4);
}

```

Notez que les `printf` 1 et 2 retournent la même adresse, soit l'adresse de la constante "allo". 

Les `printf` 3 et 3a sont uniquement pour vous montrer que l'adresse du premier élément d'un tableau et l'adresse du tableau, c'est la même chose. 

Les `printf` 3 et 4 ne retourne pas la même adresse car ce n'est pas le même tableau. 